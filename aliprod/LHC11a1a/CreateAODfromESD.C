#if !defined(__CINT__) || defined(__MAKECINT__)
#include <TChain.h>
#include <TSystem.h>
#include "AliAnalysisManager.h"
#include "AliESDInputHandler.h"
#include "AliAODHandler.h"
#include "AliAnalysisTaskESDfilter.h"
#include "AliAnalysisDataContainer.h"
#endif

void CreateAODfromESD(const char *inFileName = "AliESDs.root",
		      const char *outFileName = "AliAOD.root",
		      Bool_t bKineFilter = kTRUE) 
{
  
    gSystem->Load("libTree");
    gSystem->Load("libGeom");
    gSystem->Load("libPhysics");
    gSystem->Load("libVMC");

    gSystem->Load("libTree");
    gSystem->Load("libPhysics");
    gSystem->Load("libHist");
    gSystem->Load("libVMC");
    gSystem->Load("libCGAL.so");
    gSystem->Load("libfastjet.so");
    gSystem->Load("libsiscone.so");
    gSystem->Load("libSISConePlugin.so");
    gSystem->Load("libSTEERBase");
    gSystem->Load("libESD");
    gSystem->Load("libAOD");
    gSystem->Load("libANALYSIS");
    gSystem->Load("libANALYSISalice");
    gSystem->Load("libCORRFW");
    gSystem->Load("libPWG3base");
    gSystem->Load("libPWG3muon");
    gSystem->Load("libJETAN");
    gSystem->Load("libFASTJETAN");

    TChain *chain = new TChain("esdTree");
    // Steering input chain
    chain->Add(inFileName);
    AliAnalysisManager *mgr  = new AliAnalysisManager("ESD to AOD", "Analysis Manager");

    // Input
    AliESDInputHandler* inpHandler = new AliESDInputHandler();
    inpHandler->SetReadFriends(kFALSE);
    inpHandler->SetReadTags();
    mgr->SetInputEventHandler  (inpHandler);
    // Output
    AliAODHandler* aodHandler   = new AliAODHandler();
    aodHandler->SetOutputFileName(outFileName);
    mgr->SetOutputEventHandler(aodHandler);
    mgr->GetCommonInputContainer()->SetSpecialOutput();

    // MC Truth
    if(bKineFilter){
	AliMCEventHandler* mcHandler = new AliMCEventHandler();
	mgr->SetMCtruthEventHandler(mcHandler);
    }

    gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/AddTaskESDFilter.C");
    AliAnalysisTaskESDfilter *taskesdfilter = AddTaskESDFilter(bKineFilter);

    gROOT->LoadMacro("AddTaskJets.C");
    AliAnalysisTaskJets *taskjets = AddTaskJets(16);
    UInt_t runFlag = (1<<0)|(1<<9); // create MC jet for UA1 and anti-kT
    AddTaskJetsDelta("",16,kTRUE,runFlag);

    AliAnalysisTaskTagCreator* tagTask = new AliAnalysisTaskTagCreator("AOD Tag Creator");
    AliAnalysisDataContainer *coutTags = mgr->CreateContainer("cTag",  TTree::Class(),
							      AliAnalysisManager::kOutputContainer, "AOD.tag.root");
    mgr->ConnectInput (tagTask, 0, mgr->GetCommonInputContainer());
    mgr->ConnectOutput(tagTask, 1, coutTags);

    //    taskjets = AddTaskJets(16);
    AliLog::SetGlobalLogLevel(AliLog::kError);
    mgr->InitAnalysis();
    mgr->PrintStatus();
    mgr->StartAnalysis("local", chain);
}

