//--- Magnetic Field ---
enum Mag_t
{
  kNoField, k5kG, kFieldMax
};
const char * pprField[] = {
  "kNoField", "k5kG"
};

static Mag_t         mag      = k5kG;
void ProcessEnvironmentVars();

void rec() {
  ProcessEnvironmentVars();

  AliReconstruction reco;
  reco.SetUniformFieldTracking(kFALSE);
  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();
  reco.SetRecoParam("TPC",AliTPCRecoParam::GetLowFluxParam());
  reco.SetRecoParam("TRD",AliTRDrecoParam::GetLowFluxParam());
  reco.SetRecoParam("PHOS",AliPHOSRecoParam::GetDefaultParameters());
  reco.SetRecoParam("MUON",AliMUONRecoParam::GetLowFluxParam());
  AliTPCReconstructor::SetStreamLevel(1);
  //   reco.SetInput("raw.root");
  reco.SetRunReconstruction("ITS TPC VZERO");
  reco.SetDefaultStorage("alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");
  reco.SetRunQA(":");
  reco.SetRunGlobalQA(kFALSE);
  reco.SetMeanVertexConstraint(kFALSE);

// **** The field map settings must be the same as in Config.C !
  AliMagWrapCheb* field = 0x0;
  if (mag == kNoField) {
    field = new AliMagWrapCheb("Maps","Maps", 2, 0., 10., AliMagWrapCheb::k2kG,
			       kTRUE,"$(ALICE_ROOT)/data/maps/mfchebKGI_sym.root");
  } else if (mag == k5kG) {
    field = new AliMagWrapCheb("Maps","Maps", 2, 1., 10., AliMagWrapCheb::k5kG,
			       kTRUE,"$(ALICE_ROOT)/data/maps/mfchebKGI_sym.root");
  }
  Bool_t uniform=kFALSE;
  AliTracker::SetFieldMap(field,uniform);  // tracking with the real map

  TStopwatch timer;
  timer.Start();
  reco.Run();
  timer.Stop();
  timer.Print();
}
void ProcessEnvironmentVars()
{
    // Field
    if (gSystem->Getenv("CONFIG_FIELD")) {
      for (Int_t iField = 0; iField < kFieldMax; iField++) {
	if (strcmp(gSystem->Getenv("CONFIG_FIELD"), pprField[iField])==0) {
	  mag = (Mag_t)iField;
	  cout<<"Field set to "<<pprField[iField]<<endl;
	}
      }
    }
}
