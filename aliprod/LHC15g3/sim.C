void sim(Int_t nev=200) 
{
	AliSimulation simulator;

	//	simulator.SetWriteRawData("ALL","raw.root",kFALSE);
	simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD AD");
	simulator.SetMakeDigitsFromHits("ITS TPC");
	// can't detect from GRP if HLT was running, off for safety now
	simulator.SetRunHLT("");
	//
	//
	// RAW OCDB
	simulator.SetDefaultStorage("alien://Folder=/alice/data/2015/OCDB");

	// Specific storages = 23 

	//
	// ITS  (1 Total)
	//     Alignment from Ideal OCDB 

	simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

	//
	// MUON (1 object)

	simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal"); 
	
	//
	// TPC (2 new objects for Run2 MC) 

        // Parameters should be used from the raw OCDB at some point
	simulator.SetSpecificStorage("TPC/Calib/Parameters",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
        // ClusterParam needs to be tuned for each group of MCs
	simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");

	// TPC (4 old objects) 
	
	simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
	simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
	simulator.SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
	simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

	// ZDC
        simulator.SetSpecificStorage("ZDC/Align/Data", "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

	// Vertex and magfield

	simulator.UseVertexFromCDB();
	simulator.UseMagFieldFromGRP();

	// PHOS simulation settings
	AliPHOSSimParam *simParam = AliPHOSSimParam::GetInstance();
	simParam->SetCellNonLineairyA(0.001);
	simParam->SetCellNonLineairyB(0.2);
	simParam->SetCellNonLineairyC(1.02);

	//
	// The rest
	//

        simulator.SetRunQA(":");

	printf("Before simulator.Run(nev);\n");
	simulator.Run(nev);
	printf("After simulator.Run(nev);\n");
}

