#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TRandom.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include "STEER/AliRunLoader.h"
#include "STEER/AliRun.h"
#include "STEER/AliConfig.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "STEER/AliMagFCheb.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "MUON/AliMUONv1.h"
#endif

static Int_t     runNumber = 0;
static Int_t     eventNumber = 0;
static UInt_t    seed     = 1;
static Float_t   energy   = 2760.0; // energy in CMS

void Config(){
  if (gSystem->Getenv("CONFIG_SEED")) seed      = atoi(gSystem->Getenv("CONFIG_SEED"));
  if (gSystem->Getenv("DC_RUN"))      runNumber = atoi(gSystem->Getenv("DC_RUN"));
  gRandom->SetSeed(seed);
  cerr<<"Seed for random number generation= "<<seed<<endl; 
  if (gSystem->Getenv("CONFIG_ENERGY")) energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
  cout<<"Energy set to "<<energy<<" GeV"<<endl; 

  Int_t NEventsPerSegment = 10000; 

  // Libraries required by geant321
#if defined(__CINT__)
  gSystem->Load("liblhapdf");      // Parton density functions
  gSystem->Load("libEGPythia6");   // TGenerator interface
  gSystem->Load("libpythia6");     // Pythia 6.2
  gSystem->Load("libAliPythia6");  // ALICE specific implementations
  gSystem->Load("libgeant321");
#endif

  new TGeant3TGeo("C++ Interface to Geant3");
  cout<<"Config.C: Creating Run Loader ..."<<endl;
  AliRunLoader* rl=AliRunLoader::Open("galice.root",AliConfig::GetDefaultEventFolderName(),"recreate");
  if (rl == 0x0) {
    gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
    return;
  }

  // Added these 2 (JN 6-Feb-2012) 
  rl->LoadKinematics("RECREATE"); 
  rl->MakeTree("E"); 

  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(NEventsPerSegment);
  gAlice->SetRunLoader(rl);

  // AliGenReaderSL* reader = new AliGenReaderSL();
  // reader->SetFormat(3);
  // reader->SetFileName("slight.out");

  AliGenReaderTreeK* reader = new AliGenReaderTreeK();
  reader->SetFileName("galice.root");
  reader->AddDir("./input");

  AliGenExtFile* gener = new AliGenExtFile(-1);
  gener->SetVertexSmear(kPerEvent);      	
  gener->SetEnergyCMS(energy); 
  gener->SetProjectile("A", 208, 82); 
  gener->SetTarget    ("A", 208, 82); 
  gener->SetReader(reader);
  gener->Init();

  // Event number
  //  if (gSystem->Getenv("DC_EVENT")) {
  //    eventNumber = atoi(gSystem->Getenv("DC_EVENT"));
  //  }
  //  cout<<" In Config.C, eventNumber: "<<eventNumber<<endl; 
  // Run number
  if (gSystem->Getenv("DC_RUN")) {
    eventNumber = atoi(gSystem->Getenv("DC_RUN"));
  }
  cout<<" In Config.C, runNumber: "<<runNumber<<endl; 
  Int_t nseg = 0; 
  if( runNumber == 168108 ){
    nseg = 0;
  } elseif (runNumber == 168107 ) {
    nseg = 1;
  } elseif (runNumber == 168066 ) {
    nseg = 2;
  } elseif (runNumber == 167988 ) {
    nseg = 3;
  } elseif (runNumber == 167987 ) {
    nseg = 4;
  } elseif (runNumber == 167986 ) {
    nseg = 5;
  } elseif (runNumber == 167985 ) {
    nseg = 6;
  } elseif (runNumber == 167921 ) {
    nseg = 7;
  } elseif (runNumber == 167920 ) {
    nseg = 8;
  } elseif (runNumber == 167915 ) {
    nseg = 9;
  } elseif (runNumber == 167818 ) {
    nseg = 10;
  } elseif (runNumber == 167814 ) {
    nseg = 11;
  } elseif (runNumber == 167813 ) {
    nseg = 12;
  } elseif (runNumber == 167808 ) {
    nseg = 13;
  } elseif (runNumber == 167807 ) {
    nseg = 14;
  } elseif (runNumber == 167806 ) {
    nseg = 15;
  } elseif (runNumber == 170083 ) {
    nseg = 16;
  } elseif (runNumber == 170084 ) {
    nesg = 17;
  } elseif (runNumber == 170308 ) {
    nseg = 18;
  } elseif (runNumber == 170311 ) {
    nseg = 19;
  } 
  Int_t off = nseg * NEventsPerSegment;	
  printf("offset is %5d \n", off); 
  for (Int_t i = 0; i < off; i++) reader->NextEvent(); 
  
  gMC->SetProcess("DCAY",1);
  gMC->SetProcess("PAIR",1);
  gMC->SetProcess("COMP",1);
  gMC->SetProcess("PHOT",1);
  gMC->SetProcess("PFIS",0);
  gMC->SetProcess("DRAY",0);
  gMC->SetProcess("ANNI",1);
  gMC->SetProcess("BREM",1);
  gMC->SetProcess("MUNU",1);
  gMC->SetProcess("CKOV",1);
  gMC->SetProcess("HADR",1);
  gMC->SetProcess("LOSS",2);
  gMC->SetProcess("MULS",1);
  gMC->SetProcess("RAYL",1);

  Float_t cut = 1.e-3;
  Float_t tofmax = 1.e10;

  gMC->SetCut("CUTGAM", cut);
  gMC->SetCut("CUTELE", cut);
  gMC->SetCut("CUTNEU", cut);
  gMC->SetCut("CUTHAD", cut);
  gMC->SetCut("CUTMUO", cut);
  gMC->SetCut("BCUTE",  cut); 
  gMC->SetCut("BCUTM",  cut); 
  gMC->SetCut("DCUTE",  cut); 
  gMC->SetCut("DCUTM",  cut); 
  gMC->SetCut("PPCUTM", cut);
  gMC->SetCut("TOFMAX", tofmax); 
  
  TGeoGlobalMagField::Instance()->SetField(new AliMagF("Maps","Maps", -1., -1., AliMagF::k5kG, AliMagF::kBeamTypeAA, 1380.));
  rl->CdGAFile();
  
  AliBODY *BODY = new AliBODY("BODY", "Alice envelop");
  AliMAG *MAG   = new AliMAG("MAG", "Magnet");
  AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
  AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
  AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
  AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
  FRAME->SetHoles(1);
  AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
  AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
  AliMUON *MUON   = new AliMUONv1("MUON", "default");
  MUON->SetTriggerEffCells(1);
}aliroot -b -q simrun.C --run 170040 --event 3 --process kStarlight --energy 2760
