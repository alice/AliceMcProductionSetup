#!/bin/bash

FILESTOCHECK="rec.log AliESDs.root Run*.ESD.tag.root"
REMOVEROOTONERROR=""

for rcfile in validation.rc validation_merge.rc; do
    if [ -f $rcfile ]; then
        source $rcfile
    fi
done

validateout=`dirname $0`

if [ -z "$validateout" ]; then
    validateout="."
fi

cd "$validateout"
validateworkdir=`pwd`

(
echo "* *****************************************************"
echo "* AliRoot Validation Script V2.1                      *"
echo "* Time:    `date`"
echo "* Dir:     $validateout"
echo "* Workdir: $validateworkdir"
echo "* PATH: $PATH"
echo "* LD_LIBRARY_PATH: $LD_LIBRARY_PATH"
echo "* ----------------------------------------------------*"

for subdir in . Barrel OuterDet; do
    if [ -d "$subdir" ]; then
        echo "Listing $subdir" 
        ls -lA "$subdir/"
        echo ""
    fi
done

echo "* ----------------------------------------------------*"
) >> stdout

if [ -f OCDB.generating.job ]; then
    echo "* This was a special OCDB.root job for which I'll skip the rest of the validation" >> stdout

    mv stdout stdout.ocdb.log 2>/dev/null
    mv stderr stderr.ocdb.log 2>/dev/null

    if [ -f OCDB.root ]; then
        echo "* ODCB.root found" >> stdout.ocdb.log
        exit 0
    else
        echo "* Error: OCDB.root NOT found! Failing validation" >> stdout.ocdb.log
        exit 1
    fi
fi

error=0

if grep -q -s "W-AliReconstruction::Run: No events passed trigger selection" *.log stdout stderr; then
    echo "Job is forcefully validated since no events passed trigger selection" >> stdout
else
    if [ -f "qa.log" ]; then
        FILESTOCHECK="$FILESTOCHECK QAresults.root EventStat_temp.root"
    fi
    
    for file in $FILESTOCHECK; do
        if [ ! -f "$file" ]; then
            error=1
            echo "* Error: Required file $file not found in the output" >> stdout
        fi
    done
    
    cat <<EOF >grep.cnf
std::bad_alloc
Segmentation violation
Segmentation fault
Bus error
floating point exception
Killed
busy flag cleared
Cannot Build the PAR Archive
*** glibc detected ***
E-AliCDBGrid::PutEntry:
F-AliCDBGrid::
EOF

    found=`grep -f grep.cnf -i -n -m 1 stdout stderr *.log`
    
    if [ ! -z "$found" ]; then
        (
            echo "* Found an error message in the logs:"
            echo "$found"
            echo ""
        ) >> stdout
    
        error=2
    fi

    cat <<EOF >grep.cnf
Abort
Break
EOF
    
    if [ $error -eq 0 ]; then
        found=`grep -f grep.cnf -w -n -m 1 stdout stderr *.log`
    
        if [ ! -z "$found" ]; then
    	(
            echo "* Found an error message in the logs:"
            echo "$found"
            echo ""
    	) >> stdout
    
    	error=2        
        fi
    fi
    
    rm grep.cnf
    
    if [ -f check.log ]; then
        if ! grep -q "check of ESD was successfull" check.log; then
            echo "*! The ESD was not successfully checked according to check.log" >> stdout
            error=3
        fi
    fi
fi

(
if [ $error -eq 0 ]; then
    echo "* ################   Job validated ####################"
else
    echo "*! ################   Job NOT validated, error code $error ################"
    
    if [ ! -z "$REMOVEROOTONERROR" ]; then
    echo "* ########## Removing all ROOT files from the local directory, leaving only the logs ###"
        rm -rf *.root
    fi
fi
) >> stdout

mv stdout stdout.log
mv stderr stderr.log

exit $error
