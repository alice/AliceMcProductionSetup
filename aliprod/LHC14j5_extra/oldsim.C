void AddDetToGRP(AliDAQ::DetectorBits det, int run);


void runSimulation(Int_t nev, Int_t runNumber) {

  AliSimulation *simulator = new AliSimulation();
  
  //   simulator->SetTriggerConfig("MUON");
  simulator->SetMakeSDigits("MFT TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD");
  simulator->SetMakeDigits("MFT TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD");
  //  simulator->SetMakeDigits("MUON MFT");
  simulator->SetMakeDigitsFromHits("ITS TPC");
  simulator->SetRunQA(":");
  simulator->SetRunHLT("");
  

  simulator->SetDefaultStorage("alien://folder=/alice/data/2010/OCDB");

  // ITS
  simulator->SetSpecificStorage("ITS/Align/Data", "alien://folder=/alice/simulation/LS1_upgrade/Ideal");
  simulator->SetSpecificStorage("ITS/Calib/SimuParam", "alien://folder=/alice/simulation/LS1_upgrade/Ideal");

  // TPC 
  simulator->SetSpecificStorage("TPC/Calib/TimeGain",       "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator->SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator->SetSpecificStorage("TPC/Calib/RecoParam",      "alien://folder=/alice/simulation/2008/v4-15-Release/Residual/");
  simulator->SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator->SetSpecificStorage("TPC/Calib/Correction",     "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator->SetSpecificStorage("TPC/Align/Data",           "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator->SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  
  // MUON Tracker
  simulator->SetSpecificStorage("MUON/Align/Data",      "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator->SetSpecificStorage("MUON/Calib/RecoParam", "alien://folder=/alice/cern.ch/user/a/auras/OCDB/");
  simulator->SetSpecificStorage("MFT/Align/Data",       "alien://folder=/alice/cern.ch/user/a/auras/OCDB/");
  simulator->SetSpecificStorage("MFT/Calib/RecoParam",  "alien://folder=/alice/cern.ch/user/a/auras/OCDB/");

  simulator->UseVertexFromCDB();
  simulator->UseMagFieldFromGRP();

  AliSysInfo::SetVerbose(kTRUE);

  // The rest
  TStopwatch timer;
  timer.Start();
  simulator->Run(nev);
  //
  AddDetToGRP(AliDAQ::kMFT,runNumber); // recreate GRP
  //
  timer.Stop();
  timer.Print();

}


void AddDetToGRP(AliDAQ::DetectorBits det, int run)
{
  // take raw GRP and the detector
  printf("Detector %d requested to be present in GRP\n",det);
  AliCDBManager* man = AliCDBManager::Instance();
  if (man->GetLock()) {
    man->Destroy();
    man = AliCDBManager::Instance();
  }
  man->UnsetDefaultStorage();
  man->SetDefaultStorage("raw://");
  man->SetRun(run);
  AliCDBEntry* grpe = man->Get("GRP/GRP/Data");
  AliGRPObject* grp = (AliGRPObject*)grpe->GetObject();
  int ndet = grp->GetNumberOfDetectors();
  UInt_t msk = grp->GetDetectorMask();
  if (!(msk&det)) {
    msk |= det; 
    grp->SetDetectorMask(msk); 
    grp->SetNumberOfDetectors(++ndet);
  }
  grpe->SetObject(0);
  man->UnsetDefaultStorage();
  man->SetDefaultStorage("local://");
  AliCDBMetaData* md = new AliCDBMetaData();
  AliCDBId id("GRP/GRP/Data",run,run);
  man->Put(grp,id,md);
  man->UnsetDefaultStorage();
  //
}
