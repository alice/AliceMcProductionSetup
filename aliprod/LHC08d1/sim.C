void sim(Int_t nev=150) {

  AliSimulation simulator;
  simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
  simulator.SetMakeDigitsFromHits("ITS TPC");
  //  simulator.SetWriteRawData("ALL","raw.root",kTRUE);
  simulator.SetDefaultStorage("alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetRunHLT("");
  simulator.SetRunQA(":");
  simulator.SetWriteGRPEntry(kFALSE);
 
  TStopwatch timer;
  timer.Start();
  simulator.Run(nev);
  timer.Stop();
  timer.Print();
}
