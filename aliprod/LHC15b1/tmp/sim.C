void sim(Int_t nev=150) {
	AliSimulation simulator;

	//	simulator.SetWriteRawData("ALL","raw.root",kFALSE);
	simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD AD");
	simulator.SetMakeDigitsFromHits("ITS TPC");
	//
	//
	// RAW OCDB
	simulator.SetDefaultStorage("alien://Folder=/alice/data/2013/OCDB");

	// Specific storages = 23 

	//
	// ITS  (1 Total)
	//     Alignment from Ideal OCDB 

	simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

	//
	// MUON (1 object)

	simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal"); 

	//
	// TPC (3 new objects for Run2 MC) 

	simulator.SetSpecificStorage("TPC/Calib/Parameters",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");
	simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");
	simulator.SetSpecificStorage("TPC/Calib/RecoParam",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");

	// TPC (5 old objects) 
	simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
	simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
	simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
	simulator.SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
	simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");


	//
	// EMCAL

        simulator.SetSpecificStorage("EMCAL/Align/Data"         ,"alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Calib/Data"         ,"alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Calib/Mapping"      ,"alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Calib/PeakFinder"   ,"alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Calib/Pedestals"    ,"alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Calib/RecoParam"    ,"alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Calib/SimParam"     ,"alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Calib/Trigger"      ,"alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Config/Preprocessor","alien://Folder=/alice/data/2015/OCDB");
        simulator.SetSpecificStorage("EMCAL/Config/Temperature" ,"alien://Folder=/alice/data/2015/OCDB");

	// PHOS
	// see https://alice.its.cern.ch/jira/browse/ALIROOT-5623?focusedCommentId=147957&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-147957
	simulator.SetSpecificStorage("PHOS/Align/Data", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("PHOS/Calib/EmcBadChannels", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("PHOS/Calib/EmcGainPedestals", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("PHOS/Calib/Mapping", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("PHOS/Calib/RecoParam", "alien://Folder=/alice/data/2015/OCDB");
	
	// AD
	simulator.SetSpecificStorage("AD/Align/Data", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("AD/Calib/Data", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("AD/Calib/LightYields", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("AD/Calib/PMGains", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("AD/Calib/RecoParam", "alien://Folder=/alice/data/2015/OCDB");
	simulator.SetSpecificStorage("AD/Calib/TimeDelays", "alien://Folder=/alice/data/2015/OCDB");


	// Vertex and magfield

	simulator.UseVertexFromCDB();
	simulator.UseMagFieldFromGRP();

	// PHOS simulation settings
	AliPHOSSimParam *simParam = AliPHOSSimParam::GetInstance();
	simParam->SetCellNonLineairyA(0.001);
	simParam->SetCellNonLineairyB(0.2);
	simParam->SetCellNonLineairyC(1.02);

	//
	// The rest
	//

        simulator.SetRunQA(":");

	printf("Before simulator.Run(nev);\n");
	simulator.Run(nev);
	printf("After simulator.Run(nev);\n");
}

