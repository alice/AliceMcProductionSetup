void sim(Int_t nev=800) 
{
  if ( 0) {
    TGeoGlobalMagField::Instance()->SetField(new AliMagF("Maps","Maps", -1., -1, AliMagF::k5kG));
  }

  AliSimulation simulator;
  simulator.SetRunQA("MUON:ALL");
  simulator.SetRunHLT("");

  if ( 1 )
  {
    simulator.SetMakeSDigits("MUON T0 VZERO FMD"); // T0 and VZERO for trigger efficiencies, FMD for diffractive studies
    simulator.SetMakeDigitsFromHits("ITS"); // ITS needed to propagate the simulated vertex
    simulator.SetMakeDigits("MUON T0 VZERO FMD");// ITS"); // ITS needed to propagate the simulated vertex
  }
  else
  {
    simulator.SetTriggerConfig("MUON");
    simulator.SetMakeSDigits("MUON");
    simulator.SetMakeDigits("MUON");// ITS"); // ITS needed to propagate the simulated vertex
  }
  

  simulator.SetDefaultStorage("raw://");
  
  if ( kFALSE )
  {
    simulator.SetCDBSnapshotMode("OCDB_sim.root");
  }
  
  if ( ! 0 ) {
    
    // MUON Tracker
    simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");

    simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");
    // Mag.field from OCDB
    simulator.UseMagFieldFromGRP();

    if ( 1 )
    {
      simulator.UseVertexFromCDB();
    }
  }
  
   // The rest
  TStopwatch timer;
  timer.Start();
  simulator.Run(nev);
  timer.Stop();
  timer.Print();
}
