void rec() {  
  AliReconstruction reco;

  reco.SetCleanESD(kFALSE);
  reco.SetCleanESD(kFALSE);
  reco.SetFillTriggerESD();
  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();
  reco.SetFractionFriends(.1);

  // ITS Efficiency and tracking errors
  reco.SetRunPlaneEff(kTRUE);
  reco.SetUseTrackingErrorsForAlignment("ITS");

  // RAW OCDB
  reco.SetDefaultStorage("alien://Folder=/alice/data/2013/OCDB");

  // OADB call  
  // from https://savannah.cern.ch/task/?39374#comment85
  //
  reco.SetOption("TPC","PID.OADB=TSPLINE3_MC_%s_LHC13B2_FIXn1_PASS1_PPB_MEAN"); 


  // ITS (2 objects)
  reco.SetSpecificStorage("ITS/Align/Data",          "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");  

  // MUON objects (1 object)
  reco.SetSpecificStorage("MUON/Align/Data",         "alien://folder=/alice/simulation/2008/v4-15-Release/Residual"); 

  // TPC (7 objects)
  reco.SetSpecificStorage("TPC/Align/Data",         "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/ClusterParam", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/RecoParam",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/TimeGain",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/AltroConfig",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/TimeDrift",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/Correction",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/"); 

  // customize ITS recoparam to overcome MC trigger problems
  {
    AliITSRecoParam * itsRecoParam = AliITSRecoParam::GetLowFluxParam();
    itsRecoParam->SetSkipSubdetsNotInTriggerCluster(kFALSE);
    itsRecoParam->SetClusterErrorsParam(2);

    // find independently ITS SA tracks 
    itsRecoParam->SetSAUseAllClusters();
    itsRecoParam->SetOuterStartLayerSA(2);

    itsRecoParam->SetAllowProlongationWithEmptyRoad(kTRUE);
    
    // larger seach windows for SA (in case of large misalignments)
    itsRecoParam->SetFactorSAWindowSizes(2);
    
    // Misalignment syst errors decided at ITS meeting 25.03.2010
    // Errors in Z reduced on 11.10.2010 for SPD and SDD
    // additional error due to misal (B off)
    itsRecoParam->SetClusterMisalErrorY(0.0010,0.0010,0.0300,0.0300,0.0020,0.0020); // [cm]
    itsRecoParam->SetClusterMisalErrorZ(0.0050,0.0050,0.0050,0.0050,0.1000,0.1000); // [cm]
    // additional error due to misal (B on)
    itsRecoParam->SetClusterMisalErrorYBOn(0.0010,0.0030,0.0500,0.0500,0.0020,0.0020); // [cm]
    itsRecoParam->SetClusterMisalErrorZBOn(0.0050,0.0050,0.0050,0.0050,0.1000,0.1000); // [cm]
    //----

    // SDD configuration 
    itsRecoParam->SetUseSDDCorrectionMaps(kTRUE); // changed 30.04.2010
    itsRecoParam->SetUseSDDClusterSizeSelection(kTRUE);
    itsRecoParam->SetMinClusterChargeSDD(30.);
    itsRecoParam->SetUseUnfoldingInClusterFinderSDD(kFALSE);

    // Plane Efficiency evaluation with tracklets Method
    itsRecoParam->SetIPlanePlaneEff(-1);
    itsRecoParam->SetComputePlaneEff(kTRUE,kFALSE);
    itsRecoParam->SetUseTrackletsPlaneEff(kTRUE);
    itsRecoParam->SetTrackleterPhiWindowL2(0.07);
    itsRecoParam->SetTrackleterZetaWindowL2(0.4);
    itsRecoParam->SetTrackleterPhiWindowL1(0.10);
    itsRecoParam->SetTrackleterZetaWindowL1(0.6);
    itsRecoParam->SetUpdateOncePerEventPlaneEff(kTRUE);
    itsRecoParam->SetMinContVtxPlaneEff(3);
    // itsRecoParam->SetOptTrackletsPlaneEff(kTRUE); // activate it for MC (very important) !
    // Removal of tracklets reconstructed in the SPD overlaps 
    // 
    itsRecoParam->SetTrackleterRemoveClustersFromOverlaps(kTRUE);
    itsRecoParam->SetTrackleterPhiWindow(0.08);
    itsRecoParam->SetTrackleterThetaWindow(0.025);
    itsRecoParam->SetTrackleterScaleDThetaBySin2T(kTRUE);
    //
    // V0 finder (A. Marin)
    itsRecoParam->GetESDV0Params()->SetMaxPidProbPionForb(0.9);

    //******************************************************************

    itsRecoParam->SetEventSpecie(AliRecoParam::kLowMult);
    itsRecoParam->SetTitle("LowMult");
    reco.SetRecoParam("ITS",itsRecoParam);
  }
  reco.SetRunQA(":");
  reco.Run();
}
