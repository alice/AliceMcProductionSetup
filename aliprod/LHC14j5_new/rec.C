void rec() {

  gSystem->Load("libITSUpgradeBase.so");
  gSystem->Load("libITSUpgradeSim.so");
  gSystem->Load("libITSUpgradeRec.so");
  // Set ITS upgrade reconstructor

  gPluginMgr->AddHandler("AliReconstructor", "ITS",
                         "AliITSUReconstructor","ITS", "AliITSUReconstructor()");

  AliReconstruction *reco = new AliReconstruction("galice.root");
  
  // switch off cleanESD, write ESDfriends and Alignment data
  
  reco->SetCleanESD(kTRUE);
  //  reco->SetCleanESD(kFALSE);
  reco->SetWriteESDfriend();
  //  reco->SetWriteESDfriend(kFALSE);
  reco->SetWriteAlignmentData();
  reco->SetFractionFriends(.1);
  reco->SetRunQA(":");

  reco->SetDefaultStorage("alien://folder=/alice/data/2010/OCDB");   // Raw OCDB
  
  reco->SetRecoParam("ZDC",AliZDCRecoParamPbPb::GetHighFluxParam(2760)); 

  // ITS (2 objects)
  reco->SetSpecificStorage("ITS/Align/Data",      "alien://folder=/alice/simulation/LS1_upgrade/Ideal");
  reco->SetSpecificStorage("ITS/Calib/RecoParam", "alien://folder=/alice/simulation/LS1_upgrade/Ideal");

  // MUON Tracker
  reco->SetSpecificStorage("MUON/Align/Data",      "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco->SetSpecificStorage("MUON/Calib/RecoParam", "alien://folder=/alice/cern.ch/user/a/auras/OCDB/");
  reco->SetSpecificStorage("MFT/Align/Data",	   "alien://folder=/alice/cern.ch/user/a/auras/OCDB/");
  reco->SetSpecificStorage("MFT/Calib/RecoParam",  "alien://folder=/alice/cern.ch/user/a/auras/OCDB/");

  reco->SetOption("MUON MFT","SAVEDIGITS");

  // TPC (7 objects)      
  reco->SetSpecificStorage("TPC/Align/Data",         "alien://folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco->SetSpecificStorage("TPC/Calib/ClusterParam", "alien://folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco->SetSpecificStorage("TPC/Calib/RecoParam",    "alien://folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco->SetSpecificStorage("TPC/Calib/TimeGain",     "alien://folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco->SetSpecificStorage("TPC/Calib/AltroConfig",  "alien://folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco->SetSpecificStorage("TPC/Calib/TimeDrift",    "alien://folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco->SetSpecificStorage("TPC/Calib/Correction",   "alien://folder=/alice/simulation/2008/v4-15-Release/Residual/"); 

  reco->SetOption("TPC", "useRAW");

  // GRP from local OCDB
  reco->SetSpecificStorage("GRP/GRP/Data",Form("local://%s",gSystem->pwd()));

  reco->SetStopOnError(kFALSE);
  reco->SetRunVertexFinder(kFALSE); // to be implemented - CreateVertexer
  reco->SetRunMultFinder(kFALSE);   // to be implemented - CreateMultFinder
  reco->SetRunPlaneEff(kFALSE);     // to be implemented - CreateTrackleter

  AliSysInfo::SetVerbose(kTRUE);

  TStopwatch timer;
  timer.Start();
  reco->Run();
  timer.Stop();
  timer.Print();

  delete reco;

}
