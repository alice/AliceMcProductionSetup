void sim(Int_t nev=200) {
  
  AliSimulation simulator;
  simulator.SetMakeDigits("MUON ITS VZERO T0 FMD");
  simulator.SetMakeSDigits("MUON VZERO T0 FMD");
  simulator.SetMakeDigitsFromHits("ITS");
  simulator.SetRunQA("MUON:ALL");
  simulator.SetRunHLT("");

  // raw OCDB
  simulator.SetDefaultStorage("alien://folder=/alice/data/2013/OCDB");
  // MUON tracker
  simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  // mean vertex from raw OCDB
  //simulator.SetSpecificStorage("GRP/Calib/MeanVertex","alien://folder=/alice/data/2013/OCDB");

  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();
  
  TStopwatch timer;
  timer.Start();
  simulator.Run(nev);
  simulator.RunTrigger("","MUON ITS VZERO");
  timer.Stop();
  timer.Print();
}
