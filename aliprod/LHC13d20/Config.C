//
// Configuration for the first physics production 2008
//

// One can use the configuration macro in compiled mode by
// root [0] gSystem->Load("libgeant321");
// root [0] gSystem->SetIncludePath("-I$ROOTSYS/include -I$ALICE_ROOT/include\
//                   -I$ALICE_ROOT -I$ALICE/geant3/TGeant3");
// root [0] .x grun.C(1,"Config.C++")

#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TRandom.h>
#include <TDatime.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include "STEER/AliRunLoader.h"
#include "STEER/AliRun.h"
#include "STEER/AliConfig.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "PYTHIA6/AliGenPythia.h"
#include "TDPMjet/AliGenDPMjet.h"
#include "STEER/AliMagFCheb.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "ITS/AliITSv11.h"
#include "TPC/AliTPCv2.h"
#include "TOF/AliTOFv6T0.h"
#include "HMPID/AliHMPIDv3.h"
#include "ZDC/AliZDCv3.h"
#include "TRD/AliTRDv1.h"
#include "TRD/AliTRDgeometry.h"
#include "FMD/AliFMDv1.h"
#include "MUON/AliMUONv1.h"
#include "PHOS/AliPHOSv1.h"
#include "PHOS/AliPHOSSimParam.h"
#include "PMD/AliPMDv1.h"
#include "T0/AliT0v1.h"
#include "EMCAL/AliEMCALv2.h"
#include "ACORDE/AliACORDEv1.h"
#include "VZERO/AliVZEROv7.h"
#endif


enum PDC06Proc_t 
{
  kPythia6, kPythia6D6T, kPythia6ATLAS, kPythia6ATLAS_Flat, kPythiaPerugia0, kPhojet, kPythiaPerugia0DiffTune, kPythiaPerugia0DiffFlatZ, kPhojetDiffTune, kRunMax
};

const char * pprRunName[] = {
  "kPythia6", "kPythia6D6T", "kPythia6ATLAS", "kPythia6ATLAS_Flat", "kPythiaPerugia0", "kPhojet" , "kPythiaPerugia0DiffTune","kPythiaPerugia0DiffFlatZ","kPhojetDiffTune"
};

enum Mag_t
{
  kNoField, k5kG, kFieldMax
};

const char * pprField[] = {
  "kNoField", "k5kG"
};


enum PprTrigConf_t
{
    kDefaultPPTrig, kDefaultPbPbTrig
};

const char * pprTrigConfName[] = {
    "p-p","Pb-Pb"
};

//--- Functions ---
class AliGenPythia;
AliGenerator *MbPythia();
AliGenerator *MbPythiaTuneD6T();
AliGenerator *MbPhojet();
AliGenerator *MbPythiaRetunePerugia0();
AliGenerator* MbPhojetRetune();
AliGenerator* MBPythiaRetunePerugia0FlatZ();

void ProcessEnvironmentVars();

// Geterator, field, beam energy
static PDC06Proc_t   proc     = kPhojet;
static Mag_t         mag      = k5kG;
static Float_t       energy   = 10000; // energy in CMS
static Int_t         runNumber = 0;
static PprTrigConf_t strig = kDefaultPPTrig; // default pp trigger configuration
//========================//
// Set Random Number seed //
//========================//
TDatime dt;
static UInt_t seed    = dt.Get();

// Comment line
static TString comment;

void Config()
{
    

  // Get settings from environment variables
  ProcessEnvironmentVars();

  gRandom->SetSeed(seed);
  cerr<<"Seed for random number generation= "<<seed<<endl; 

  // Libraries required by geant321
#if defined(__CINT__)
  gSystem->Load("liblhapdf");      // Parton density functions
  gSystem->Load("libEGPythia6");   // TGenerator interface
  if (proc == kPythia6 || proc == kPhojet) {
    gSystem->Load("libpythia6");        // Pythia 6.2
  } else {
    gSystem->Load("libpythia6.4.21");   // Pythia 6.4
  }
  gSystem->Load("libAliPythia6");  // ALICE specific implementations
  gSystem->Load("libgeant321");
  gSystem->Load("libITSbase.so");
  gSystem->Load("libITSsim.so");  
  gSystem->Load("libITSrec.so");
  gSystem->Load("libITSUpgradeBase.so");
  gSystem->Load("libITSUpgradeSim.so");  
  gSystem->Load("libITSUpgradeRec.so");
#endif

  new TGeant3TGeo("C++ Interface to Geant3");

  //=======================================================================
  //  Create the output file

   
  AliRunLoader* rl=0x0;

  cout<<"Config.C: Creating Run Loader ..."<<endl;
  rl = AliRunLoader::Open("galice.root",
			  AliConfig::GetDefaultEventFolderName(),
			  "recreate");
  if (rl == 0x0)
    {
      gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
      return;
    }
  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(1000);
  gAlice->SetRunLoader(rl);
  // gAlice->SetGeometryFromFile("geometry.root");
  // gAlice->SetGeometryFromCDB();
  
  // Set the trigger configuration: proton-proton

   AliSimulation::Instance()->SetTriggerConfig(pprTrigConfName[strig]);
   cout <<"Trigger configuration is set to  "<<pprTrigConfName[strig]<<endl;
  //
  //=======================================================================
  // ************* STEERING parameters FOR ALICE SIMULATION **************
  // --- Specify event type to be tracked through the ALICE setup
  // --- All positions are in cm, angles in degrees, and P and E in GeV


    gMC->SetProcess("DCAY",1);
    gMC->SetProcess("PAIR",1);
    gMC->SetProcess("COMP",1);
    gMC->SetProcess("PHOT",1);
    gMC->SetProcess("PFIS",0);
    gMC->SetProcess("DRAY",0);
    gMC->SetProcess("ANNI",1);
    gMC->SetProcess("BREM",1);
    gMC->SetProcess("MUNU",1);
    gMC->SetProcess("CKOV",1);
    gMC->SetProcess("HADR",1);
    gMC->SetProcess("LOSS",2);
    gMC->SetProcess("MULS",1);
    gMC->SetProcess("RAYL",1);

    Float_t cut = 1.e-3;        // 1MeV cut by default
    Float_t tofmax = 1.e10;

    gMC->SetCut("CUTGAM", cut);
    gMC->SetCut("CUTELE", cut);
    gMC->SetCut("CUTNEU", cut);
    gMC->SetCut("CUTHAD", cut);
    gMC->SetCut("CUTMUO", cut);
    gMC->SetCut("BCUTE",  cut); 
    gMC->SetCut("BCUTM",  cut); 
    gMC->SetCut("DCUTE",  cut); 
    gMC->SetCut("DCUTM",  cut); 
    gMC->SetCut("PPCUTM", cut);
    gMC->SetCut("TOFMAX", tofmax); 




  //======================//
  // Set External decayer //
  //======================//
  TVirtualMCDecayer* decayer = new AliDecayerPythia();
  decayer->SetForceDecay(kAll);
  decayer->Init();
  gMC->SetExternalDecayer(decayer);

  //=========================//
  // Generator Configuration //
  //=========================//
  AliGenerator* gener = 0x0;
  
  if (proc == kPythia6) {
      gener = MbPythia();
  } else if (proc == kPythia6D6T) {
      gener = MbPythiaTuneD6T();
  } else if (proc == kPythia6ATLAS) {
      gener = MbPythiaTuneATLAS();
  } else if (proc == kPythiaPerugia0) {
      gener = MbPythiaTunePerugia0();
  } else if (proc == kPythia6ATLAS_Flat) {
      gener = MbPythiaTuneATLAS_Flat();
  } else if (proc == kPhojet) {
      gener = MbPhojet();
  }
  else if (proc == kPythiaPerugia0DiffTune) {
      gener = MbPythiaRetunePerugia0();
  }
  else if (proc == kPythiaPerugia0DiffFlatZ) {
      gener = MbPythiaRetunePerugia0FlatZ();
  }
  else if (proc == kPhojetDiffTune) {
      gener = MbPhojetRetune();
  }
  
  
  //
  //
  // Size of the interaction diamond
  // Longitudinal
  Float_t sigmaz  = 100.0 / TMath::Sqrt(2.); // [cm]
    
  //
  // Transverse

  // beta*
  Float_t betast                  = 10.0;  // beta* [m]
  if (runNumber >= 117048) betast =  2.0;
  if (runNumber >  122375) betast =  3.5;  // starting with fill 1179
  //	
  //
  Float_t eps     = 5.0e-6;            // emittance [m]
  Float_t gamma   = energy / 2.0 / 0.938272;  // relativistic gamma [1]
  Float_t sigmaxy = TMath::Sqrt(eps * betast / gamma) / TMath::Sqrt(2.) * 100.;  // [cm]
  printf("\n \n Diamond size x-y: %10.3e z: %10.3e\n \n", sigmaxy, sigmaz);
    
  gener->SetSigma(sigmaxy, sigmaxy, sigmaz);      // Sigma in (X,Y,Z) (cm) on IP position
  gener->SetVertexSmear(kPerEvent);
  gener->Init();

  printf("\n \n Comment: %s \n \n", comment.Data());

  rl->CdGAFile();
  
  Int_t iABSO  = 1;
  Int_t iACORDE= 0;
  Int_t iDIPO  = 1;
  Int_t iEMCAL = 1;
  Int_t iFMD   = 1;
  Int_t iFRAME = 1;
  Int_t iHALL  = 1;
  Int_t iITS   = 1;
  Int_t iMAG   = 1;
  Int_t iMUON  = 1;
  Int_t iPHOS  = 1;
  Int_t iPIPE  = 1;
  Int_t iPMD   = 1;
  Int_t iHMPID = 1;
  Int_t iSHIL  = 1;
  Int_t iT0    = 1;
  Int_t iTOF   = 1;
  Int_t iTPC   = 1;
  Int_t iTRD   = 1;
  Int_t iVZERO = 1;
  Int_t iZDC   = 1;
  

    //=================== Alice BODY parameters =============================
    AliBODY *BODY = new AliBODY("BODY", "Alice envelop");


    if (iMAG)
    {
        //=================== MAG parameters ============================
        // --- Start with Magnet since detector layouts may be depending ---
        // --- on the selected Magnet dimensions ---
        AliMAG *MAG = new AliMAG("MAG", "Magnet");
    }


    if (iABSO)
    {
        //=================== ABSO parameters ============================
        AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
    }

    if (iDIPO)
    {
        //=================== DIPO parameters ============================

        AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
    }

    if (iHALL)
    {
        //=================== HALL parameters ============================

        AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
    }


    if (iFRAME)
    {
        //=================== FRAME parameters ============================

        AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
	FRAME->SetHoles(1);
    }

    if (iSHIL)
    {
        //=================== SHIL parameters ============================

        AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
    }


    if (iPIPE)
    {
        //=================== PIPE parameters ============================

        AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
    }
 
    if (iITS)
    {
        //=================== ITS parameters ============================

	AliITS *ITS  = new AliITSv11("ITS","ITS v11");
    }

    if (iTPC)
    {
      //============================ TPC parameters =====================

        AliTPC *TPC = new AliTPCv2("TPC", "Default");
    }


    if (iTOF) {
        //=================== TOF parameters ============================

	AliTOF *TOF = new AliTOFv6T0("TOF", "normal TOF");
    }


    if (iHMPID)
    {
        //=================== HMPID parameters ===========================

        AliHMPID *HMPID = new AliHMPIDv3("HMPID", "normal HMPID");

    }


    if (iZDC)
    {
        //=================== ZDC parameters ============================
	
      	AliZDC *ZDC = new AliZDCv3("ZDC", "normal ZDC");
        //Collimators aperture
        ZDC->SetVCollSideCAperture(0.85);
        ZDC->SetVCollSideCCentre(0.);
        ZDC->SetVCollSideAAperture(0.75);
        ZDC->SetVCollSideACentre(0.);
        //Detector position
        ZDC->SetYZNC(1.6);
        ZDC->SetYZNA(1.6);
        ZDC->SetYZPC(1.6);
        ZDC->SetYZPA(1.6);
    }

    if (iTRD)
    {
        //=================== TRD parameters ============================

        AliTRD *TRD = new AliTRDv1("TRD", "TRD slow simulator");
        AliTRDgeometry *geoTRD = TRD->GetGeometry();
	// Partial geometry: modules at 0,1,7,8,9,16,17
	// starting at 3h in positive direction
	geoTRD->SetSMstatus(2,0);
	geoTRD->SetSMstatus(3,0);
	geoTRD->SetSMstatus(4,0);
        geoTRD->SetSMstatus(5,0);
	geoTRD->SetSMstatus(6,0);
        geoTRD->SetSMstatus(11,0);
        geoTRD->SetSMstatus(12,0);
        geoTRD->SetSMstatus(13,0);
        geoTRD->SetSMstatus(14,0);
        geoTRD->SetSMstatus(15,0);
        geoTRD->SetSMstatus(16,0);
    }

    if (iFMD)
    {
        //=================== FMD parameters ============================

	AliFMD *FMD = new AliFMDv1("FMD", "normal FMD");
   }

    if (iMUON)
    {
        //=================== MUON parameters ===========================
        // New MUONv1 version (geometry defined via builders)
	AliMUON *MUON = new AliMUONv1("MUON", "default");
	// activate trigger efficiency by cells
	MUON->SetTriggerEffCells(1);
    }

    if (iPHOS)
    {
        //=================== PHOS parameters ===========================

     AliPHOS *PHOS = new AliPHOSv1("PHOS", "noCPV_Modules123");

    }


    if (iPMD)
    {
        //=================== PMD parameters ============================

        AliPMD *PMD = new AliPMDv1("PMD", "normal PMD");
    }

    if (iT0)
    {
        //=================== T0 parameters ============================
        AliT0 *T0 = new AliT0v1("T0", "T0 Detector");
    }

    if (iEMCAL)
    {
        //=================== EMCAL parameters ============================

      //        AliEMCAL *EMCAL = new AliEMCALv2("EMCAL", "EMCAL_FIRSTYEAR");
        AliEMCAL *EMCAL = new AliEMCALv1("EMCAL", "EMCAL_FIRSTYEARV1");

    }

     if (iACORDE)
    {
        //=================== ACORDE parameters ============================

        AliACORDE *ACORDE = new AliACORDEv1("ACORDE", "normal ACORDE");
    }

     if (iVZERO)
    {
        //=================== ACORDE parameters ============================

        AliVZERO *VZERO = new AliVZEROv7("VZERO", "normal VZERO");
    }
}
//
//           PYTHIA
//

AliGenerator* MbPythia()
{
      comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1); 
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
      
      return pythia;
}

AliGenerator* MbPythiaTuneD6T()
{
      comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1); 
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    109     D6T : Rick Field's CDF Tune D6T (NB: needs CTEQ6L pdfs externally)
      pythia->SetTune(109); // F I X 
      pythia->SetStrucFunc(kCTEQ6l);
//
      return pythia;
}

AliGenerator* MbPythiaTunePerugia0()
{
      comment = comment.Append(" pp: Pythia low-pt (Perugia0)");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1); 
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320); 
      pythia->UseNewMultipleInteractionsScenario();
      pythia->SetCrossingAngle(0,0.000280);

//
      return pythia;
}

AliGenerator* MbPythiaRetunePerugia0()
{
      comment = comment.Append(" pp: Pythia low-pt (Perugia0) diff tune");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1); 
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320); 
      pythia->UseNewMultipleInteractionsScenario();
      pythia->SetCrossingAngle(0,0.000280);
      pythia->SetTuneForDiff();
//
      return pythia;
}

AliGenerator* MbPythiaRetunePerugia0FlatZ()
{
      comment = comment.Append(" pp: Pythia low-pt (Perugia0) diff tune with flat Z distribution");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1); 
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320); 
      pythia->UseNewMultipleInteractionsScenario();
      pythia->SetCrossingAngle(0,0.000280);
      pythia->SetCutVertexZ(0.6);
      pythia->SetTuneForDiff();
//
      return pythia;
}


AliGenerator* MbPythiaTuneATLAS()
{
      comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1); 
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    C   306 ATLAS-CSC: Arthur Moraes' (new) ATLAS tune (needs CTEQ6L externally)
      pythia->SetTune(306);
      pythia->SetStrucFunc(kCTEQ6l);
//
      return pythia;
}

AliGenerator* MbPythiaTuneATLAS_Flat()
{
  Double_t cont900[] = 
    {0, 
     0.122867, 0.0467072, -0.00627787, -0.00313533, 0.000365169, 0.00461525, 0.00905571, 0.0115782, 0.0153643, 0.0174519,
     0.0165793, 0.0159452, 0.0157922, 0.0131965, 0.0104403, 0.010058, 0.0117962, 0.0107275, 0.00877908, 0.00816277, 
     0.00725234, 0.00857953, 0.0106741, 0.0101365, 0.00789826, 0.00777313, 0.00912233, 0.00981399, 0.00875476, 0.00903165, 
     0.00814042, 0.008889, 0.0086213, 0.00761283, 0.0080958, 0.0106609, 0.0102939, 0.00934675, 0.0112591, 0.0076591,
     0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 
     0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 
     0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 
     0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 0.00879798, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0};

  Double_t err900[] =
    {0, 
     0.000764126, 0.000290835, 0.000379656, 0.000333575, 0.000296718, 0.000283571, 0.000283658, 0.000300263, 0.000341663, 0.000385709, 
     0.000439665, 0.000495392, 0.000553816, 0.000591794, 0.000631109, 0.000657317, 0.000700036, 0.000715527, 0.000750986, 0.000756709, 
     0.000767991, 0.000785745, 0.000816073, 0.000838953, 0.000859145, 0.000879126, 0.000899835, 0.000929581, 0.000948717, 0.000992716, 
     0.000988994, 0.00104928, 0.00103572, 0.00108203, 0.00108221, 0.00117097, 0.00117541, 0.00127544, 0.00134156, 0.00132726, 
     0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 
     0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 
     0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 
     0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 0.000457271, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0};

  Double_t cont7000[] =
    {0, 
     0.124006, 0.0449258, -0.0113921, -0.00464723, 0.000376562, 0.00382703, 0.00730431, 0.0113167, 0.0148601, 0.0168044, 
     0.0174948, 0.0171911, 0.0155283, 0.0133439, 0.013508, 0.0108804, 0.0102104, 0.00855802, 0.010599, 0.010279, 
     0.00660211, 0.00863472, 0.00937699, 0.0088311, 0.00809214, 0.00854984, 0.00688991, 0.00663105, 0.00519294, 0.00678956, 
     0.00764158, 0.00785182, 0.00839256, 0.00494343, 0.00531117, 0.00693826, 0.0076254, 0.0068545, 0.00661081, 0.00581549, 
     0.00724929, 0.00825396, 0.00542933, 0.00615563, 0.00456606, 0.00593181, 0.00934908, 0.00782482, 0.00814794, 0.00454949, 
     0.00408373, 0.00793694, 0.00733509, 0.00560431, 0.00917909, 0.00675468, 0.00505163, 0.00510786, 0.0052046, 0.00721047,
     0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 
     0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 
     0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 
     0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 0.00676547, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0};

  Double_t err7000[] =
    {0, 
     0.00084098, 0.00030437, 0.000420504, 0.000355134, 0.00031745, 0.0003039, 0.000307702, 0.000332444, 0.0003751, 0.000433704, 
     0.000503045, 0.000577022, 0.000645837, 0.000709, 0.000778967, 0.000810835, 0.000880097, 0.000882641, 0.000969397, 0.000960003, 
     0.00102364, 0.00103852, 0.00107975, 0.00110806, 0.00114894, 0.00118038, 0.00119514, 0.00122909, 0.0012108, 0.00126209, 
     0.00124214, 0.00131174, 0.00131256, 0.00133419, 0.0013493, 0.00135517, 0.00138469, 0.00139953, 0.00143463, 0.00142857, 
     0.00148453, 0.00148785, 0.00151182, 0.00154541, 0.00150113, 0.00156888, 0.0015722, 0.00162636, 0.0016941, 0.00165147, 
     0.00171392, 0.00170122, 0.00173585, 0.00175372, 0.00186916, 0.0017961, 0.00193129, 0.00182528, 0.00191836, 0.00187835,
     0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 
     0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 
     0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 
     0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 0.000442637, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0};

    



      AliGenPythia* pythia = MbPythiaTuneATLAS();
      
      comment = comment.Append("; flat multiplicity distribution");
      
      // set high multiplicity trigger
      // this weight achieves a flat multiplicity distribution
      TH1 *weight = new TH1D("weight","weight",120,-0.5,119.5);
      if (energy==900) {
	weight->SetContent(cont900);
	weight->SetError(err900);
      }
      else {
	weight->SetContent(cont7000);
	weight->SetError(err7000);
      }
        
      Int_t limit = weight->GetRandom();
      pythia->SetTriggerChargedMultiplicity(limit, 1.4);
      
      comment = comment.Append(Form("; multiplicity threshold set to %d in |eta| < 1.4", limit));

      return pythia;
}

AliGenerator* MbPhojet()
{
      comment = comment.Append(" pp: Pythia low-pt");
//
//    DPMJET
#if defined(__CINT__)
  gSystem->Load("libdpmjet");      // Parton density functions
  gSystem->Load("libTDPMjet");      // Parton density functions
#endif
      AliGenDPMjet* dpmjet = new AliGenDPMjet(-1);
      dpmjet->SetMomentumRange(0, 999999.);
      dpmjet->SetThetaRange(0., 180.);
      dpmjet->SetYRange(-12.,12.);
      dpmjet->SetPtRange(0,1000.);
      dpmjet->SetProcess(kDpmMb);
      dpmjet->SetEnergyCMS(energy);
      dpmjet->SetCrossingAngle(0,0.000280);
      return dpmjet;
}


AliGenerator* MbPhojetRetune()
{
      comment = comment.Append(" pp: Phojet diff tune");
//
//    DPMJET
#if defined(__CINT__)
  gSystem->Load("libdpmjet");      // Parton density functions
  gSystem->Load("libTDPMjet");      // Parton density functions
#endif

      AliGenDPMjet* dpmjet = new AliGenDPMjet(-1); 
      dpmjet->SetMomentumRange(0, 999999.);
      dpmjet->SetThetaRange(0., 180.);
      dpmjet->SetYRange(-12.,12.);
      dpmjet->SetPtRange(0,1000.);
      dpmjet->SetProcess(kDpmMb);
      dpmjet->SetEnergyCMS(energy);
      dpmjet->SetCrossingAngle(0,0.000280);
      dpmjet->SetTuneForDiff();

      return dpmjet;
}




void ProcessEnvironmentVars()
{
    // Run type
    if (gSystem->Getenv("CONFIG_RUN_TYPE")) {
      for (Int_t iRun = 0; iRun < kRunMax; iRun++) {
	if (strcmp(gSystem->Getenv("CONFIG_RUN_TYPE"), pprRunName[iRun])==0) {
	  proc = (PDC06Proc_t)iRun;
	  cout<<"Run type set to "<<pprRunName[iRun]<<endl;
	}
      }
    }

    // Field
    if (gSystem->Getenv("CONFIG_FIELD")) {
      for (Int_t iField = 0; iField < kFieldMax; iField++) {
	if (strcmp(gSystem->Getenv("CONFIG_FIELD"), pprField[iField])==0) {
	  mag = (Mag_t)iField;
	  cout<<"Field set to "<<pprField[iField]<<endl;
	}
      }
    }

    // Energy
    if (gSystem->Getenv("CONFIG_ENERGY")) {
      energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
      cout<<"Energy set to "<<energy<<" GeV"<<endl;
    }

    // Random Number seed
    if (gSystem->Getenv("CONFIG_SEED")) {
      seed = atoi(gSystem->Getenv("CONFIG_SEED"));
    }

    // Run number
    if (gSystem->Getenv("DC_RUN")) {
      runNumber = atoi(gSystem->Getenv("DC_RUN"));
    }
}
