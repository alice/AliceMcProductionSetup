#!/bin/sh
##################################################
validateout=`dirname $0`
validatetime=`date`
validated="0";
error=1
if [ -z $validateout ]
then
    validateout="."
fi

cd $validateout;
validateworkdir=`pwd`;

echo "*******************************************************" >> stdout;
echo "* AliRoot Validation Script V1.0                      *" >> stdout;
echo "* Time:    $validatetime " >> stdout;
echo "* Dir:     $validateout" >> stdout;
echo "* Workdir: $validateworkdir" >> stdout;
echo "* ----------------------------------------------------*" >> stdout;
echo "* Library path: $LD_LIBRARY_PATH" >> stdout;
echo "* ----------------------------------------------------*" >> stdout;
echo "* Path: $PATH" >> stdout;
echo "* ----------------------------------------------------*" >> stdout;
ls -la ./ >> stdout;
echo "* ----------------------------------------------------*" >> stdout;

#rm aod.log
##################################################
if [ -f rec.log ] && [ -f sim.log ] 
then
sv=`grep -i  "Segmentation violation" *.log`
if [ "$sv" = "" ]
    then
    sf=`grep -i  "Segmentation fault" *.log`
    if [ "$sf" = "" ]
        then
        be=`grep -i  "Bus error" *.log`
        if [ "$be" = "" ]
            then
            ab=`grep -i "Abort" *.log`
            if [ "$ab" = "" ]
                then
                fp=`grep -i  "Floating point exception" *.log`
                if [ "$fp" = "" ]
                    then
                    kl=`grep -i  "Killed" *.log | grep -v "Particle"`
                    if [ "$kl" = "" ]
                        then
                        bf=`grep -i "busy flag cleared" *.log`
                        if [ "$bf" = "" ]
                           then
                                echo "* ----------------   Job Validated  ------------------*" >> stdout;
                                error="0";
                           fi
                        else
                            echo "* #             Check Macro failed !                  #" >> stdout;
                    fi;
                fi
            fi
        fi
    fi
fi
else
    echo "* ########## Job not validated - no rec.log or sim.log ###" >> stdout;
    echo "* ########## Removing all ROOT files from the local directory, leaving only the logs ###" >> stdout;
    rm -rf *.root
fi
if [ "$error" = "1" ]
    then
    echo "* ################   Job not validated ################" >> stdout;
fi
echo "* ----------------------------------------------------*" >> stdout;
echo "*******************************************************" >> stdout;
sleep 15;
cd -
exit $error

