// #define VERBOSEARGS
// simrun.C
{
// set job and simulation variables as :
// root.exe -b -q simrun.C  
//	--run <x> --event <y> --aliencounter <counter>
//	--process <kPyJetJet/kPyJetJetD6T/kPytJetJetPerugia0> 
//	--field <kNoField/k5kG>   // not used, taken from CDB
//	--energy <900/7000> 
//	--pthardbin <bin> 
//	--quench <0/1> --qhat <>

  int nrun = 0;
  int nevent = 0;
  int seed = 0;
  int naliencounter = 0;

  char sseed[1024];
  char srun[1024];
  char sevent[1024];
  char saliencounter[1024];
  char sprocess[1024];
  char sfield[1024];
  char senergy[1024];
  char sminpthard[1024];
  char smaxpthard[1024];
  char squench[1024];
  char sqhat[1024];

  Int_t npthardbin = 0;
  const Int_t nPtHardBins = 11;
  const Int_t ptHardLo[nPtHardBins] = { 0, 5,11,21,36,57, 84,117,152,191,234};
  const Int_t ptHardHi[nPtHardBins] = { 5,11,21,36,57,84,117,152,191,234,-1};
  const Int_t nPtHardBinsEMCAL = 7;
  const Int_t nPtHardBinsPHOS  = 5; 

  sprintf(srun,"");
  sprintf(sevent,"");
  sprintf(saliencounter,"");
  sprintf(sprocess,"");
  sprintf(sfield,"");
  sprintf(senergy,"");
  sprintf(sminpthard,"");
  sprintf(smaxpthard,"");
  sprintf(squench,"");
  sprintf(sqhat,"");

  for (int i=0; i< gApplication->Argc();i++){
    #ifdef VERBOSEARGS
    printf("Arg  %d:  %s\n",i,gApplication->Argv(i));
    #endif

    if (!(strcmp(gApplication->Argv(i),"--run")))
      nrun = atoi(gApplication->Argv(i+1));
    sprintf(srun,"%d",nrun);

    if (!(strcmp(gApplication->Argv(i),"--event")))
      nevent = atoi(gApplication->Argv(i+1));
    sprintf(sevent,"%d",nevent);

    if (!(strcmp(gApplication->Argv(i),"--aliencounter")))
      naliencounter = atoi(gApplication->Argv(i+1));
    sprintf(saliencounter,"%d",naliencounter);

    if (!(strcmp(gApplication->Argv(i),"--process")))
      sprintf(sprocess, gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--field")))
      sprintf(sfield,gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--energy")))
      sprintf(senergy,gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--pthardbin")))
      npthardbin = atoi(gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--minhard")))
      sprintf(sminpthard,gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--maxhard")))
      sprintf(smaxpthard,gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--minpt")))
      sprintf(sminptgammapi0,gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--quench")))
      sprintf(squench,gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--qhat")))
      sprintf(sqhat,gApplication->Argv(i+1));

  }

  if(!(strcmp(sminpthard,""))){
    sprintf(sminpthard,"%d",ptHardLo[npthardbin%nPtHardBins]);
    if(strstr(sprocess,"EMCAL")) sprintf(sminpthard,"%d",ptHardLo[npthardbin%nPtHardBinsEMCAL]);
    if(strstr(sprocess,"PHOS")) sprintf(sminpthard,"%d",ptHardLo[npthardbin%nPtHardBinsPHOS]);
    Printf(">>> MinPtHard %s %d",sminpthard,npthardbin%nPtHardBins);
  }
  if(!(strcmp(smaxpthard,""))){
    sprintf(smaxpthard,"%d",ptHardHi[npthardbin%nPtHardBins]);
    if(strstr(sprocess,"EMCAL")){
          sprintf(smaxpthard,"%d",ptHardHi[npthardbin%nPtHardBinsEMCAL]);
          if(npthardbin==nPtHardBinsEMCAL) sprintf(smaxpthard,"-1");
          if(npthardbin>nPtHardBinsEMCAL){
             Printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
             Printf("!!! WARNING! pt hard bin %d > max pt hard bin for EMCAL %d   !!!",npthardbin,nPtHardBinsEMCAL);
             Printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
          }
    }
    else if(strstr(sprocess,"PHOS")){
          sprintf(smaxpthard,"%d",ptHardHi[npthardbin%nPtHardBinsPHOS]);
          if(npthardbin==nPtHardBinsPHOS) sprintf(smaxpthard,"-1");
          if(npthardbin>nPtHardBinsPHOS){
             Printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
             Printf("!!! WARNING! pt hard bin %d > max pt hard bin for PHOS %d    !!!",npthardbin,nPtHardBinsPHOS);
             Printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
          }
    }
    Printf(">>> MaxPtHard %s %d",smaxpthard,npthardbin%nPtHardBins);

  }

  seed = (npthardbin+1) * nrun + naliencounter;
  sprintf(sseed,"%d",seed);

  if (seed==0) {
    fprintf(stderr,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    fprintf(stderr,"!!!!  WARNING! Seeding variable for MC is 0          !!!!\n");
    fprintf(stderr,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  } else {
    fprintf(stdout,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    fprintf(stdout,"!!!  MC Seed is %d \n",seed);
    fprintf(stdout,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  }
  
// set the environment variables
  gSystem->Setenv("CONFIG_SEED",sseed);
  gSystem->Setenv("CONFIG_RUN_TYPE",sprocess); // kPyJetJet
  gSystem->Setenv("CONFIG_FIELD",sfield);      // kNoField or k5kG
  gSystem->Setenv("CONFIG_ENERGY",senergy);    // 900 or 7000 (GeV)
  gSystem->Setenv("CONFIG_PTHARDMIN", sminpthard);
  gSystem->Setenv("CONFIG_PTHARDMAX", smaxpthard);
  gSystem->Setenv("CONFIG_QUENCHING", squench);
  gSystem->Setenv("QHAT", sqhat);
  gSystem->Setenv("DC_RUN",srun); // Not used in Config.C
  gSystem->Setenv("DC_EVENT",sevent); // Not used in Config.C
  
// Needed to produce simulated RAW data
  gSystem->Setenv("ALIMDC_RAWDB1","./mdc1");
  gSystem->Setenv("ALIMDC_RAWDB2","./mdc2");
  gSystem->Setenv("ALIMDC_TAGDB","./mdc1/tag");
  gSystem->Setenv("ALIMDC_RUNDB","./mdc1/meta");
  cout<< "SIMRUN:: Run " << gSystem->Getenv("DC_RUN") << " Event " << gSystem->Getenv("DC_EVENT")
	  << " Generator "    << gSystem->Getenv("CONFIG_RUN_TYPE")
	  << " Field " << gSystem->Getenv("CONFIG_FIELD")
	  << " Energy " << gSystem->Getenv("CONFIG_ENERGY")
          << " minpthard " << gSystem->Getenv("PTHARDMIN")
          << " maxpthard " << gSystem->Getenv("PTHARDMAX")
	  << endl;


  TStopwatch w;
  TStopwatch global;
  w.Start();
  global.Start();
  cout<<">>>>> SIMULATION <<<<<"<<endl;
  gSystem->Exec("echo ALICE_ROOT $ALICE_ROOT");
  gSystem->Exec("echo ROOTSYS $ROOTSYS");
  gSystem->Exec("echo LD_LIB $LD_LIBRARY_PATH");
  gSystem->Exec("echo PATH $PATH");

  if(strcmp(sprocess, "kPyJetJetParton")){
    gSystem->Exec(Form("aliroot -b -q sim.C\\(%d\\) > sim.log 2>&1",nevent));
  } else {
    gSystem->Exec("aliroot -b -q simParton.C > sim.log 2>&1");
  }
  gSystem->Exec("mv syswatch.log simwatch.log");
  w.Stop();
  w.Print();

  if(strcmp(sprocess, "kPyJetJetParton")){

    w.Start();
    cout<<">>>>> RECONSTRUCTION <<<<<"<<endl;
    gSystem->Exec("aliroot -b -q rec.C > rec.log 2>&1");
    gSystem->Exec("mv syswatch.log recwatch.log");
    w.Stop();
    w.Print();

    w.Start();
    cout<<">>>>> TAG <<<<<"<<endl;
    gSystem->Exec("aliroot -b -q tag.C > tag.log 2>&1");
    w.Stop();
    w.Print();

    w.Start();
    cout<<">>>>> CHECK ESD <<<<<"<<endl;
    gSystem->Exec("aliroot -b -q CheckESD.C > check.log 2>&1");
    w.Stop();
    w.Print();

    
    
    w.Start();
    cout<<">>>>> AOD <<<<<"<<endl;
    gSystem->Exec("aliroot -b -q AODtrainsim.C > aod.log 2>&1");
    gSystem->Exec("cat aod.log");
    w.Stop();
    w.Print();
    

    global.Stop();
    global.Print();

  }

}
