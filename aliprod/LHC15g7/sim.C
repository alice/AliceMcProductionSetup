void sim(Int_t nev=10) {

  AliSimulation simulator;

  simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD");
  simulator.SetMakeDigitsFromHits("ITS TPC");

//
// RAW OCDB
//
  simulator.SetDefaultStorage("alien://Folder=/alice/data/2011/OCDB");

//
// ITS  (1 Total)
//     Alignment from Ideal OCDB
  simulator.SetSpecificStorage("ITS/Align/Data",           "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");

//
// MUON  (1 Total)
//     MCH
  simulator.SetSpecificStorage("MUON/Align/Data",          "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");

//
// TPC (7 total)
//
  simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Align/Data",           "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/RecoParam",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");

//
// Vertex and Mag. field from OCDB
//
  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

//
// simulate HLT TPC clusters, trigger, and HLT ESD
//
  simulator.SetRunHLT("chains=TPC-compression,GLOBAL-Trigger,GLOBAL-esd-converter");

//
// PHOS simulation settings
//
//  AliPHOSSimParam *simParam = AliPHOSSimParam::GetInstance();
//  simParam->SetCellNonLineairyA(0.001);
//  simParam->SetCellNonLineairyB(0.2);
//  simParam->SetCellNonLineairyC(1.02);

//
// The rest
//
  simulator.SetRunQA(":");

  printf("Before simulator.Run(nev);\n");
  simulator.Run(nev);
  printf("After simulator.Run(nev);\n");
}
