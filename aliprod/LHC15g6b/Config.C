// One can use the configuration macro in compiled mode by
// root [0] gSystem->Load("libgeant321");
// root [0] gSystem->SetIncludePath("-I$ROOTSYS/include -I$ALICE_ROOT/include\
//                   -I$ALICE_ROOT -I$ALICE/geant3/TGeant3");
// root [0] .x grun.C(1,"Config.C++")

#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TRandom.h>
#include <TDatime.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include "STEER/AliRunLoader.h"
#include "STEER/AliRun.h"
#include "STEER/AliConfig.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "PYTHIA6/AliGenPythia.h"
#include "TDPMjet/AliGenDPMjet.h"
#include "STEER/AliMagFCheb.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "ITS/AliITSv11Hybrid.h"
#include "TPC/AliTPCv2.h"
#include "TOF/AliTOFv6T0.h"
#include "HMPID/AliHMPIDv3.h"
#include "ZDC/AliZDCv3.h"
#include "TRD/AliTRDv1.h"
#include "TRD/AliTRDgeometry.h"
#include "FMD/AliFMDv1.h"
#include "MUON/AliMUONv1.h"
#include "PHOS/AliPHOSv1.h"
#include "PHOS/AliPHOSSimParam.h"
#include "PMD/AliPMDv1.h"
#include "T0/AliT0v1.h"
#include "EMCAL/AliEMCALv2.h"
#include "ACORDE/AliACORDEv1.h"
#include "VZERO/AliVZEROv7.h"
#endif


enum PDC06Proc_t 
{
  kPyJetJet, kPyJetJetD6T, kPyJetJetPerugia0, kPyJetJetPerugia11, kPyJetJetParton ,kRunMax
};

const char * pprRunName[] = {
  "kPyJetJet", "kPyJetJetD6T", "kPyJetJetPerugia0", "kPyJetJetPerugia11", "kPyJetJetParton"
};

enum Mag_t
{
  kNoField, k5kG, kFieldMax
};

const char * pprField[] = {
  "kNoField", "k5kG"
};


enum PprTrigConf_t
{
    kDefaultPPTrig, kDefaultPbPbTrig
};

const char * pprTrigConfName[] = {
    "p-p","Pb-Pb"
};

//--- Functions ---
class AliGenPythia;

void ProcessEnvironmentVars();
void DetConfig();
void GenConfig();
void LoadSimLibraries();
void SetupGeant();
void SetupMC();
void SetEnergyFromGRP();

// Geterator, field, beam energy
static PDC06Proc_t   proc     = kPyJetJetPerugia11;
static Mag_t         mag      = k5kG;
static Float_t       energy   = -7000; // energy in CMS, if negative, extract from GRP
static Int_t         runNumber = 0;
static Float_t       ptHardMin = 10.;  // Will be overridden by values from env vars
static Float_t       ptHardMax = 20.;
static Int_t         iquenching = 0;
static Float_t       qhat = 0;
static PprTrigConf_t strig = kDefaultPPTrig; // default pp trigger configuration
//========================//
// Set Random Number seed //
//========================//
TDatime dt;
static UInt_t seed    = dt.Get();

// Comment line
static TString comment;


void Config()
{
  // Get settings from environment variables
  ProcessEnvironmentVars();
  LoadSimLibraries();

  gRandom->SetSeed(seed);
  cerr<<"Seed for random number generation= "<<seed<<endl; 

  SetupGeant();
  CreateGAlice();
  DetConfig();
  GenConfig();
  SetupMC();
  printf("\n \n Comment: %s \n \n", comment.Data());  
}

// 
void DetConfig()
{
  //======================//
  //   Define detectors   //
  //======================//

  Int_t iABSO  = 1;
  Int_t iACORDE= 0;
  Int_t iDIPO  = 1;
  Int_t iEMCAL = 1;
  Int_t iFMD   = 1;
  Int_t iFRAME = 1;
  Int_t iHALL  = 1;
  Int_t iITS   = 1;
  Int_t iMAG   = 1;
  Int_t iMUON  = 1;
  Int_t iPHOS  = 1;
  Int_t iPIPE  = 1;
  Int_t iPMD   = 1;
  Int_t iHMPID = 1;
  Int_t iSHIL  = 1;
  Int_t iT0    = 1;
  Int_t iTOF   = 1;
  Int_t iTPC   = 1;
  Int_t iTRD   = 1;
  Int_t iVZERO = 1;
  Int_t iZDC   = 1;
  
  
  //=================== Alice BODY parameters =============================
  AliBODY *BODY = new AliBODY("BODY", "Alice envelop");
  
  
  if (iMAG)
    {
      //=================== MAG parameters ============================
      // --- Start with Magnet since detector layouts may be depending ---
      // --- on the selected Magnet dimensions ---
      AliMAG *MAG = new AliMAG("MAG", "Magnet");
    }
  

  if (iABSO)
    {
      //=================== ABSO parameters ============================
      AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
    }

  if (iDIPO)
    {
      //=================== DIPO parameters ============================

      AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
    }

  if (iHALL)
    {
      //=================== HALL parameters ============================

      AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
    }


  if (iFRAME)
    {
      //=================== FRAME parameters ============================

      AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
      FRAME->SetHoles(1);
    }

  if (iSHIL)
    {
      //=================== SHIL parameters ============================

      AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
    }


  if (iPIPE)
    {
      //=================== PIPE parameters ============================

      AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
    }
 
  if (iITS)
    {
      //=================== ITS parameters ============================

      AliITS *ITS  = new AliITSv11("ITS","ITS v11");
    }

  if (iTPC)
    {
      //============================ TPC parameters =====================

      AliTPC *TPC = new AliTPCv2("TPC", "Default");
    }


  if (iTOF) {
    //=================== TOF parameters ============================

    AliTOF *TOF = new AliTOFv6T0("TOF", "normal TOF");
  }


  if (iHMPID)
    {
      //=================== HMPID parameters ===========================

      AliHMPID *HMPID = new AliHMPIDv3("HMPID", "normal HMPID");
    }


  if (iZDC)
    {
      //=================== ZDC parameters ============================
	
      AliZDC *ZDC = new AliZDCv3("ZDC", "normal ZDC");
      //Collimators aperture
      ZDC->SetVCollSideCAperture(0.85);
      ZDC->SetVCollSideCCentre(0.);
      ZDC->SetVCollSideAAperture(0.75);
      ZDC->SetVCollSideACentre(0.);
      //Detector position
      ZDC->SetYZNC(1.6);
      ZDC->SetYZNA(1.6);
      ZDC->SetYZPC(1.6);
      ZDC->SetYZPA(1.6);
    }

  if (iTRD)
    {
      //=================== TRD parameters ============================

      AliTRD *TRD = new AliTRDv1("TRD", "TRD slow simulator");
      AliTRDgeometry *geoTRD = TRD->GetGeometry();
      // Partial geometry: modules at 0,1,7,8,9,16,17
      // starting at 3h in positive direction
      geoTRD->SetSMstatus(2,0);
      geoTRD->SetSMstatus(3,0);
      geoTRD->SetSMstatus(4,0);
      geoTRD->SetSMstatus(5,0);
      geoTRD->SetSMstatus(6,0);
      geoTRD->SetSMstatus(11,0);
      geoTRD->SetSMstatus(12,0);
      geoTRD->SetSMstatus(13,0);
      geoTRD->SetSMstatus(14,0);
      geoTRD->SetSMstatus(15,0);
      geoTRD->SetSMstatus(16,0);
    }

  if (iFMD)
    {
      //=================== FMD parameters ============================

      AliFMD *FMD = new AliFMDv1("FMD", "normal FMD");
    }

  if (iMUON)
    {
      //=================== MUON parameters ===========================
      // New MUONv1 version (geometry defined via builders)
      AliMUON *MUON = new AliMUONv1("MUON", "default");
      // activate trigger efficiency by cells
      MUON->SetTriggerEffCells(1);
    }

  if (iPHOS)
    {
      //=================== PHOS parameters ===========================

      AliPHOS *PHOS = new AliPHOSv1("PHOS", "noCPV_Modules123");

    }


  if (iPMD)
    {
      //=================== PMD parameters ============================

      AliPMD *PMD = new AliPMDv1("PMD", "normal PMD");
    }

  if (iT0)
    {
      //=================== T0 parameters ============================
      AliT0 *T0 = new AliT0v1("T0", "T0 Detector");
    }

  if (iEMCAL)
    {
      //=================== EMCAL parameters ============================

      AliEMCAL *EMCAL = new AliEMCALv2("EMCAL", "EMCAL_FIRSTYEARV1");

    }

  if (iACORDE)
    {
      //=================== ACORDE parameters ============================

      AliACORDE *ACORDE = new AliACORDEv1("ACORDE", "normal ACORDE");
    }
     
  if (iVZERO)
    {
      //=================== ACORDE parameters ============================
      
      AliVZERO *VZERO = new AliVZEROv7("VZERO", "normal VZERO");
    }  
}

void CreateGAlice() 
{
  //=======================================================================
  //  Create the output file
   
  AliRunLoader* rl=0x0;

  cout<<"Config.C: Creating Run Loader ..."<<endl;
  rl = AliRunLoader::Open("galice.root",
			  AliConfig::GetDefaultEventFolderName(),
			  "recreate");
  if (!rl) {
    gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
    return;
  }
  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(1000);
  gAlice->SetRunLoader(rl);
  // gAlice->SetGeometryFromFile("geometry.root");
  // gAlice->SetGeometryFromCDB();
  rl->CdGAFile();
}


void SetupMC()
{
  //======================//
  //    Set MC options    //
  //======================//

  // Set the trigger configuration: proton-proton
  AliSimulation::Instance()->SetTriggerConfig(pprTrigConfName[strig]);
  cout <<"Trigger configuration is set to  "<<pprTrigConfName[strig]<<endl;
  //
  gMC->SetProcess("DCAY",1);
  gMC->SetProcess("PAIR",1);
  gMC->SetProcess("COMP",1);
  gMC->SetProcess("PHOT",1);
  gMC->SetProcess("PFIS",0);
  gMC->SetProcess("DRAY",0);
  gMC->SetProcess("ANNI",1);
  gMC->SetProcess("BREM",1);
  gMC->SetProcess("MUNU",1);
  gMC->SetProcess("CKOV",1);
  gMC->SetProcess("HADR",1);
  gMC->SetProcess("LOSS",2);
  gMC->SetProcess("MULS",1);
  gMC->SetProcess("RAYL",1);
  
  Float_t cut = 1.e-3;        // 1MeV cut by default
  Float_t tofmax = 1.e10;
  
  gMC->SetCut("CUTGAM", cut);
  gMC->SetCut("CUTELE", cut);
  gMC->SetCut("CUTNEU", cut);
  gMC->SetCut("CUTHAD", cut);
  gMC->SetCut("CUTMUO", cut);
  gMC->SetCut("BCUTE",  cut); 
  gMC->SetCut("BCUTM",  cut); 
  gMC->SetCut("DCUTE",  cut); 
  gMC->SetCut("DCUTM",  cut); 
  gMC->SetCut("PPCUTM", cut);
  gMC->SetCut("TOFMAX", tofmax); 
  //
  //======================//
  // Set External decayer //
  //======================//
  TVirtualMCDecayer* decayer = new AliDecayerPythia();
  decayer->SetForceDecay(kAll);
  decayer->Init();
  gMC->SetExternalDecayer(decayer);
  //
}

void VertexSigma(int run, double &sx, double &sy, double &sz)
{
  
  // defualts
  sx = 65E-4; 
  sy = 65E-4;
  sz = 6.0;  // usually set from the mean vertex CDB object
  //
  // tuned on LHC10 pass4 MeanVertex objects, with 10% downscaling
  if (run>=114786&&run<=115193) {sx =  66.E-4; sy =  85.E-4;} // b1005-1019, 9 runs
  else if (run>=115310&&run<=115345) {sx =  72.E-4; sy =  76.E-4;} // b1022-1022, 8 runs
  else if (run>=115393&&run<=115521) {sx =  73.E-4; sy =  72.E-4;} // b1023-1023, 5 runs
  else if (run>=115880&&run<=116081) {sx =  66.E-4; sy =  70.E-4;} // b1031-1031, 7 runs
  else if (run>=116102&&run<=116134) {sx =  80.E-4; sy =  64.E-4;} // b1034-1034, 6 runs
  else if (run>=116198&&run<=116288) {sx =  67.E-4; sy =  73.E-4;} // b1035-1035, 2 runs
  else if (run>=116401&&run<=116403) {sx =  65.E-4; sy =  61.E-4;} // b1044-1044, 3 runs
  else if (run>=116562&&run<=116684) {sx =  67.E-4; sy =  82.E-4;} // b1045-1045, 4 runs
  else if (run>=117034&&run<=118506) {sx =  53.E-4; sy =  56.E-4;} // b1058-1058, 20 runs
  else if (run>=117220&&run<=117222) {sx =  46.E-4; sy =  47.E-4;} // b1059-1059, 2 runs
  else if (run>=118503&&run<=118561) {sx = 213.E-4; sy = 240.E-4;} // b1068-1068, 6 runs
  else if (run>=119037&&run<=119086) {sx =  69.E-4; sy =  66.E-4;} // b1089-1089, 12 runs
  else if (run>=119156&&run<=120244) {sx =  56.E-4; sy =  54.E-4;} // b1090-1090, 4 runs
  else if (run>=120503&&run<=120505) {sx =  59.E-4; sy =  69.E-4;} // b1117-1117, 3 runs
  else if (run>=120611&&run<=120617) {sx =  55.E-4; sy =  61.E-4;} // b1118-1118, 5 runs
  else if (run>=120671&&run<=120822) {sx =  54.E-4; sy =  51.E-4;} // b1119-1119, 2 runs
  else if (run>=120823&&run<=120829) {sx =  52.E-4; sy =  80.E-4;} // b1122-1122, 8 runs
  else if (run>=121039&&run<=121040) {sx = 197.E-4; sy = 232.E-4;} // b1128-1128, 2 runs
  else if (run>=122372&&run<=122375) {sx =  54.E-4; sy =  49.E-4;} // b1134-1134, 3 runs
  else if (run>=124187&&run<=124191) {sx =  60.E-4; sy =  71.E-4;} // b1179-1179, 2 runs
  else if (run>=124355&&run<=124376) {sx =  64.E-4; sy =  55.E-4;} // b1182-1182, 15 runs
  else if (run>=124377&&run<=125296) {sx =  69.E-4; sy =  63.E-4;} // b1182-1182, 15 runs
  else if (run>=125628&&run<=125634) {sx =  74.E-4; sy =  58.E-4;} // b1207-1207, 5 runs
  else if (run>=125842&&run<=125855) {sx =  72.E-4; sy =  67.E-4;} // b1222-1222, 9 runs
  else if (run>=126004&&run<=126097) {sx =  67.E-4; sy =  65.E-4;} // b1224-1224, 3 runs
  else if (run>=126158&&run<=126177) {sx =  70.E-4; sy =  61.E-4;} // b1226-1226, 5 runs
  else if (run>=126158&&run<=126177) {sx =  64.E-4; sy =  62.E-4;} // b1229-1229, 3 runs
  else if (run>=126283&&run<=126285) {sx =  69.E-4; sy =  67.E-4;} // b1232-1232, 4 runs
  else if (run>=126350&&run<=126359) {sx =  63.E-4; sy =  59.E-4;} // b1233-1233, 12 runs
  else if (run>=126403&&run<=126437) {sx =  69.E-4; sy =  65.E-4;} // b1250-1250, 7 runs
  else if (run>=127712&&run<=128192) {sx =  67.E-4; sy =  61.E-4;} // b1251-1251, 6 runs
  else if (run>=127712&&run<=136136) {sx =  68.E-4; sy =  62.E-4;} // b1260-1260, 1 runs
  else {
    ::Info("VertexSigma","no matching run found, defaults will be used");
  }
  ::Info("VertexSigma",Form("Luminous Region set in Config: sX: %.4f sY: %.4f sZ: %.4f",sx,sy,sz));
}

void LoadSimLibraries()
{
  // Libraries
#if defined(__CINT__)
  gSystem->Load("liblhapdf");      // Parton density functions
  gSystem->Load("libEGPythia6");   // TGenerator interface
  gSystem->Load("libpythia6.4.25");        // Pythia 6.2
  gSystem->Load("libAliPythia6");  // ALICE specific implementations
  gSystem->Load("libgeant321");
#endif
  //
}

//
void GenConfig()
{
  //*******************************************************************//
  // Configuration file for hard QCD processes generation with PYTHIA  //
  //                                                                   //
  //*******************************************************************//
  AliGenPythia * gener = 0x0;

  switch(proc) {

  case kPyJetJet:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ6l);
    break;

  case kPyJetJetD6T:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ6l);
    //    Tune
    //    109     D6T : Rick Field's CDF Tune D6T (NB: needs CTEQ6L pdfs externally)
    gener->SetTune(109); // F I X 
    break;

  case kPyJetJetPerugia0:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    //gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetEtaRange(-1.1, 1.1);//  Final state kinematic cuts - OB: smaller range
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ6l);
    //    Tune
    //    320     Perugia 0
    gener->SetTune(320); 
    gener->UseNewMultipleInteractionsScenario();
    break;

  case kPyJetJetPerugia11:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    //gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetEtaRange(-1.1, 1.1);//  Final state kinematic cuts - OB: smaller range
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ5L);
    //    Tune
    //    350     Perugia 11
    gener->SetTune(350); 
    gener->UseNewMultipleInteractionsScenario();
    break;

  case kPyJetJetParton:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ6l);
    gener->SetMIS(0);

  }

  //
  // Set up jetfinder for triggering
  //
  gener->SetPycellParameters(2.5, 343, 432, 0., 0.1, 5., 0.6); // OB: lower seed threshold, smaller eta range
  gener->SetPtKick(5.); // set the intrinsic kt to 5 GeV/c
  gener->SetGluonRadiation(1,1);
 
  //
  // Size of the interaction diamond
  // Longitudinal
  Float_t sigmaz  = 5.4 / TMath::Sqrt(2.); // [cm]
  if (energy == 900)
    //sigmaz  = 10.5 / TMath::Sqrt(2.); // [cm]
    //sigmaz = 3.7;
  if (energy == 7000 || energy == 2760)
    sigmaz  = 6.3 / TMath::Sqrt(2.); // [cm]

  //
  // Transverse
  // beta*
  Float_t betast  = 2.;                 // beta* [m]
  printf("beta* for run# %8d is %13.3f", runNumber, betast);
  //
  Float_t eps     = 3.75e-6;            // emittance [m]
  Float_t gamma   = energy / 2.0 / 0.938272;  // relativistic gamma [1]
  Float_t sigmaxy = TMath::Sqrt(eps * betast / gamma) / TMath::Sqrt(2.) * 100.;  // [cm]
  printf("\n \n Diamond size x-y: %10.3e z: %10.3e\n \n", sigmaxy, sigmaz);
   
  gener->SetSigma(sigmaxy, sigmaxy, sigmaz);      // Sigma in (X,Y,Z) (cm) on IP position
  gener->SetVertexSmear(kPerEvent);
  gener->Init();

  //Quenching
  gener->SetQuench(iquenching);
  if(iquenching == 1){
     Float_t k = 6e5*(qhat/1.7);  //qhat=1.7, k=6e5, default value
     AliPythia::Instance()->InitQuenching(0.,0.1,k,0,0.95,6);
  }
  gener->Init();
  return gener;
}

void SetupGeant() 
{
  //=========================//
  // Geant instance, setup   //
  //=========================//
  
  new TGeant3TGeo("C++ Interface to Geant3");
}


void ProcessEnvironmentVars()
{

    // Run number
    if (gSystem->Getenv("DC_RUN")) {
      runNumber = atoi(gSystem->Getenv("DC_RUN"));
    }

    // Run type
    if (gSystem->Getenv("CONFIG_RUN_TYPE")) {
      for (Int_t iRun = 0; iRun < kRunMax; iRun++) {
	if (strcmp(gSystem->Getenv("CONFIG_RUN_TYPE"), pprRunName[iRun])==0) {
	  proc = (PDC06Proc_t)iRun;
	  cout<<"Run type set to "<<pprRunName[iRun]<<endl;
	}
      }
    }

    // Field
    if (gSystem->Getenv("CONFIG_FIELD")) {
      for (Int_t iField = 0; iField < kFieldMax; iField++) {
	if (strcmp(gSystem->Getenv("CONFIG_FIELD"), pprField[iField])==0) {
	  mag = (Mag_t)iField;
	  cout<<"Field set to "<<pprField[iField]<<endl;
	}
      }
    }

    // Energy
    if (gSystem->Getenv("CONFIG_ENERGY")) {
      energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
    }
    if (energy>0) cout<<"Energy set to "<<energy<<" GeV"<<endl;
    else SetEnergyFromGRP();

    // Random Number seed
    if (gSystem->Getenv("CONFIG_SEED")) {
      seed = atoi(gSystem->Getenv("CONFIG_SEED"));
    }
    // hard probes
    if (gSystem->Getenv("CONFIG_PTHARDMIN"))
      ptHardMin = atof(gSystem->Getenv("CONFIG_PTHARDMIN"));
    if (gSystem->Getenv("CONFIG_PTHARDMAX"))
      ptHardMax = atof(gSystem->Getenv("CONFIG_PTHARDMAX"));
    if (gSystem->Getenv("QUENCHING"))
      iquenching = atof(gSystem->Getenv("QUENCHING"));
    if (gSystem->Getenv("QHAT"))
      qhat = atof(gSystem->Getenv("QHAT"));

    cout<<">> Run type set to "<<pprRunName[proc]<<endl;
    cout<<">> Collision energy set to "<<energy <<endl;
    cout<<">> ptHard limits: "<<ptHardMin<<" to " <<ptHardMax<<" GeV"<<endl;
    cout<<">> quenching on? "<< iquenching<<" qhat "<<qhat<<endl;
    cout << endl;
    cout << "######################################" << endl;
    cout << "######################################" << endl;
}

void SetEnergyFromGRP()
{
  // obtain energy from GRP
  printf("Extracting beam energy from GRP for run %d\n",runNumber);
  if (runNumber<1) {
    printf("Cannot extract energy from GRP for unphysical run %d\n",runNumber);
    exit(1);
  }
  AliCDBEntry* entry = AliCDBManager::Instance()->Get("GRP/GRP/Data");
  AliGRPObject* grpData = dynamic_cast<AliGRPObject*>(entry->GetObject()); 
  energy = 2*grpData->GetBeamEnergy();
  printf("Energy set to %.1f GeV from GRP\n",energy);
}
