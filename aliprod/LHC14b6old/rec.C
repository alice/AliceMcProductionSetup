void rec() {
  AliReconstruction reco;

  reco.SetRunReconstruction("ITS TPC TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO HLT");

  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();

//
// RAW OCDB

  reco.SetDefaultStorage("alien://Folder=/alice/data/2010/OCDB");
//  reco.SetFromCDBSnapshot("OCDBrec.root");
  reco.SetCDBSnapshotMode("OCDBrec.root");
  
// Total number of specific storages - 28
//
// ITS (2 objects)

  reco.SetSpecificStorage("ITS/Align/Data",     "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");

//
// MUON (1 object)
//      MCH

  reco.SetSpecificStorage("MUON/Align/Data",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");

//
// TPC (7 objects)

  reco.SetSpecificStorage("TPC/Align/Data", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/ClusterParam", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/RecoParam", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/TimeGain", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/AltroConfig", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/TimeDrift", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/Correction", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");

  // ZDC

  reco.SetRecoParam("ZDC",AliZDCRecoParamPbPb::GetHighFluxParam(2760));

  // Introduce the TPC bug
  reco.SetOption("TPC","IntroduceBug"); 

  reco.SetRunQA(":"); 
  reco.Run();
}
