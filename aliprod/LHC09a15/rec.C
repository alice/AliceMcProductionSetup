void rec() {

  AliReconstruction reco;

  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();

  reco.SetDefaultStorage("alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("GRP/GRP/Data",
			  Form("local://%s",gSystem->pwd()));

  AliGRPRecoParam *grpRecoParam = AliGRPRecoParam::GetLowFluxParam();
  grpRecoParam->SetVertexerTracksConstraintITS(kFALSE);
  grpRecoParam->SetVertexerTracksConstraintTPC(kFALSE);
  reco.SetRecoParam("GRP",grpRecoParam);

  TStopwatch timer;
  timer.Start();
  reco.Run();
  timer.Stop();
  timer.Print();
}
