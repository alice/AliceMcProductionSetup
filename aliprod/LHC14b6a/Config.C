//
// Configuration for the first physics production 2008
//

// One can use the configuration macro in compiled mode by
// root [0] gSystem->Load("libgeant321");
// root [0] gSystem->SetIncludePath("-I$ROOTSYS/include -I$ALICE_ROOT/include\
//                   -I$ALICE_ROOT -I$ALICE/geant3/TGeant3");
// root [0] .x grun.C(1,"Config.C++")

#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TRandom.h>
#include <TDatime.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include "STEER/AliRunLoader.h"
#include "STEER/AliRun.h"
#include "STEER/AliConfig.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "PYTHIA6/AliGenPythia.h"
#include "TDPMjet/AliGenDPMjet.h"
#include "STEER/AliMagFCheb.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "ITS/AliITSv11Hybrid.h"
#include "TPC/AliTPCv2.h"
#include "TOF/AliTOFv6T0.h"
#include "HMPID/AliHMPIDv3.h"
#include "ZDC/AliZDCv3.h"
#include "TRD/AliTRDv1.h"
#include "TRD/AliTRDgeometry.h"
#include "FMD/AliFMDv1.h"
#include "MUON/AliMUONv1.h"
#include "PHOS/AliPHOSv1.h"
#include "PHOS/AliPHOSSimParam.h"
#include "PMD/AliPMDv1.h"
#include "T0/AliT0v1.h"
#include "EMCAL/AliEMCALv2.h"
#include "ACORDE/AliACORDEv1.h"
#include "VZERO/AliVZEROv7.h"
#endif


enum PDC06Proc_t  // renaming?
{
  kPyJetJet, kPyJetJetD6T, kPyJetJetPerugia0, kPyJetJetParton ,kRunMax
};

const char * pprRunName[] = {
  "kPyJetJet", "kPyJetJetD6T", "kPyJetJetPerugia0", "kPyJetJetParton"
};

enum Mag_t
{
  kNoField, k5kG, kFieldMax
};

const char * pprField[] = {
  "kNoField", "k5kG"
};

// Geterator, field, beam energy
static PDC06Proc_t   proc     = kPyJetJet;
static Mag_t         mag      = k5kG; // not used in this production, taken from CDB (see sim.C)
static Float_t       energy   = 7000; // energy in CMS
static Int_t         runNumber = 0;

static Float_t       ptHardMin = 10.;
static Float_t	     ptHardMax = 20.;
static Int_t         iquenching = 0;
static Float_t       qhat = 0;


//========================//
// Set Random Number seed //
//========================//
TDatime dt;
static UInt_t seed    = dt.Get();

// Comment line
static TString comment;

//--- Functions ---
class AliGenPythia;
/*
AliGenerator *MbPythia();
AliGenerator *MbPythiaTuneD6T();
AliGenerator *MbPhojet();
*/
void ProcessEnvironmentVars();

AliGenPythia *Blubb(Int_t proc){
  //*******************************************************************//
  // Configuration file for hard QCD processes generation with PYTHIA  //
  //                                                                   //
  //*******************************************************************//
  AliGenPythia * gener = 0x0;

  switch(proc) {

  case kPyJetJet:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ6l);
    break;

  case kPyJetJetD6T:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ6l);
    //    Tune
    //    109     D6T : Rick Field's CDF Tune D6T (NB: needs CTEQ6L pdfs externally)
    gener->SetTune(109); // F I X 
    break;

  case kPyJetJetPerugia0:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    //gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetEtaRange(-1.1, 1.1);//  Final state kinematic cuts - OB: smaller range
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ6l);
    //    Tune
    //    320     Perugia 0
    gener->SetTune(320); 
    gener->UseNewMultipleInteractionsScenario();
    break;

  case kPyJetJetParton:
    comment = comment.Append(Form("pp->jet + jet at %3.0f TeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    AliGenPythia * gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ6l);
    gener->SetMIS(0);

    }
    

    return gener;

}


void Config()
{
    

  // Get settings from environment variables
  ProcessEnvironmentVars();

  gRandom->SetSeed(seed);
  cerr<<"Seed for random number generation= "<<seed<<endl; 

  // Libraries required by geant321
#if defined(__CINT__)
  gSystem->Load("liblhapdf");      // Parton density functions
  gSystem->Load("libEGPythia6");   // TGenerator interface
  //if (proc == kPythia6 || proc == kPhojet) {
  //  gSystem->Load("libpythia6");        // Pythia 6.2
  //} else {
    gSystem->Load("libpythia6.4.21");   // Pythia 6.4
  //}
  gSystem->Load("libAliPythia6");  // ALICE specific implementations
  gSystem->Load("libgeant321");
#endif

  new TGeant3TGeo("C++ Interface to Geant3");

  //=======================================================================
  //  Create the output file

   
  AliRunLoader* rl=0x0;

  cout<<"Config.C: Creating Run Loader ..."<<endl;
  rl = AliRunLoader::Open("galice.root",
			  AliConfig::GetDefaultEventFolderName(),
			  "recreate");
  if (rl == 0x0)
    {
      gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
      return;
    }
  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(1000);
  gAlice->SetRunLoader(rl);
  // gAlice->SetGeometryFromFile("geometry.root");
  // gAlice->SetGeometryFromCDB();
  
  // Set the trigger configuration: proton-proton
  //gAlice->SetTriggerDescriptor("p-p");
  AliSimulation::Instance()->SetTriggerConfig("p-p");

  //
  //=======================================================================
  // ************* STEERING parameters FOR ALICE SIMULATION **************
  // --- Specify event type to be tracked through the ALICE setup
  // --- All positions are in cm, angles in degrees, and P and E in GeV


    gMC->SetProcess("DCAY",1);
    gMC->SetProcess("PAIR",1);
    gMC->SetProcess("COMP",1);
    gMC->SetProcess("PHOT",1);
    gMC->SetProcess("PFIS",0);
    gMC->SetProcess("DRAY",0);
    gMC->SetProcess("ANNI",1);
    gMC->SetProcess("BREM",1);
    gMC->SetProcess("MUNU",1);
    gMC->SetProcess("CKOV",1);
    gMC->SetProcess("HADR",1);
    gMC->SetProcess("LOSS",2);
    gMC->SetProcess("MULS",1);
    gMC->SetProcess("RAYL",1);

    Float_t cut = 1.e-3;        // 1MeV cut by default
    Float_t tofmax = 1.e10;

    gMC->SetCut("CUTGAM", cut);
    gMC->SetCut("CUTELE", cut);
    gMC->SetCut("CUTNEU", cut);
    gMC->SetCut("CUTHAD", cut);
    gMC->SetCut("CUTMUO", cut);
    gMC->SetCut("BCUTE",  cut); 
    gMC->SetCut("BCUTM",  cut); 
    gMC->SetCut("DCUTE",  cut); 
    gMC->SetCut("DCUTM",  cut); 
    gMC->SetCut("PPCUTM", cut);
    gMC->SetCut("TOFMAX", tofmax); 




  //======================//
  // Set External decayer //
  //======================//
  TVirtualMCDecayer* decayer = new AliDecayerPythia();
  decayer->SetForceDecay(kAll);
  decayer->Init();
  gMC->SetExternalDecayer(decayer);

  //=========================//
  // Generator Configuration //
  //=========================//
  AliGenPythia *gener = Blubb((Int_t) proc );

  //gener->SetPycellParameters(2., 274, 432, 0., 4., 5., 1.0);
  // expand the pycell grid to eta max 2.5 since jet eta max is 1.5
  //gener->SetPycellParameters(2.5, 343, 432, 0., 4., 5., 1.0);
  gener->SetPycellParameters(2.5, 343, 432, 0., 0.1, 5., 0.6); // OB: lower seed threshold, smaller eta range
  gener->SetPtKick(5.); // set the intrinsic kt to 5 GeV/c
  gener->SetGluonRadiation(1,1);
  
  //
  //
  // Size of the interaction diamond
  // Longitudinal
  Float_t sigmaz  = 5.4 / TMath::Sqrt(2.); // [cm]
  if (energy == 900)
    //sigmaz  = 10.5 / TMath::Sqrt(2.); // [cm]
    //sigmaz = 3.7;
  if (energy == 7000 || energy == 2760)
    sigmaz  = 6.3 / TMath::Sqrt(2.); // [cm]
  
  //
  // Transverse
  // beta*
  Float_t betast  = 2.;                 // beta* [m]
  printf("beta* for run# %8d is %13.3f", runNumber, betast);
  //
  Float_t eps     = 3.75e-6;            // emittance [m]
  Float_t gamma   = energy / 2.0 / 0.938272;  // relativistic gamma [1]
  Float_t sigmaxy = TMath::Sqrt(eps * betast / gamma) / TMath::Sqrt(2.) * 100.;  // [cm]
  printf("\n \n Diamond size x-y: %10.3e z: %10.3e\n \n", sigmaxy, sigmaz);
    
  gener->SetSigma(sigmaxy, sigmaxy, sigmaz);      // Sigma in (X,Y,Z) (cm) on IP position
  gener->SetVertexSmear(kPerEvent);
  gener->Init();

  //Quenching
  gener->SetQuench(iquenching);
  if(iquenching == 1){
     Float_t k = 6e5*(qhat/1.7);  //qhat=1.7, k=6e5, default value
     AliPythia::Instance()->InitQuenching(0.,0.1,k,0,0.95,6);
  }



  printf("\n \n Comment: %s \n \n", comment.Data());

  rl->CdGAFile();
  
  Int_t iABSO  = 1;
  Int_t iACORDE= 0;
  Int_t iDIPO  = 1;
  Int_t iEMCAL = 1;
  Int_t iFMD   = 1;
  Int_t iFRAME = 1;
  Int_t iHALL  = 1;
  Int_t iITS   = 1;
  Int_t iMAG   = 1;
  Int_t iMUON  = 1;
  Int_t iPHOS  = 1;
  Int_t iPIPE  = 1;
  Int_t iPMD   = 1;
  Int_t iHMPID = 1;
  Int_t iSHIL  = 1;
  Int_t iT0    = 1;
  Int_t iTOF   = 1;
  Int_t iTPC   = 1;
  Int_t iTRD   = 1;
  Int_t iVZERO = 1;
  Int_t iZDC   = 1;
  

    //=================== Alice BODY parameters =============================
    AliBODY *BODY = new AliBODY("BODY", "Alice envelop");


    if (iMAG)
    {
        //=================== MAG parameters ============================
        // --- Start with Magnet since detector layouts may be depending ---
        // --- on the selected Magnet dimensions ---
        AliMAG *MAG = new AliMAG("MAG", "Magnet");
    }


    if (iABSO)
    {
        //=================== ABSO parameters ============================
        AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
    }

    if (iDIPO)
    {
        //=================== DIPO parameters ============================

        AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
    }

    if (iHALL)
    {
        //=================== HALL parameters ============================

        AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
    }


    if (iFRAME)
    {
        //=================== FRAME parameters ============================

        AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
	FRAME->SetHoles(1);
    }

    if (iSHIL)
    {
        //=================== SHIL parameters ============================

        AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
    }


    if (iPIPE)
    {
        //=================== PIPE parameters ============================

        AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
    }
 
    if (iITS)
    {
        //=================== ITS parameters ============================

        AliITS *ITS  = new AliITSv11("ITS","ITS v11");
 	//AliITS *ITS  = new AliITSv11Hybrid("ITS","ITS v11Hybrid");
    }

    if (iTPC)
    {
      //============================ TPC parameters =====================

        AliTPC *TPC = new AliTPCv2("TPC", "Default");
    }


    if (iTOF) {
        //=================== TOF parameters ============================

	AliTOF *TOF = new AliTOFv6T0("TOF", "normal TOF");
    }


    if (iHMPID)
    {
        //=================== HMPID parameters ===========================

        AliHMPID *HMPID = new AliHMPIDv3("HMPID", "normal HMPID");

    }


    if (iZDC)
    {
        //=================== ZDC parameters ============================

        AliZDC *ZDC = new AliZDCv3("ZDC", "normal ZDC");
	  // ZDC->SetSpectatorsTrack(); 
	ZDC->SetLumiLength(0.);
    }

    if (iTRD)
    {
        //=================== TRD parameters ============================

        AliTRD *TRD = new AliTRDv1("TRD", "TRD slow simulator");
        AliTRDgeometry *geoTRD = TRD->GetGeometry();
	// Partial geometry: modules at 0,1,7,8,9,16,17
	// starting at 3h in positive direction
	geoTRD->SetSMstatus(2,0);
	geoTRD->SetSMstatus(3,0);
	geoTRD->SetSMstatus(4,0);
        geoTRD->SetSMstatus(5,0);
	geoTRD->SetSMstatus(6,0);
        geoTRD->SetSMstatus(11,0);
        geoTRD->SetSMstatus(12,0);
        geoTRD->SetSMstatus(13,0);
        geoTRD->SetSMstatus(14,0);
        geoTRD->SetSMstatus(15,0);
        geoTRD->SetSMstatus(16,0);
    }

    if (iFMD)
    {
        //=================== FMD parameters ============================

	AliFMD *FMD = new AliFMDv1("FMD", "normal FMD");
   }

    if (iMUON)
    {
        //=================== MUON parameters ===========================
        // New MUONv1 version (geometry defined via builders)

        AliMUON *MUON = new AliMUONv1("MUON", "default");
	MUON->SetTriggerEffCells(1); // not needed if raw masks
    }

    if (iPHOS)
    {
        //=================== PHOS parameters ===========================

     AliPHOS *PHOS = new AliPHOSv1("PHOS", "noCPV_Modules123");

    }


    if (iPMD)
    {
        //=================== PMD parameters ============================

        AliPMD *PMD = new AliPMDv1("PMD", "normal PMD");
    }

    if (iT0)
    {
        //=================== T0 parameters ============================
        AliT0 *T0 = new AliT0v1("T0", "T0 Detector");
    }

    if (iEMCAL)
    {
        //=================== EMCAL parameters ============================

        AliEMCAL *EMCAL = new AliEMCALv2("EMCAL", "EMCAL_FIRSTYEAR");
    }

     if (iACORDE)
    {
        //=================== ACORDE parameters ============================

        AliACORDE *ACORDE = new AliACORDEv1("ACORDE", "normal ACORDE");
    }

     if (iVZERO)
    {
        //=================== ACORDE parameters ============================

        AliVZERO *VZERO = new AliVZEROv7("VZERO", "normal VZERO");
    }
}


void ProcessEnvironmentVars()
{
    // Run type
    if (gSystem->Getenv("CONFIG_RUN_TYPE")) {
      for (Int_t iRun = 0; iRun < kRunMax; iRun++) {
	if (strcmp(gSystem->Getenv("CONFIG_RUN_TYPE"), pprRunName[iRun])==0) {
	  proc = (PDC06Proc_t)iRun;
	  cout<<"Run type set to "<<pprRunName[iRun]<<endl;
	}
      }
    }

    // Field
    if (gSystem->Getenv("CONFIG_FIELD")) {
      for (Int_t iField = 0; iField < kFieldMax; iField++) {
	if (strcmp(gSystem->Getenv("CONFIG_FIELD"), pprField[iField])==0) {
	  mag = (Mag_t)iField;
	  cout<<"Field set to "<<pprField[iField]<<endl;
	}
      }
    }

    // Energy
    if (gSystem->Getenv("CONFIG_ENERGY")) {
      energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
      cout<<"Energy set to "<<energy<<" GeV"<<endl;
    }

    // Random Number seed
    if (gSystem->Getenv("CONFIG_SEED")) {
      seed = atoi(gSystem->Getenv("CONFIG_SEED"));
    }

    // Run number
    if (gSystem->Getenv("DC_RUN")) {
      runNumber = atoi(gSystem->Getenv("DC_RUN"));
    }

    // hard probes
    if (gSystem->Getenv("CONFIG_PTHARDMIN"))
      ptHardMin = atof(gSystem->Getenv("CONFIG_PTHARDMIN"));
    if (gSystem->Getenv("CONFIG_PTHARDMAX"))
      ptHardMax = atof(gSystem->Getenv("CONFIG_PTHARDMAX"));
    if (gSystem->Getenv("QUENCHING"))
      iquenching = atof(gSystem->Getenv("QUENCHING"));
    if (gSystem->Getenv("QHAT"))
      qhat = atof(gSystem->Getenv("QHAT"));

    cout<<">> Run type set to "<<pprRunName[proc]<<endl;
    cout<<">> Collision energy set to "<<energy <<endl;
    cout<<">> ptHard limits: "<<ptHardMin<<" to " <<ptHardMax<<" GeV"<<endl;
    cout<<">> quenching on? "<< iquenching<<" qhat "<<qhat<<endl;

    cout << "######################################" << endl;
    cout << "######################################" << endl;

}
