#include "AliGenDPMjet.h"

// Phojet (now included in DPMJET III) p-p, Diffractive Tune

AliGenerator* GenDPMjet()
{
  AliGenDPMjet* gener = new AliGenDPMjet(-1);
  
  gener->SetMomentumRange(0, 999999.);
  gener->SetThetaRange(0., 180.);
  gener->SetYRange(-12.,12.);
  gener->SetTuneForDiff();
  gener->SetPtRange(0,1000.);
  gener->SetProcess(kDpmMb);
  gener->SetEnergyCMS(8000);
  gener->SetCrossingAngle(0,0.000280);
  
  return gener;
}
