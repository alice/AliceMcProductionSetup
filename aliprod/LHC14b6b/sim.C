void sim(Int_t nev=200) { //400

  AliSimulation simulator;

   simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON FMD PMD T0 VZERO"); //ZDC removed
  simulator.SetMakeDigitsFromHits("ITS TPC");

  // RAW OCDB
  simulator.SetDefaultStorage("alien://Folder=/alice/data/2010/OCDB"); 
//  simulator.SetCDBSnapshotMode("OCDB_MCsim.root");
  simulator.SetCDBSnapshotMode("OCDBsim.root");


  // simulate HLT TPC clusters, trigger, and HLT ESD
  //simulator.SetRunHLT("chains=TPC-compression,GLOBAL-Trigger,GLOBAL-esd-converter");
  simulator.SetRunHLT(""); // OB switch off
  
  // Specific storages = 3
  
//
// ITS  1 Total)
//     Alignment from Ideal OCDB
  
  simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

  //
  // MUON (1 Total)
  //      MCH
  
  simulator.SetSpecificStorage("MUON/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

  //
  // TPC (6 total)
  simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

  
  //
  // Vertex and Mag.field from OCDB
  
  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();
  
  //
  // Geometry from generated file
  //simulator.SetGeometryFile("geometry.root");  // OB switch off ?
   
  //
  // The rest
  
  //TStopwatch timerSim;
  //timerSim.Start();
  simulator.Run(nev);
  WriteXsection();  
  //timerSim.Stop();
  //timerSim.Print();
}


WriteXsection()
{
 TPythia6 *pythia = TPythia6::Instance();
 pythia->Pystat(1);
 Double_t xsection = pythia->GetPARI(1);
 UInt_t    ntrials  = pythia->GetMSTI(5);

 TFile *file = new TFile("pyxsec.root","recreate");
 TTree   *tree   = new TTree("Xsection","Pythia cross section");
 TBranch *branch = tree->Branch("xsection", &xsection, "X/D");
 TBranch *branch = tree->Branch("ntrials" , &ntrials , "X/i");
 tree->Fill();



 tree->Write();
 file->Close();

 cout << "Pythia cross section: " << xsection
      << ", number of trials: " << ntrials << endl;
}

