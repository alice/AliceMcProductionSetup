void sim(Int_t nev=3) {
  AliSimulation simulator;


  //simulator.SetWriteRawData("ALL","raw.root",kFALSE);
  simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD");
  simulator.SetMakeDigitsFromHits("ITS TPC");
//
//
// RAW OCDB
  simulator.SetDefaultStorage("alien://Folder=/alice/data/2010/OCDB");

// Specific storages = 23 
//
// ITS  (1 Total)
//     Alignment from Ideal OCDB 

  simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

//
// MUON (1 object)

  simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal"); 

//
// TPC (7 total) 

  simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/RecoParam",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");

// ZDC for 2010 the following is needed (https://savannah.cern.ch/task/?func=detailitem&item_id=33180#comment46)
  simulator.SetSpecificStorage("ZDC/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/"); 

//
// Vertex and magfield
  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

//
// The rest
//
  simulator.SetRunQA(":");

  printf("Before simulator.Run(nev);\n");
  simulator.Run(nev);
  printf("After simulator.Run(nev);\n");
}
