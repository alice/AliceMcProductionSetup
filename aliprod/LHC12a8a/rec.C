void rec() {
  
  AliReconstruction reco;
  reco.SetRunReconstruction("MUON");
//  reco.SetRunReconstruction("MUON ITS");
  reco.SetRunQA("MUON:ALL");
  reco.SetQAWriteExpert(AliQAv1::kMUON);
  
// switch off cleanESD
  reco.SetCleanESD(kFALSE);
    
  // Raw OCDB
  reco.SetDefaultStorage("alien://folder=/alice/data/2011/OCDB");
  
  // GRP from local OCDB
  reco.SetSpecificStorage("GRP/GRP/Data",Form("local://%s",gSystem->pwd()));
  
  // MUON Tracker
  reco.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
  
  // RecoParam for vertexerMC
  //reco.SetSpecificStorage("ITS/Calib/RecoParam","alien://folder=/alice/cern.ch/user/p/ppillot/OCDB_PbPbSim");
  
  TStopwatch timer;
  timer.Start();
  reco.Run();
  timer.Stop();
  timer.Print();
}
