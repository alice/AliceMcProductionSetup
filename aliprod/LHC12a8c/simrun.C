// void simrun(int run = 170040, int seed = 1){
{
  int nrun = 0;
  int nevent = 0;
  int seed = 0;

  char sseed[1024];
  char srun[1024];
  char sevent[1024];
  char sprocess[1024];
  char sfield[1024];
  char senergy[1024];
 
  sprintf(srun,"");
  sprintf(sevent,"");
  sprintf(sprocess,"");
  sprintf(sfield,"");
  sprintf(senergy,"");

  for (int i=0; i< gApplication->Argc();i++){
    if (!(strcmp(gApplication->Argv(i),"--run"))) nrun = atoi(gApplication->Argv(i+1));
      sprintf(srun,"%d",nrun);

    if (!(strcmp(gApplication->Argv(i),"--event"))) nevent = atoi(gApplication->Argv(i+1));
      sprintf(sevent,"%d",nevent);

    if (!(strcmp(gApplication->Argv(i),"--process")))
      sprintf(sprocess, gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--field")))
      sprintf(sfield,gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--energy")))
      sprintf(senergy,gApplication->Argv(i+1));
  }
  seed = nrun * 100 + nevent;
  sprintf(sseed,"%d",seed);

  //  gSystem->Setenv("CONFIG_ENERGY",Form("%d",2760));
  //  gSystem->Setenv("CONFIG_SEED",Form("%d",seed));
  //  gSystem->Setenv("DC_RUN",Form("%d",run));

  gSystem->Setenv("CONFIG_SEED",sseed);
  gSystem->Setenv("CONFIG_RUN_TYPE",sprocess); // kPythia6 or kPhojet
  gSystem->Setenv("CONFIG_FIELD",sfield);      // kNoField or k5kG
  gSystem->Setenv("CONFIG_ENERGY",senergy);    // 900 or 10000 (GeV)
  gSystem->Setenv("DC_RUN",srun); // Not used in Config.C
  gSystem->Setenv("DC_EVENT",sevent); // Not used in Config.C

  cout<< "SIMRUN:: Run " << gSystem->Getenv("DC_RUN") << " Event " << gSystem->Getenv("DC_EVENT")
	  << " Generator "    << gSystem->Getenv("CONFIG_RUN_TYPE")
	  << " Energy " << gSystem->Getenv("CONFIG_ENERGY")
          << " Seed " << gSystem->Getenv("CONFIG_SEED")
	  << endl;

  gSystem->Exec("mkdir input");
  gSystem->Exec("mv galice.root ./input/galice.root");
  gSystem->Exec("mv Kinematics.root ./input/Kinematics.root");
  gSystem->Exec("ls input");

  cout<<">>>>> SIMULATION <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q sim.C > sim.log 2>&1");
  gSystem->Exec("mv syswatch.log simwatch.log");
  cout<<">>>>> RECONSTRUCTION <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q rec.C > rec.log 2>&1");
  gSystem->Exec("mv syswatch.log recwatch.log");
  cout<<">>>>> TAG <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q tag.C > tag.log 2>&1");
  cout<<">>>>> CHECK ESD <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q CheckESD.C > check.log 2>&1");
  cout<<">>>>> AOD <<<<<"<<endl;
  //  gSystem->Exec("aliroot -b -q sim.C > sim.log 2>&1");
  //  gSystem->Exec("aliroot -b -q rec.C > rec.log 2>&1");
}
