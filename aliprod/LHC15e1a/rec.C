void rec() 
{  
	AliReconstruction reco;

	//
	// switch off cleanESD, write ESDfriends and Alignment data
  
	reco.SetCleanESD(kFALSE);
	reco.SetWriteESDfriend();
	reco.SetWriteAlignmentData();
	reco.SetFractionFriends(.1);
	
	reco.SetRunPlaneEff(kTRUE);
	reco.SetUseTrackingErrorsForAlignment("ITS");

	// RAW OCDB

	reco.SetDefaultStorage("alien://Folder=/alice/data/2015/OCDB");

	// ITS (2 objects)

	reco.SetSpecificStorage("ITS/Align/Data",     "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
	reco.SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");  

	// MUON objects (1 object)

	reco.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Residual"); 

	// TPC (7 objects)

	//
	// TPC (3 new objects for Run2 MC) 

	reco.SetSpecificStorage("TPC/Calib/Parameters",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");
	reco.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");
	reco.SetSpecificStorage("TPC/Calib/RecoParam",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");

	// TPC (5 old objects) 
	reco.SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
	reco.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
	reco.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
	reco.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
	reco.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/"); 
	
        // ZDC
        reco.SetSpecificStorage("ZDC/Align/Data","alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

	reco.SetRunQA(":");
	reco.Run();
}
