#if !defined(__CINT__) || defined(__MAKECINT__)
#include "Riostream.h"
#include "TRandom.h"
#include "TDatime.h"
#include "TSystem.h"
#include "TVirtualMC.h"
#include "TGeant3TGeo.h"
#include "STEER/AliRunLoader.h"
#include "STEER/AliRun.h"
#include "STEER/AliConfig.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "PYTHIA6/AliGenPythia.h"
#include "TDPMjet/AliGenDPMjet.h"
#include "STEER/AliMagFCheb.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "ITS/AliITSv11Hybrid.h"
#include "TPC/AliTPCv2.h"
#include "TOF/AliTOFv6T0.h"
#include "HMPID/AliHMPIDv3.h"
#include "ZDC/AliZDCv3.h"
#include "TRD/AliTRDv1.h"
#include "TRD/AliTRDgeometry.h"
#include "FMD/AliFMDv1.h"
#include "MUON/AliMUONv1.h"
#include "PHOS/AliPHOSv1.h"
#include "PHOS/AliPHOSSimParam.h"
#include "PMD/AliPMDv1.h"
#include "T0/AliT0v1.h"
#include "EMCAL/AliEMCALv2.h"
#include "ACORDE/AliACORDEv1.h"
#include "VZERO/AliVZEROv7.h"
#endif

void Config(){
  // Libraries required by geant321
#if defined(__CINT__)
  gSystem->Load("liblhapdf");      // Parton density functions
  gSystem->Load("libEGPythia6");   // TGenerator interface
  gSystem->Load("libpythia6");     // Pythia 6.2
  gSystem->Load("libAliPythia6");  // ALICE specific implementations
  gSystem->Load("libdpmjet");
  gSystem->Load("libTDPMjet");
  gSystem->Load("libampt");
  gSystem->Load("libTAmpt");
  gSystem->Load("libgeant321");
#endif

  new TGeant3TGeo("C++ Interface to Geant3");

  AliRunLoader* rl=0x0;
  cout<<"Config.C: Creating Run Loader ..."<<endl;
  rl = AliRunLoader::Open("galice.root",AliConfig::GetDefaultEventFolderName(),"recreate");
  if (rl == 0x0) {
    gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
    return;
  }
  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(20000);
  gAlice->SetRunLoader(rl);
  
  AliSimulation::Instance()->SetTriggerConfig("p-p");
  
  gMC->SetProcess("DCAY",1);
  gMC->SetProcess("PAIR",1);
  gMC->SetProcess("COMP",1);
  gMC->SetProcess("PHOT",1);
  gMC->SetProcess("PFIS",0);
  gMC->SetProcess("DRAY",0);
  gMC->SetProcess("ANNI",1);
  gMC->SetProcess("BREM",1);
  gMC->SetProcess("MUNU",1);
  gMC->SetProcess("CKOV",1);
  gMC->SetProcess("HADR",1);
  gMC->SetProcess("LOSS",2);
  gMC->SetProcess("MULS",1);
  gMC->SetProcess("RAYL",1);

  Float_t cut = 1.e-3;        // 1MeV cut by default
  Float_t tofmax = 1.e10;

  gMC->SetCut("CUTGAM", cut);
  gMC->SetCut("CUTELE", cut);
  gMC->SetCut("CUTNEU", cut);
  gMC->SetCut("CUTHAD", cut);
  gMC->SetCut("CUTMUO", cut);
  gMC->SetCut("BCUTE",  cut); 
  gMC->SetCut("BCUTM",  cut); 
  gMC->SetCut("DCUTE",  cut); 
  gMC->SetCut("DCUTM",  cut); 
  gMC->SetCut("PPCUTM", cut);
  gMC->SetCut("TOFMAX", tofmax); 

  // Set External decayer
  TVirtualMCDecayer* decayer = new AliDecayerPythia;
  decayer->SetForceDecay(kAll);
  decayer->Init();
  gMC->SetExternalDecayer(decayer);
  
  AliGenerator* gener = Ampt();

  Float_t sigmax = 0.0025;
  Float_t sigmay = 0.0029;
  Float_t sigmaz = 6.2450;
  gener->SetOrigin(0., 0., 0.);
  gener->SetSigma(sigmax, sigmay, sigmaz);
  gener->SetVertexSmear(kPerEvent);

  gener->Print();
  gener->Init();
  
  TGeoGlobalMagField::Instance()->SetField(new AliMagF("Maps","Maps", -1., -1., AliMagF::k5kG,AliMagF::kBeamTypepA,82.*4000/208.));
  
  rl->CdGAFile();
  
  AliBODY *BODY     = new AliBODY("BODY", "Alice envelop");
  AliMAG  *MAG      = new AliMAG("MAG", "Magnet");
  AliABSO *ABSO     = new AliABSOv3("ABSO", "Muon Absorber");
  AliDIPO *DIPO     = new AliDIPOv3("DIPO", "Dipole version 3");
  AliHALL *HALL     = new AliHALLv3("HALL", "Alice Hall");
  AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
  FRAME->SetHoles(1);
  AliSHIL *SHIL     = new AliSHILv3("SHIL", "Shielding Version 3");
  AliPIPE *PIPE     = new AliPIPEv3("PIPE", "Beam Pipe");
  AliITS *ITS       = new AliITSv11("ITS","ITS v11");
  AliFMD *FMD       = new AliFMDv1("FMD", "normal FMD");
  AliMUON *MUON     = new AliMUONv1("MUON","default");
  MUON->SetTriggerEffCells(1);
  MUON->SetTriggerResponseV1(2);
  AliT0 *T0         = new AliT0v1("T0", "T0 Detector");
  AliVZERO *VZERO   = new AliVZEROv7("VZERO", "normal VZERO");
}

AliGenerator* Ampt(){
  AliGenAmpt *genHi = new AliGenAmpt(-1);
  genHi->SetEnergyCMS(5023);
  genHi->SetReferenceFrame("CMS");
  genHi->SetTarget     ("A", 208, 82);
  genHi->SetProjectile ("P", 1, 1);
  genHi->SetIsoft(4);            // 1: default - 4: string melting
  genHi->SetStringFrag(0.5,0.9); // Lund string frgmentation parameters
  genHi->SetNtMax(150);          // NTMAX: number of timesteps (D=150)
  genHi->SetXmu(2.2814);         // parton screening mass in fm^(-1) (D=3.2264d0)
  genHi->SetJetQuenching(0);     // enable jet quenching
  genHi->SetShadowing(1);        // enable shadowing
  genHi->SetDecaysOff(1);        // neutral pion and heavy particle decays switched off
  genHi->SetSpectators(0);       // track spectators
  genHi->SetPtHardMin(2);
  genHi->SetImpactParameterRange(0,10.);
  genHi->SetRandomReactionPlane(kTRUE);
  genHi->SetBoostLHC(1);
  return genHi;
}
