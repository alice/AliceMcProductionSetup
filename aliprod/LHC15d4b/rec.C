void rec(){
  AliReconstruction reco;

  reco.SetRunReconstruction("MUON ITS VZERO");
  reco.SetRunGlobalQA(kFALSE);
  reco.SetFillTriggerESD();
  reco.SetCleanESD(kFALSE);

  //raw OCDB
  reco.SetDefaultStorage("alien://folder=/alice/data/2013/OCDB");
  // GRP from local OCDB
  reco.SetSpecificStorage("GRP/GRP/Data",Form("local://%s",gSystem->pwd()));
  // MUON tracker
  reco.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
  //RS: SPD MC reco needs special alignment
  reco.SetSpecificStorage("ITS/Align/Data",     "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");

  TStopwatch timer;
  timer.Start();
  reco.Run();
  timer.Stop();
  timer.Print();
}
