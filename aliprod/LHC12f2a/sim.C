void sim(Int_t nev=400) {
  
  AliSimulation simulator;

  simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
  simulator.SetMakeDigits ("TRD TOF PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
  simulator.SetMakeDigitsFromHits("ITS TPC");

  //simulator.SetRunQA(":");

  // RAW OCDB
  simulator.SetDefaultStorage("alien://Folder=/alice/data/2011/OCDB");
  

  // Specific storages = 31

  //
  // ITS  (1 Total)
  //     Alignment from Ideal OCDB 

  simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

  //
  // MUON  (6 Total)
  //      MCH

  simulator.SetSpecificStorage("MUON/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("MUON/Calib/Gains", "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

  //      MTR

  simulator.SetSpecificStorage("MUON/Calib/GlobalTriggerCrateConfig", "alien://Folder=/alice/simulation/2008/v4-15-Release/Full");
  simulator.SetSpecificStorage("MUON/Calib/LocalTriggerBoardMasks",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Full");
  simulator.SetSpecificStorage("MUON/Calib/RegionalTriggerConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Full");
  simulator.SetSpecificStorage("MUON/Calib/TriggerEfficiency",        "alien://Folder=/alice/simulation/2008/v4-15-Release/Full");
  //simulator.SetSpecificStorage("MUON/Calib/MappingData",        "alien://folder=/alice/simulation/2008/v4-15-Release/Full");
  
  
  //
  // TPC (21 total) 

  simulator.SetSpecificStorage("TPC/Calib/PadGainFactor",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  // simulator.SetSpecificStorage("TPC/Calib/GainFactorDedx", "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/PadTime0",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Distortion",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/PadNoise",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Pedestals",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Temperature",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Parameters",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Pulser",         "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/CE",             "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Mapping",        "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Goofie",         "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Raw",            "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/QA",             "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  //simulator.SetSpecificStorage("TPC/Calib/HighVoltage",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");


// Vertex and Mag.field from OCDB

  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

  TStopwatch timerSim;
  timerSim.Start();
  simulator.Run(nev);
  WriteXsection(); 
  timerSim.Stop();
  timerSim.Print();

}


WriteXsection()
{
 TPythia6 *pythia = TPythia6::Instance();
 pythia->Pystat(1);
 Double_t xsection = pythia->GetPARI(1);
 UInt_t    ntrials  = pythia->GetMSTI(5);

 TFile *file = new TFile("pyxsec.root","recreate");
 TTree   *tree   = new TTree("Xsection","Pythia cross section");
 TBranch *branch = tree->Branch("xsection", &xsection, "X/D");
 TBranch *branch = tree->Branch("ntrials" , &ntrials , "X/i");
 tree->Fill();



 tree->Write();
 file->Close();

 cout << "Pythia cross section: " << xsection
      << ", number of trials: " << ntrials << endl;
}

