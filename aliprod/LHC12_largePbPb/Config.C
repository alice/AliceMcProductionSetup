//********************************************************************************************************************************
//******************************** Configuration for PbPb ************************************************************************
// - 1 HIJING event 2.76 TeV with b<30fm and central dNch/deta=2000 
// - 1 AAMPT event 2.76 TeV with b<30fm 
// - GA signals 
//    * 1 pi0 & eta in PHOS acceptance
//    * 1 pi0 & eta in EMCAL acceptance
//    * 10 pi0 & eta in central barrel
//********************************************************************************************************************************


#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TH1.h>
#include <TRandom.h>
#include <TDatime.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include <TGeoGlobalMagField.h>
#include "STEER/STEER/AliRunLoader.h"
#include "STEER/STEER/AliRun.h"
#include "STEER/STEER/AliConfig.h"
#include "STEER/STEER/AliSimulation.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "PYTHIA6/AliGenPythia.h"
#include "TDPMjet/AliGenDPMjet.h"
#include "EVGEN/AliGenParam.h"
#include "EVGEN/AliGenMUONlib.h"
#include "EVGEN/AliGenPHOSlib.h"
#include "EVGEN/AliGenSTRANGElib.h"
#include "EVGEN/AliGenCocktail.h"
#include "EVGEN/AliGenBox.h"
#include "THijing/AliGenHijing.h"
#include "TAmpt/AliGenAmpt.h"
#include "TUHKMgen/AliGenUHKM.h"
#include "EVGEN/AliGenSlowNucleons.h"
#include "EVGEN/AliSlowNucleonModel.h"
#include "EVGEN/AliSlowNucleonModelExp.h"
#include "STEER/STEERBase/AliMagF.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "ITS/AliITSv11.h"
#include "TPC/AliTPCv2.h"
#include "TOF/AliTOFv6T0.h"
#include "HMPID/AliHMPIDv3.h"
#include "ZDC/AliZDCv3.h"
#include "TRD/AliTRDv1.h"
#include "TRD/AliTRDgeometry.h"
#include "FMD/AliFMDv1.h"
#include "MUON/AliMUONv1.h"
#include "PHOS/AliPHOSv1.h"
#include "PHOS/AliPHOSSimParam.h"
#include "PMD/AliPMDv1.h"
#include "T0/AliT0v1.h"
#include "EMCAL/AliEMCALv2.h"
#include "ACORDE/AliACORDEv1.h"
#include "VZERO/AliVZEROv7.h"
#endif


enum PprRun_t 
{
   kPythia6, kPythia6D6T, kPythia6ATLAS, kPythiaPerugia0, kPhojet, kHijing, kHijing2000, kHijing2000GA, kHydjet, kDpmjet, kAmptGA, kAmpt, kRunMax
};

const char * pprRunName[] = {
   "kPythia6", "kPythia6D6T", "kPythia6ATLAS", "kPythiaPerugia0", "kPhojet", "kHijing", "kHijing2000", "kHijing2000GA", "kHydjet", "kDpmjet", "kAmptGA", "kAmpt"
};

enum Mag_t
{
   kNoField, k5kG, kFieldMax
};

const char * pprField[] = {
   "kNoField", "k5kG"
};

enum PprTrigConf_t
{
   kDefaultPPTrig, kDefaultPbPbTrig
};

const char * pprTrigConfName[] = {
   "p-p","Pb-Pb"
};

//--- Functions ---
class AliGenPythia;
AliGenerator *MbPythia();
AliGenerator *MbPythiaTuneD6T();
AliGenerator *MbPythiaTuneATLAS();
AliGenerator *MbPythiaTunePerugia0();
AliGenerator *MbPhojet();
AliGenerator *Hijing();
AliGenerator *Hijing2000();
AliGenerator *Hijing2000GA();
AliGenerator *Hydjet();
AliGenerator *Dpmjet();
AliGenerator *Ampt();
Float_t EtaToTheta(Float_t arg);

void ProcessEnvironmentVars();

// Geterator, field, beam energy
static PprRun_t   proc     = kHijing2000;
static AliMagF::BMap_t smag = AliMagF::k5kG;
static Float_t       pBeamEnergy = 4000.; // energy p-Beam
static Float_t       energy   = 2760.;
static Float_t       bMin     = 0.;
static Float_t       bMax     = 100.;
static PprTrigConf_t strig = kDefaultPbPbTrig; // default pp trigger configuration
//========================//
// Set Random Number seed //
//========================//
TDatime dt;
static UInt_t seed    = dt.Get();

// Comment line
static TString comment;



void Config() {
   // Get settings from environment variables
   ProcessEnvironmentVars();

   gRandom->SetSeed(seed);
   cerr<<"Seed for random number generation= "<<seed<<endl; 

   // Libraries required by geant321
   #if defined(__CINT__)
   gSystem->Load("liblhapdf");      // Parton density functions
   gSystem->Load("libEGPythia6");   // TGenerator interface
   if (proc == kPythia6 || proc == kPhojet || proc == kDpmjet) {
      gSystem->Load("libpythia6");        // Pythia 6.2
      gSystem->Load("libAliPythia6");     // ALICE specific implementations
   } else if (proc != kHydjet) {
      gSystem->Load("libpythia6.4.21");   // Pythia 6.4
      gSystem->Load("libAliPythia6");     // ALICE specific implementations	
   }

   if (proc == kHijing || proc == kHijing2000 || proc == kHijing2000GA ) {
      gSystem->Load("libhijing");	
      gSystem->Load("libTHijing");
      AliPDG::AddParticlesToPdgDataBase();
   } else if (proc == kHydjet)  {
      gSystem->Load("libTUHKMgen");
   } else if (proc == kDpmjet) {
      gSystem->Load("libdpmjet");
            gSystem->Load("libTDPMjet");
   } else if (proc == kAmptGA || proc == kAmpt) {
      gSystem->Load("libampt");
            gSystem->Load("libTAmpt");
   } 

   gSystem->Load("libgeant321");

   #endif

   new TGeant3TGeo("C++ Interface to Geant3");

   //=======================================================================
   //  Create the output file

      
   AliRunLoader* rl=0x0;

   cout<<"Config.C: Creating Run Loader ..."<<endl;
   rl = AliRunLoader::Open("galice.root",
            AliConfig::GetDefaultEventFolderName(),
            "recreate");
   if (rl == 0x0){
      gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
      return;
   }
   rl->SetCompressionLevel(2);
   rl->SetNumberOfEventsPerFile(1000);
   gAlice->SetRunLoader(rl);
   // gAlice->SetGeometryFromFile("geometry.root");
   // gAlice->SetGeometryFromCDB();
   
   // Set the trigger configuration
   AliSimulation::Instance()->SetTriggerConfig(pprTrigConfName[strig]);
   cout<<"Trigger configuration is set to  "<<pprTrigConfName[strig]<<endl;

   //
   //=======================================================================
   // ************* STEERING parameters FOR ALICE SIMULATION **************
   // --- Specify event type to be tracked through the ALICE setup
   // --- All positions are in cm, angles in degrees, and P and E in GeV


   gMC->SetProcess("DCAY",1);
   gMC->SetProcess("PAIR",1);
   gMC->SetProcess("COMP",1);
   gMC->SetProcess("PHOT",1);
   gMC->SetProcess("PFIS",0);
   gMC->SetProcess("DRAY",0);
   gMC->SetProcess("ANNI",1);
   gMC->SetProcess("BREM",1);
   gMC->SetProcess("MUNU",1);
   gMC->SetProcess("CKOV",1);
   gMC->SetProcess("HADR",1);
   gMC->SetProcess("LOSS",2);
   gMC->SetProcess("MULS",1);
   gMC->SetProcess("RAYL",1);

   Float_t cut = 1.e-3;        // 1MeV cut by default
   Float_t tofmax = 1.e10;

   gMC->SetCut("CUTGAM", cut);
   gMC->SetCut("CUTELE", cut);
   gMC->SetCut("CUTNEU", cut);
   gMC->SetCut("CUTHAD", cut);
   gMC->SetCut("CUTMUO", cut);
   gMC->SetCut("BCUTE",  cut); 
   gMC->SetCut("BCUTM",  cut); 
   gMC->SetCut("DCUTE",  cut); 
   gMC->SetCut("DCUTM",  cut); 
   gMC->SetCut("PPCUTM", cut);
   gMC->SetCut("TOFMAX", tofmax); 

   //======================//
   // Set External decayer //
   //======================//
   if (proc != kHydjet) {
      TVirtualMCDecayer* decayer = new AliDecayerPythia();
      decayer->SetForceDecay(kAll);
      decayer->Init();
      gMC->SetExternalDecayer(decayer);  
   }

   //=========================//
   // Generator Configuration //
   //=========================//
   AliGenerator* gener = 0x0;
   
   if (proc == kPythia6) {
      gener = MbPythia();
   } else if (proc == kPythia6D6T) {
      gener = MbPythiaTuneD6T();
   } else if (proc == kPythia6ATLAS) {
      gener = MbPythiaTuneATLAS();
   } else if (proc == kPythiaPerugia0) {
      gener = MbPythiaTunePerugia0();
   } else if (proc == kPhojet) {
      gener = MbPhojet();
   } else if (proc == kHijing) {
      gener = Hijing();	
   } else if (proc == kHijing2000) {
      gener = Hijing2000();	
   } else if (proc == kHijing2000GA || proc == kAmptGA) {
      gener = Hijing2000GA();	
   } else if (proc == kHydjet) {
      gener = Hydjet();	
   } else if (proc == kDpmjet) {
      gener = Dpmjet();	
   } else if (proc == kAmpt) {
      gener = Ampt();	 
   }


      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
   gener->SetOrigin(0., 0., 0.); // Taken from OCDB
   gener->SetSigma(0, 0, 5.3);   // Sigma in (X,Y,Z) (cm) on IP position
   gener->SetVertexSmear(kPerEvent);
   gener->Init();

   //  
   // FIELD
   //

   if (smag == AliMagF::k2kG) {
      comment = comment.Append(" | L3 field 0.2 T");
   } else if (smag == AliMagF::k5kG) {
      comment = comment.Append(" | L3 field 0.5 T");
   }

   TGeoGlobalMagField::Instance()->SetField(new AliMagF("Maps","Maps", -1., -1., AliMagF::k5kG, AliMagF::kBeamTypeAA, 1380.)); 


   printf("\n \n Comment: %s \n \n", comment.Data());

   rl->CdGAFile();
   
   Int_t iABSO  = 1;
   Int_t iACORDE= 0;
   Int_t iDIPO  = 1;
   Int_t iEMCAL = 1;
   Int_t iFMD   = 1;
   Int_t iFRAME = 1;
   Int_t iHALL  = 1;
   Int_t iITS   = 1;
   Int_t iMAG   = 1;
   Int_t iMUON  = 1;
   Int_t iPHOS  = 1;
   Int_t iPIPE  = 1;
   Int_t iPMD   = 1;
   Int_t iHMPID = 1;
   Int_t iSHIL  = 1;
   Int_t iT0    = 1;
   Int_t iTOF   = 1;
   Int_t iTPC   = 1;
   Int_t iTRD   = 1;
   Int_t iVZERO = 1;
   Int_t iZDC   = 1;
   

   //=================== Alice BODY parameters =============================
   AliBODY *BODY = new AliBODY("BODY", "Alice envelop");


   if (iMAG){
      //=================== MAG parameters ============================
      // --- Start with Magnet since detector layouts may be depending ---
      // --- on the selected Magnet dimensions ---
      AliMAG *MAG = new AliMAG("MAG", "Magnet");
   }


   if (iABSO){
      //=================== ABSO parameters ============================
      AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
   }

   if (iDIPO){
      //=================== DIPO parameters ============================
      AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
   }

   if (iHALL){
      //=================== HALL parameters ============================
      AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
   }


   if (iFRAME){
      //=================== FRAME parameters ============================
      AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
      FRAME->SetHoles(1);
   }

   if (iSHIL){
      //=================== SHIL parameters ============================
      AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
   }


   if (iPIPE) {
      //=================== PIPE parameters ============================
      AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
   }

   if (iITS){
      //=================== ITS parameters ============================
      AliITS *ITS  = new AliITSv11("ITS","ITS v11");
      //	AliITS *ITS  = new AliITSv11Hybrid("ITS","ITS v11Hybrid");
   }

   if (iTPC) {
      //============================ TPC parameters =====================
      AliTPC *TPC = new AliTPCv2("TPC", "Default");
   }


   if (iTOF) {
      //=================== TOF parameters ============================
      AliTOF *TOF = new AliTOFv6T0("TOF", "normal TOF");
   }

   if (iHMPID) {
      //=================== HMPID parameters ===========================
      AliHMPID *HMPID = new AliHMPIDv3("HMPID", "normal HMPID");
   }


   if (iZDC){
      //=================== ZDC parameters ============================
      AliZDC *ZDC = new AliZDCv3("ZDC", "normal ZDC");
      ZDC->SetSpectatorsTrack(); 
      ZDC->SetLumiLength(0.);
   }

   if (iTRD)
   {
      //=================== TRD parameters ============================
      AliTRD *TRD = new AliTRDv1("TRD", "TRD slow simulator");
      AliTRDgeometry *geoTRD = TRD->GetGeometry();
      // configuration for PbPb 2010
	geoTRD->SetSMstatus(2,0);
    	geoTRD->SetSMstatus(3,0);
	geoTRD->SetSMstatus(4,0);
        geoTRD->SetSMstatus(5,0);
        geoTRD->SetSMstatus(6,0);
        geoTRD->SetSMstatus(11,0);
        geoTRD->SetSMstatus(12,0);
        geoTRD->SetSMstatus(13,0);
        geoTRD->SetSMstatus(14,0);
        geoTRD->SetSMstatus(15,0);
        geoTRD->SetSMstatus(16,0); 
   }

   if (iFMD){
      //=================== FMD parameters ============================
      AliFMD *FMD = new AliFMDv1("FMD", "normal FMD");
   }

   if (iMUON) {
      //=================== MUON parameters ===========================
      // New MUONv1 version (geometry defined via builders)
      AliMUON *MUON = new AliMUONv1("MUON", "default");
      // activate trigger efficiency by cells
      MUON->SetTriggerEffCells(1); // not needed if raw masks
      MUON->SetTriggerResponseV1(2);
   }

   if (iPHOS){
      //=================== PHOS parameters ===========================
      AliPHOS *PHOS = new AliPHOSv1("PHOS", "noCPV_Modules123");
      AliPHOSSimParam *simParam = AliPHOSSimParam::GetInstance() ;
      simParam->SetCellNonLineairyA(0.001) ;
      simParam->SetCellNonLineairyB(0.2) ;
      simParam->SetCellNonLineairyC(1.0302) ;
   }

   if (iPMD){
      //=================== PMD parameters ============================
      AliPMD *PMD = new AliPMDv1("PMD", "normal PMD");
   }

   if (iT0){
      //=================== T0 parameters ============================
      AliT0 *T0 = new AliT0v1("T0", "T0 Detector");
   }

   if (iEMCAL){
      //=================== EMCAL parameters ============================
      AliEMCAL *EMCAL = new AliEMCALv2("EMCAL", "EMCAL_FIRSTYEARV1 ");
   }

   if (iACORDE){
      //=================== ACORDE parameters ============================
      AliACORDE *ACORDE = new AliACORDEv1("ACORDE", "normal ACORDE");
   }

   if (iVZERO){
      //=================== ACORDE parameters ============================
      AliVZERO *VZERO = new AliVZEROv7("VZERO", "normal VZERO");
   }
}

Float_t EtaToTheta(Float_t arg){
   return (180./TMath::Pi())*2.*atan(exp(-arg));
}

//
//           PYTHIA
//

AliGenerator* MbPythia() {
   comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
   AliGenPythia* pythia = new AliGenPythia(-1); 
   pythia->SetMomentumRange(0, 999999.);
   pythia->SetThetaRange(0., 180.);
   pythia->SetYRange(-12.,12.);
   pythia->SetPtRange(0,1000.);
   pythia->SetProcess(kPyMb);
   pythia->SetEnergyCMS(energy);
   
   return pythia;
}

AliGenerator* MbPythiaTuneD6T() {
   comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
   AliGenPythia* pythia = new AliGenPythia(-1); 
   pythia->SetMomentumRange(0, 999999.);
   pythia->SetThetaRange(0., 180.);
   pythia->SetYRange(-12.,12.);
   pythia->SetPtRange(0,1000.);
   pythia->SetProcess(kPyMb);
   pythia->SetEnergyCMS(energy);
//    Tune
//    109     D6T : Rick Field's CDF Tune D6T (NB: needs CTEQ6L pdfs externally)
   pythia->SetTune(109); // F I X 
   pythia->SetStrucFunc(kCTEQ6l);
//
   return pythia;
}

AliGenerator* MbPythiaTunePerugia0() {
   comment = comment.Append(" pp: Pythia low-pt (Perugia0)");
//
//    Pythia
   AliGenPythia* pythia = new AliGenPythia(-1); 
   pythia->SetMomentumRange(0, 999999.);
   pythia->SetThetaRange(0., 180.);
   pythia->SetYRange(-12.,12.);
   pythia->SetPtRange(0,1000.);
   pythia->SetProcess(kPyMb);
   pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
   pythia->SetTune(320); 
   pythia->UseNewMultipleInteractionsScenario();
//
   return pythia;
}


AliGenerator* MbPythiaTuneATLAS() {
   comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
   AliGenPythia* pythia = new AliGenPythia(-1); 
   pythia->SetMomentumRange(0, 999999.);
   pythia->SetThetaRange(0., 180.);
   pythia->SetYRange(-12.,12.);
   pythia->SetPtRange(0,1000.);
   pythia->SetProcess(kPyMb);
   pythia->SetEnergyCMS(energy);
//    Tune
//    C   306 ATLAS-CSC: Arthur Moraes' (new) ATLAS tune (needs CTEQ6L externally)
   pythia->SetTune(306);
   pythia->SetStrucFunc(kCTEQ6l);
//
   return pythia;
}

AliGenerator* PythiaJets() {
   comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
   AliGenPythia* pythia = new AliGenPythia(-1); 
   pythia->SetMomentumRange(0, 999999.);
   pythia->SetThetaRange(0., 180.);
   pythia->SetYRange(-12., 12.);
   pythia->SetPtRange(0, 1000.);
   pythia->SetProcess(kPyJets);
   pythia->SetEnergyCMS(energy);
   pythia->SetStrucFunc(kCTEQ6l);
   pythia->SetJetEtaRange(-1.5, 1.5); 
   pythia->SetJetEtRange(50., 800.);
   pythia->SetPtHard(45., 1000.);
   pythia->SetPycellParameters(2.2, 300, 432, 0., 4., 5., 0.7);
//
   return pythia;
}


AliGenerator* MbPhojet() {
   comment = comment.Append(" pp: Pythia low-pt");
//
//    DPMJET
#if defined(__CINT__)
gSystem->Load("libdpmjet");      // Parton density functions
gSystem->Load("libTDPMjet");      // Parton density functions
#endif
   AliGenDPMjet* dpmjet = new AliGenDPMjet(-1); 
   dpmjet->SetMomentumRange(0, 999999.);
   dpmjet->SetThetaRange(0., 180.);
   dpmjet->SetYRange(-12.,12.);
   dpmjet->SetPtRange(0,1000.);
   dpmjet->SetProcess(kDpmMb);
   dpmjet->SetEnergyCMS(energy);

   return dpmjet;
}


void ProcessEnvironmentVars() {
   // Random Number seed
   if (gSystem->Getenv("CONFIG_SEED")) {
      seed = atoi(gSystem->Getenv("CONFIG_SEED"));
   }
   
   if (gSystem->Getenv("CONFIG_RUN_TYPE")) {
      for (Int_t iRun = 0; iRun < kRunMax; iRun++) {
         if (strcmp(gSystem->Getenv("CONFIG_RUN_TYPE"), pprRunName[iRun])==0) {
         proc = (PprRun_t)iRun;
         cout<<"Run type set to "<<pprRunName[iRun]<<endl;
         }
      }
   }
   
   // Energy
   if (gSystem->Getenv("CONFIG_ENERGY")) {
      energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
      cout<<"Energy set to "<<energy<<" GeV"<<endl;
   }

     // Impact param

    if (gSystem->Getenv("CONFIG_BMIN")) {

      bMin = atof(gSystem->Getenv("CONFIG_BMIN"));

    }



    if (gSystem->Getenv("CONFIG_BMAX")) {

      bMax = atof(gSystem->Getenv("CONFIG_BMAX"));

    }

    cout<<"Impact parameter in ["<<bMin<<","<<bMax<<"]"<<endl;
   
}


AliGenerator* Hijing() {
   AliGenHijing *gener = new AliGenHijing(-1);
   // centre of mass energy 
   gener->SetEnergyCMS(2760.);
   gener->SetImpactParameterRange(bMin, bMax);	
   // reference frame
   gener->SetReferenceFrame("CMS");
   // projectile
   gener->SetProjectile("A", 208, 82);
   gener->SetTarget    ("A", 208, 82);
   // tell hijing to keep the full parent child chain
   gener->KeepFullEvent();
   // enable jet quenching
   gener->SetJetQuenching(1);
   // enable shadowing
   gener->SetShadowing(1);
   // Don't track spectators
   gener->SetSpectators(0);
   // kinematic selection
   gener->SetSelectAll(0);
   return gener;
}

AliGenerator* Hijing2000() {
   AliGenHijing *gener = (AliGenHijing*) Hijing();
   gener->SetJetQuenching(0);	
   gener->SetPtHardMin (2.3);
   return gener;
}

AliGenerator* Hijing2000GA() {
   comment = comment.Append(" PbPb: GA signals");

   AliGenCocktail *cocktail = new AliGenCocktail();
   cocktail->SetProjectile("A", 208, 82);
   cocktail->SetTarget    ("A", 208, 82);
   cocktail->SetEnergyCMS(energy);
   //
   // 1 Hijing event  
   TFormula* one    = new TFormula("one",    "1.");
   // provides underlying event and collision geometry 
   if  (proc == kHijing2000GA) { 
      AliGenHijing *hijing = (AliGenHijing*)Hijing2000();
      comment = comment.Append(" + HIJING");
      cocktail->AddGenerator(hijing,"hijing",1);
   }
   if  (proc == kAmptGA) { 
      AliGenAmpt *ampt = (AliGenAmpt*)Ampt();
      comment = comment.Append(" + AMPT");
      cocktail->AddGenerator(ampt,"ampt",1);
   }
   
   // Formula add 20 pi0 in peripheral and 40 pi0 in central collisions for PCM in between an exponential 
   // decrease is assumed
   TFormula* neutralsF    = new TFormula("neutrals",  "30.+ 30.*exp(- 0.5 * x * x / 5.12 / 5.12)");//
 
   //
   // Pi0
   // Flat pt spectrum in range 0..30
   // the function neutralsF controls how many pi0 are added, this depends on centrality - n AliGenBoxes are generated
   // Set pseudorapidity range from -1.2 to 1.2
   //
   AliGenBox *pi0 = new AliGenBox(1);
   pi0->SetPart(111);
   pi0->SetPtRange(0.,30.);
   pi0->SetPhiRange(0,360);
   pi0->SetYRange(-1.2,1.2);
   cocktail->AddGenerator(pi0,"pi0", 1, neutralsF);

   //
   // Eta
   // Flat pt spectrum in range 0..30
   // the function neutralsF controls how many etas are added, this depends on centrality - n AliGenBoxes are generated
   // Set pseudorapidity range from -1.2 to 1.2
   //
   AliGenBox *eta = new AliGenBox(1);
   eta->SetPart(221);
   eta->SetPtRange(0.,30.);
   eta->SetPhiRange(0,360);
   eta->SetYRange(-1.2,1.2);
   cocktail->AddGenerator(eta,"eta",1, neutralsF);

   //
   // 1 Pi0 in EMCAL, 2010 configuration, 4 SM
   AliGenParam *gEMCPi0 = GenParamCalo(1, AliGenPHOSlib::kPi0Flat, "EMCAL");
   cocktail->AddGenerator(gEMCPi0,"pi0EMC", 1);

   //
   // 1 Pi0 in PHOS
   AliGenParam *gPHSPi0 = GenParamCalo(1, AliGenPHOSlib::kPi0Flat, "PHOS");
   cocktail->AddGenerator(gPHSPi0,"pi0PHS", 1);

   //
   // 1 Eta in EMCAL, 2010 configuration, 4 SM
   AliGenParam *gEMCEta = GenParamCalo(1, AliGenPHOSlib::kEtaFlat, "EMCAL");
   cocktail->AddGenerator(gEMCEta,"etaEMC", 1);

   //
   // 1 Pi0 in PHOS
   AliGenParam *gPHSEta = GenParamCalo(1, AliGenPHOSlib::kEtaFlat, "PHOS");
   cocktail->AddGenerator(gPHSEta,"etaPHS", 1);


   return cocktail;
}

AliGenerator* Hydjet() {
   AliGenUHKM *genHi = new AliGenUHKM(-1);
   genHi->SetAllParametersLHC();
   genHi->SetProjectile("A", 208, 82);
   genHi->SetTarget    ("A", 208, 82);
   genHi->SetEcms(2760);
   genHi->SetEnergyCMS(2760.);
   genHi->SetBmin(bMin);
   genHi->SetBmax(bMax);
   genHi->SetPyquenPtmin(9);
   return genHi;
}

AliGenerator* Dpmjet() {
   AliGenDPMjet* dpmjet = new AliGenDPMjet(-1); 
   dpmjet->SetEnergyCMS(energy);
   dpmjet->SetProjectile("A", 208, 82);
   dpmjet->SetTarget    ("A", 208, 82);
   dpmjet->SetImpactParameterRange(bMin, bMax);
   dpmjet->SetPi0Decay(0);
   return dpmjet;
}

AliGenerator* Ampt()
{
   AliGenAmpt *genHi = new AliGenAmpt(-1);
   genHi->SetEnergyCMS(2760);
   genHi->SetReferenceFrame("CMS");
   genHi->SetProjectile("A", 208, 82);
   genHi->SetTarget    ("A", 208, 82);
   genHi->SetPtHardMin (2);
   genHi->SetImpactParameterRange(bMin,bMax);
   genHi->SetJetQuenching(0); // enable jet quenching
   genHi->SetShadowing(1);    // enable shadowing
   genHi->SetDecaysOff(1);    // neutral pion and heavy particle decays switched off
   genHi->SetSpectators(0);   // track spectators 
   genHi->KeepFullEvent();
   genHi->SetSelectAll(0);
   return genHi;
}




AliGenerator * GenParamCalo(Int_t nPart, Int_t type, TString calo) {
   // nPart of type (Pi0, Eta, Pi0Flat, EtaFlat, ...) in EMCAL or PHOS
   // CAREFUL EMCAL year 2011 configuration
   AliGenParam *gener = new AliGenParam(nPart,new AliGenPHOSlib(),type,"");
   
   // meson cuts
   gener->SetMomentumRange(0,999);
   
   if(calo=="EMCAL") {
      //meson acceptance
      gener->SetYRange(-0.8,0.8);
      gener->SetPtRange(5,35);
      // photon cuts
      gener->SetForceDecay(kGammaEM); // Ensure the decays are photons
      gener->SetCutOnChild(1);
      gener->SetChildPtRange(0.,30);
      gener->SetPhiRange(80., 120.); // year 2010
      gener->SetThetaRange(EtaToTheta(0.7),EtaToTheta(-0.7));
      //decay acceptance
      gener->SetChildThetaRange(EtaToTheta(0.7),EtaToTheta(-0.7));
      gener->SetChildPhiRange(80., 120.); // year 2010
   } else if(calo=="PHOS") {
      gener->SetYRange(-0.155,0.155);
      gener->SetPtRange(0.5,25);
      // photon cuts
      gener->SetForceDecay(kGammaEM); // Ensure the decays are photons
      gener->SetCutOnChild(1);
      gener->SetChildPtRange(0.,30);
      
      //meson acceptance
      gener->SetPhiRange(260., 320.);
      gener->SetThetaRange(EtaToTheta(0.13),EtaToTheta(-0.13));
      //decay acceptance
      gener->SetChildThetaRange(EtaToTheta(0.13),EtaToTheta(-0.13));
      gener->SetChildPhiRange(260., 320.);
   }
   
   return gener;
  
}
