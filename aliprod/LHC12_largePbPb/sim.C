void sim(Int_t nev=25 ) {
   AliSimulation simulator;

   simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD");
   simulator.SetMakeDigitsFromHits("ITS TPC");
//
//
// RAW OCDB
   simulator.SetDefaultStorage("alien://folder=/alice/data/2010/OCDB");


// ZDC (http://savannah.cern.ch/task/?33180)
// 

   simulator.SetSpecificStorage("ZDC/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/"); 

// ITS  (1 Total)
//     Alignment from Ideal OCDB 

   simulator.SetSpecificStorage("ITS/Align/Data",  "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");

//
// TPC (6 total) 
//

   simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
   simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
   simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
   simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
   simulator.SetSpecificStorage("TPC/Align/Data",           "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");
   simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal/");

//
// MUON Tracker
//
   simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  
//
// Vertex and Mag.field from OCDB
//
   simulator.UseVertexFromCDB();
   simulator.UseMagFieldFromGRP();
   simulator.SetRunHLT("");
   
//
// The rest
//
  printf("Before simulator.Run(nev);\n");
  simulator.Run(nev);
  printf("After simulator.Run(nev);\n");
}
