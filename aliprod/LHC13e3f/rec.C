void rec() {  
  AliReconstruction reco;

//
// switch off cleanESD, write ESDfriends and Alignment data
  
  reco.SetCleanESD(kFALSE);
  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();
  reco.SetFractionFriends(.1);
//

// OADB call  
// from https://savannah.cern.ch/task/?39374#comment85

  reco.SetOption("TPC","PID.OADB=TSPLINE3_MC_%s_LHC13B2_FIXn1_PASS1_PPB_MEAN"); 

// ITS Efficiency and tracking errors

  reco.SetRunPlaneEff(kTRUE);
  reco.SetUseTrackingErrorsForAlignment("ITS");

//
// RAW OCDB
  AliCDBManager* man = AliCDBManager::Instance();
  man->SetDefaultStorage("alien://Folder=/alice/data/2010/OCDB");
  man->SetSpecificStorage("ITS/Align/Data",     "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
  man->SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");


//
// TPC (25 objects)


  man->SetSpecificStorage("TPC/Align/Data", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  man->SetSpecificStorage("TPC/Calib/ClusterParam", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  man->SetSpecificStorage("TPC/Calib/RecoParam", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  man->SetSpecificStorage("TPC/Calib/TimeGain", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  man->SetSpecificStorage("TPC/Calib/AltroConfig", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  man->SetSpecificStorage("TPC/Calib/TimeDrift", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  man->SetSpecificStorage("TPC/Calib/Correction", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/"); 

  // >>>------------ override some settings in the ITS reco ------------------ >>>
  AliRunLoader *rl = AliRunLoader::Open("galice.root");
  if (!rl) {printf("Did not find galice.root\n"); exit(1);}
  rl->LoadHeader();
  if (!rl->GetHeader()) {printf("Failed to load run header from galice.root\n"); exit(1);}
  int runNumber = rl->GetHeader()->GetRun();
  printf("Overriding ITS/Calib/RecoParam for run %d to do reco even in absence of trigger\n",runNumber);
  man->SetRun(runNumber);
  AliCDBEntry* obj = man->Get("ITS/Calib/RecoParam");
  TObjArray* arrPar = (TObjArray*) obj->GetObject();
  AliITSRecoParam* par = (AliITSRecoParam*)arrPar->RemoveAt(1);
  par->SetSkipSubdetsNotInTriggerCluster(kFALSE);
  reco.SetRecoParam("ITS",par);
  // <<<------------ override some settings in the ITS reco ------------------ <<<

  //  reco.SetRunQA(":");
  reco.Run();
}

