void sim(Int_t nev=250) {
  AliSimulation simulator;

  simulator.SetMakeDigits("ITS TPC TRD TOF PHOS HMPID ZDC PMD T0 VZERO FMD EMCAL MUON");
  simulator.SetMakeSDigits("TRD TOF PHOS HMPID ZDC PMD T0 VZERO FMD EMCAL MUON");
  simulator.SetMakeDigitsFromHits("ITS TPC");
  simulator.SetRunQA(":");

  // RAW OCDB

    simulator.SetDefaultStorage("alien://Folder=/alice/data/2013/OCDB");

    // ITS (2 objects)

    simulator.SetSpecificStorage("ITS/Align/Data",     "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
    simulator.SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");  

    // MUON objects (1 object)

    simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Residual"); 

    // TPC (7 objects)

    //
    // TPC (3 new objects for Run2 MC) 

    simulator.SetSpecificStorage("TPC/Calib/Parameters",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");
    simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");
    simulator.SetSpecificStorage("TPC/Calib/RecoParam",	  "alien://Folder=/alice/simulation/2008/v4-15-Release/Full/");

    // TPC (5 old objects) 
    simulator.SetSpecificStorage("TPC/Align/Data",	     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
    simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
    simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
    simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
    simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/"); 

    //
    // EMCAL
    simulator.SetSpecificStorage("EMCAL/Align/Data"         ,"alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Calib/Data"         ,"alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Calib/Mapping"      ,"alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Calib/PeakFinder"   ,"alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Calib/Pedestals"    ,"alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Calib/RecoParam"    ,"alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Calib/SimParam"     ,"alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Calib/Trigger"      ,"alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Config/Preprocessor","alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("EMCAL/Config/Temperature" ,"alien://Folder=/alice/data/2015/OCDB");

    // ZDC
    simulator.SetSpecificStorage("ZDC/Align/Data","alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

    // PHOS
    // see https://alice.its.cern.ch/jira/browse/ALIROOT-5623?focusedCommentId=147957&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-147957
    simulator.SetSpecificStorage("PHOS/Align/Data", "alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("PHOS/Calib/EmcBadChannels", "alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("PHOS/Calib/EmcGainPedestals", "alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("PHOS/Calib/Mapping", "alien://Folder=/alice/data/2015/OCDB");
    simulator.SetSpecificStorage("PHOS/Calib/RecoParam", "alien://Folder=/alice/data/2015/OCDB");
    
  // The rest
  //
  simulator.Run(nev);
//   simulator.RunTrigger();
}
