void rec() {

  AliReconstruction reco;

  //
  // switch off cleanESD, write ESDfriends and Alignment data
  
  reco.SetCleanESD(kFALSE);
  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();
  
  //
  // ITS Efficiency and tracking errors

  reco.SetRunPlaneEff(kTRUE);
  reco.SetUseTrackingErrorsForAlignment("ITS");
  
  //
  // Residual OCDB
  reco.SetDefaultStorage("alien://folder=/alice/data/2010/OCDB");
  //  reco.SetDefaultStorage("alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  
  // ITS (2 objects)
  
  reco.SetSpecificStorage("ITS/Align/Data",     "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://folder=/alice/simulation/2008/v4-15-Release/Residual");  
  
  // MUON objects (1 object)
  
  reco.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Residual"); 
  
  // TPC (7 objects)
  
  reco.SetSpecificStorage("TPC/Align/Data", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/ClusterParam", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/RecoParam", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/TimeGain", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/AltroConfig", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/TimeDrift", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  reco.SetSpecificStorage("TPC/Calib/Correction", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/"); 

  
//   // My ADDS
//
  // GRP from local OCDB
  //reco.SetSpecificStorage("GRP/GRP/Data",Form("local://%s",gSystem->pwd()));

//   // Clock phase from RAW OCDB 
//   reco.SetSpecificStorage("GRP/Calib/LHCClockPhase",   "alien://folder=/alice/data/2010/OCDB");
  
//   //
//   // TRD from RAW OCDB
//   reco.SetSpecificStorage("TRD/Calib/ChamberStatus",   "alien://folder=/alice/data/2010/OCDB");
//   reco.SetSpecificStorage("TRD/Calib/PadStatus",       "alien://folder=/alice/data/2010/OCDB");

  
// // EMCAL from RAW OCDB
//   reco.SetSpecificStorage("EMCAL/Calib/Data",          "alien://Folder=/alice/data/2010/OCDB");
//   reco.SetSpecificStorage("EMCAL/Calib/Pedestals",     "alien://Folder=/alice/data/2010/OCDB");
//   reco.SetSpecificStorage("EMCAL/Calib/RecoParam",     "alien://Folder=/alice/data/2010/OCDB"); 

// //
// // PHOS from RAW OCDB
//   reco.SetSpecificStorage("PHOS/Calib/EmcBadChannels", "alien://Folder=/alice/data/2010/OCDB"); 
//   reco.SetSpecificStorage("PHOS/Calib/RecoParam",      "alien://Folder=/alice/data/2010/OCDB"); 
//   reco.SetSpecificStorage("PHOS/Calib/Mapping",        "alien://Folder=/alice/data/2010/OCDB"); 

  //
  TStopwatch timer;
  timer.Start();
  reco.Run();
  timer.Stop();
  timer.Print();
}
