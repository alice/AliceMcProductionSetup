void sim(Int_t nev=3500) {

  //  gSystem->Setenv("DC_RUN","195344");
  

  AliSimulation simulator;


  //simulator.SetWriteRawData("ALL","raw.root",kFALSE);
  simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD");
  simulator.SetMakeDigitsFromHits("ITS TPC");
//
  man = AliCDBManager::Instance();
//
// RAW OCDB
  man->SetDefaultStorage("alien://Folder=/alice/data/2013/OCDB");

// Specific storages = 22 

//
// ITS  (1 Total)
//     Alignment from Ideal OCDB 

  man->SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

//
// MUON (1 object)

  man->SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal"); 

//
// TPC (6 total) 

man->SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
man->SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
man->SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
man->SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
man->SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
man->SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

man->SetSpecificStorage("TPC/Calib/RecoParam",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");

man->SetSpecificStorage("ZDC/Align/Data",		 "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal"); 

//
// Vertex and magfield


  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

  // PHOS simulation settings
  AliPHOSSimParam *simParam = AliPHOSSimParam::GetInstance();
  simParam->SetCellNonLineairyA(0.001);
  simParam->SetCellNonLineairyB(0.2);
  simParam->SetCellNonLineairyC(1.02);

  simulator.SetRunGeneratorOnly(kTRUE);
  /*
  simulator.SetRunGenerator(kTRUE);
  simulator.SetRunSimulation(kFALSE);
  simulator.SetRunHLT("");
  */
//
// The rest
//
  printf("Before simulator.Run(nev);\n");
  simulator.Run(nev);
  printf("After simulator.Run(nev);\n");
}
