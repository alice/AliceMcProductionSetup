void sim(Int_t nev=200) {
  
  AliSimulation simulator;
  simulator.SetTriggerConfig("MUON");  
  simulator.SetMakeDigits("MUON");
  simulator.SetMakeSDigits("MUON");
  simulator.SetMakeDigitsFromHits("");
  simulator.SetRunQA("MUON:ALL");
  simulator.SetRunHLT("");

  // raw OCDB
  simulator.SetDefaultStorage("alien://folder=/alice/data/2011/OCDB");
    
  // MUON tracker
  simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  // mean vertex from raw OCDB
  //simulator.SetSpecificStorage("GRP/Calib/MeanVertex","alien://folder=/alice/data/2013/OCDB");

 // simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();
  
  TStopwatch timer;
  timer.Start();
  simulator.Run(nev);
 // simulator.RunTrigger("","MUON");
  timer.Stop();
  timer.Print();
}
