#if !defined(__CINT__) || defined(__MAKECINT__)
#include <TChain.h>
#include <TSystem.h>
#include "AliAnalysisManager.h"
#include "AliESDInputHandler.h"
#include "AliAODHandler.h"
#include "AliAnalysisTaskESDfilter.h"
#include "AliAnalysisDataContainer.h"
#endif

void CreateAODfromESD(const char *inFileName = "AliESDs.root",
		      const char *outFileName = "AliAOD.root",
		      Bool_t bKineFilter = kTRUE) 
{
  
    gSystem->Load("libGeom");

    gSystem->Load("libTree");
    gSystem->Load("libPhysics");
    gSystem->Load("libHist");
    gSystem->Load("libVMC");
    gSystem->Load("libMinuit");
    gSystem->Load("libCGAL.so");
    gSystem->Load("libfastjet.so");
    gSystem->Load("libsiscone.so");
    gSystem->Load("libSISConePlugin.so");
    gSystem->Load("libSTEERBase");
    gSystem->Load("libESD");
    gSystem->Load("libAOD");
    gSystem->Load("libANALYSIS");
    gSystem->Load("libANALYSISalice");
    gSystem->Load("libCORRFW");
    gSystem->Load("libJETAN");
    gSystem->Load("libFASTJETAN");
    gSystem->Load("libPWGTools");
    gSystem->Load("libPWGmuon");
    gSystem->Load("libPWGJE");


    TChain *chain = new TChain("esdTree");
    // Steering input chain
    chain->Add(inFileName);
    AliAnalysisManager *mgr  = new AliAnalysisManager("ESD to AOD", "Analysis Manager");

    // Input
    AliESDInputHandler* inpHandler = new AliESDInputHandler();
    inpHandler->SetReadFriends(kFALSE);
    inpHandler->SetReadTags();
    mgr->SetInputEventHandler  (inpHandler);
    // Output
    AliAODHandler* aodHandler   = new AliAODHandler();
    aodHandler->SetOutputFileName(outFileName);
    mgr->SetOutputEventHandler(aodHandler);
    mgr->GetCommonInputContainer()->SetSpecialOutput();

    // MC Truth
    if(bKineFilter){
	AliMCEventHandler* mcHandler = new AliMCEventHandler();
	mgr->SetMCtruthEventHandler(mcHandler);
    }

    // ESD filter
    gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/AddTaskESDFilter.C");
    Int_t runFlag = 1000; // 2010 (runFlag/100=year)
    AliAnalysisTaskESDfilter *taskesdfilter = AddTaskESDFilter(bKineFilter,kFALSE, kFALSE, kFALSE, kFALSE, AliESDpid::kTOF_T0, kFALSE, kFALSE, kFALSE, runFlag);    

    // jets
    gROOT->LoadMacro("$ALICE_ROOT/PWGJE/macros/AddTaskJetCluster.C");
    AliAnalysisTaskJetCluster *taskCl = 0;
    //Int_t kHighPtFilterMask = 768;  // 2011: hybrid tracks: 1)global+ITS info & 2)global constrained to SPD vertex
    Int_t kHighPtFilterMask = 272;  // 2010: hybrid tracks: 1)global+ITS info & 2)global constrained to SPD vertex
    UInt_t iPhysicsSelectionFlag = 0; // set by pyshics selection and passed to the task, kMB, kUserDefined etc
    Float_t fTrackEtaWindow = 0.9;

    taskCl = AddTaskJetCluster("AOD","",kHighPtFilterMask,iPhysicsSelectionFlag,"ANTIKT",0.4,0,kTRUE,"",0.15,fTrackEtaWindow);
    taskCl->SetJetOutputMinPt(5.);
    taskCl = AddTaskJetCluster("AODMC2b","",kHighPtFilterMask,iPhysicsSelectionFlag,"ANTIKT",0.4,0,kTRUE,"",0.15,fTrackEtaWindow);
    taskCl->SetJetOutputMinPt(5.);

    // tags
    AliAnalysisTaskTagCreator* tagTask = new AliAnalysisTaskTagCreator("AOD Tag Creator");
    AliAnalysisDataContainer *coutTags = mgr->CreateContainer("cTag",  TTree::Class(),
							      AliAnalysisManager::kOutputContainer, "AOD.tag.root");
    mgr->ConnectInput (tagTask, 0, mgr->GetCommonInputContainer());
    mgr->ConnectOutput(tagTask, 1, coutTags);

    AliLog::SetGlobalLogLevel(AliLog::kError);
    mgr->InitAnalysis();
    mgr->PrintStatus();
    mgr->StartAnalysis("local", chain);
}

