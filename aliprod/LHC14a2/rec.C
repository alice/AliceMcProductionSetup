void rec(Bool_t useHLT = kFALSE) {  
  AliReconstruction reco;


//
// switch off cleanESD, write ESDfriends and Alignment data

  reco.SetCleanESD(kFALSE);
  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();
  reco.SetFractionFriends(.1);

//

  //reco.SetWriteESDfriend(kFALSE);
  //reco.SetWriteAlignmentData();

  //reco.SetRunReconstruction("ITS TPC TRD TOF VZERO T0 ZDC");


// OADB call  
// from https://savannah.cern.ch/task/?39374#comment85

  reco.SetOption("TPC","PID.OADB=TSPLINE3_MC_%s_LHC13B2_FIXn1_PASS1_PPB_MEAN"); 

// ITS Efficiency and tracking errors

  reco.SetRunPlaneEff(kTRUE);
  reco.SetUseTrackingErrorsForAlignment("ITS");

// RAW OCDB

  //reco.SetDefaultStorage("local:///cvmfs/alice.gsi.de/alice/data/2013/OCDB");

  reco.SetDefaultStorage("alien://Folder=/alice/data/2013/OCDB");



  // ITS (2 objects)                                                                                                                    
  reco.SetSpecificStorage("ITS/Align/Data",          "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");

  // MUON (1 object)                                                                                                                    
  reco.SetSpecificStorage("MUON/Align/Data",         "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");

  // TPC (7 objects)                                                                                                                    
  reco.SetSpecificStorage("TPC/Align/Data",          "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/ClusterParam",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/RecoParam",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/TimeGain",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/AltroConfig",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/TimeDrift",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/Correction",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");

  //reco.SetRunQA(":") ;

  // -------------------------------------------------------                          

  //reco.SetOption("TPC", "useRAW");


  //  reco.SetRunQA(":");
  reco.Run();
}

