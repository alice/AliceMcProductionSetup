#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TH1.h>
#include <TRandom.h>
#include <TDatime.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include <TGeoGlobalMagField.h>
#include "STEER/STEER/AliRunLoader.h"
#include "STEER/STEER/AliRun.h"
#include "STEER/STEER/AliConfig.h"
#include "STEER/STEER/AliSimulation.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "PYTHIA6/AliGenPythia.h"
#include "PYTHIA6/AliGenPythiaPlus.h"
#include "TDPMjet/AliGenDPMjet.h"
#include "EVGEN/AliGenParam.h"
#include "EVGEN/AliGenMUONlib.h"
#include "EVGEN/AliGenPHOSlib.h"
#include "EVGEN/AliGenSTRANGElib.h"
#include "EVGEN/AliGenCocktail.h"
#include "EVGEN/AliGenBox.h"
#include "THijing/AliGenHijing.h"
#include "TAmpt/AliGenAmpt.h"
#include "TUHKMgen/AliGenUHKM.h"
#include "EVGEN/AliGenSlowNucleons.h"
#include "EVGEN/AliSlowNucleonModel.h"
#include "EVGEN/AliSlowNucleonModelExp.h"
#include "STEER/STEERBase/AliMagF.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "ITS/AliITSv11.h"
#include "TPC/AliTPCv2.h"
#include "TOF/AliTOFv6T0.h"
#include "HMPID/AliHMPIDv3.h"
#include "ZDC/AliZDCv4.h"
#include "TRD/AliTRDv1.h"
#include "TRD/AliTRDgeometry.h"
#include "FMD/AliFMDv1.h"
#include "MUON/AliMUONv1.h"
#include "PHOS/AliPHOSv1.h"
#include "PHOS/AliPHOSSimParam.h"
#include "PMD/AliPMDv1.h"
#include "T0/AliT0v1.h"
#include "EMCAL/AliEMCALv2.h"
#include "ACORDE/AliACORDEv1.h"
#include "VZERO/AliVZEROv7.h"
#endif


enum PDC06Proc_t
{
  kPythia6, kPythia6D6T, kPythia6ATLAS, kPythia6ATLAS_Flat, kPythiaPerugia0, kPhojet, kPythiaPerugia0chadr, kPythiaPerugia0bchadr, kPythiaPerugia0cele, kPythiaPerugia0bele, kPythiaPerugia0Jpsi2e, kPythiaPerugia0BtoJpsi2e, kPythiaPerugia0HFbarrel, kpPbPythiaPerugia0HFbarrel, kpPbPythiaPerugia0Jpsibarrel, kpPbPythiaJetJet, kRunMax
};

const char * pprRunName[] = {
  "kPythia6", "kPythia6D6T", "kPythia6ATLAS", "kPythia6ATLAS_Flat", "kPythiaPerugia0", "kPhojet", "kPythiaPerugia0chadr", "kPythiaPerugia0bchadr", "kPythiaPerugia0cele", "kPythiaPerugia0bele", "kPythiaPerugia0Jpsi2e", "kPythiaPerugia0BtoJpsi2e", "kPythiaPerugia0HFbarrel", "kpPbPythiaPerugia0HFbarrel", "kpPbPythiaPerugia0Jpsibarrel", "kpPbPythiaJetJet, kRunMax"
};

enum Mag_t
{
  kNoField, k5kG, kFieldMax
};

const char * pprField[] = {
  "kNoField", "k5kG"
};

enum PprTrigConf_t
{
    kDefaultPPTrig, kDefaultPbPbTrig
};

const char * pprTrigConfName[] = {
    "p-p","Pb-Pb"
};

//--- Functions ---
//class AliGenPythia;
AliGenerator *MbPythia();
AliGenerator *MbPythiaTuneD6T();
AliGenerator *MbPhojet();
AliGenerator *Hijing_pA(Bool_t slowN);

void ProcessEnvironmentVars();

// Generator, field, beam energy
static PDC06Proc_t   proc     = kpPbPythiaJetJet;
static Mag_t         mag      = k5kG;
static Float_t       pBeamEnergy = 4000.; // energy p-Beam
static Float_t       energy   = 2.*pBeamEnergy*TMath::Sqrt(82./208.); // energy in CMS
static Float_t       bMin     = 0.;
static Float_t       bMax     = 100.;
static Double_t      JpsiPol  = 0; // Jpsi polarisation
static Bool_t        JpsiHarderPt = kFALSE; // Jpsi harder pt spectrum (8.8 TeV)
static PprTrigConf_t strig = kDefaultPPTrig; // default pp trigger configuration
//========================//
// Set Random Number seed //
//========================//
TDatime dt;
static UInt_t seed    = dt.Get();

// Comment line
static TString comment;

void Config()
{


  // Get settings from environment variables
  ProcessEnvironmentVars();

  gRandom->SetSeed(seed);
  cerr<<"Seed for random number generation= "<<seed<<endl;

   // Libraries required by geant321
#if defined(__CINT__)
  gSystem->Load("liblhapdf");      // Parton density functions
  gSystem->Load("libEGPythia6");   // TGenerator interface
  if (proc == kPythia6 || proc == kPhojet ) {
    gSystem->Load("libpythia6");        // Pythia 6.2
    gSystem->Load("libAliPythia6");     // ALICE specific implementations
  } else{
    gSystem->Load("libpythia6_4_21");   // Pythia 6.4
    gSystem->Load("libAliPythia6");     // ALICE specific implementations	
  }

  gSystem->Load("libgeant321");
  // gSystem->Load("libpythia8.so");
  // gSystem->Load("libAliPythia8.so");
  // gSystem->Setenv("PYTHIA8DATA", gSystem->ExpandPathName("$ALICE_ROOT/PYTHIA8/pythia8145/xmldoc"));
  // gSystem->Setenv("LHAPDF",      gSystem->ExpandPathName("$ALICE_ROOT/LHAPDF"));
  // gSystem->Setenv("LHAPATH",     gSystem->ExpandPathName("$ALICE_ROOT/LHAPDF/PDFsets"));

  gSystem->Load("libhijing");	
  gSystem->Load("libTHijing");
  AliPDG::AddParticlesToPdgDataBase();

 // libraries for EvtGen
 gSystem->Load("libphotos.so");
 gSystem->Load("libEvtGenBase.so");
 gSystem->Load("libEvtGenModels");
 gSystem->Load("libEvtGen");
 gSystem->Load("libTEvtGen");

#endif

  new TGeant3TGeo("C++ Interface to Geant3");

  //=======================================================================
  //  Create the output file


  AliRunLoader* rl=0x0;

  cout<<"Config.C: Creating Run Loader ..."<<endl;
  rl = AliRunLoader::Open("galice.root",
			  AliConfig::GetDefaultEventFolderName(),
			  "recreate");
  if (rl == 0x0)
    {
      gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
      return;
    }
  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(1000);
  gAlice->SetRunLoader(rl);
  // gAlice->SetGeometryFromFile("geometry.root");
  // gAlice->SetGeometryFromCDB();

  // Set the trigger configuration
  AliSimulation::Instance()->SetTriggerConfig(pprTrigConfName[strig]);
  cout<<"Trigger configuration is set to  "<<pprTrigConfName[strig]<<endl;

  //
  //=======================================================================
  // ************* STEERING parameters FOR ALICE SIMULATION **************
  // --- Specify event type to be tracked through the ALICE setup
  // --- All positions are in cm, angles in degrees, and P and E in GeV


    gMC->SetProcess("DCAY",1);
    gMC->SetProcess("PAIR",1);
    gMC->SetProcess("COMP",1);
    gMC->SetProcess("PHOT",1);
    gMC->SetProcess("PFIS",0);
    gMC->SetProcess("DRAY",0);
    gMC->SetProcess("ANNI",1);
    gMC->SetProcess("BREM",1);
    gMC->SetProcess("MUNU",1);
    gMC->SetProcess("CKOV",1);
    gMC->SetProcess("HADR",1);
    gMC->SetProcess("LOSS",2);
    gMC->SetProcess("MULS",1);
    gMC->SetProcess("RAYL",1);

    Float_t cut = 1.e-3;        // 1MeV cut by default
    Float_t tofmax = 1.e10;

    gMC->SetCut("CUTGAM", cut);
    gMC->SetCut("CUTELE", cut);
    gMC->SetCut("CUTNEU", cut);
    gMC->SetCut("CUTHAD", cut);
    gMC->SetCut("CUTMUO", cut);
    gMC->SetCut("BCUTE",  cut);
    gMC->SetCut("BCUTM",  cut);
    gMC->SetCut("DCUTE",  cut);
    gMC->SetCut("DCUTM",  cut);
    gMC->SetCut("PPCUTM", cut);
    gMC->SetCut("TOFMAX", tofmax);

    
  //======================//
  // Set External decayer //
  //======================//
  TVirtualMCDecayer* decayer = new AliDecayerPythia();
  
  decayer->SetForceDecay(kAll);
 

  decayer->Init();
  gMC->SetExternalDecayer(decayer);



  //=========================//
  // Generator Configuration //
  //=========================//
  AliGenerator* gener = 0x0;

  if (proc == kPythia6) {
      gener = MbPythia();
  } else if (proc == kPythia6D6T) {
      gener = MbPythiaTuneD6T();
  } else if (proc == kPythia6ATLAS) {
      gener = MbPythiaTuneATLAS();
  } else if (proc == kPythiaPerugia0) {
      gener = MbPythiaTunePerugia0();
  } else if (proc == kPythia6ATLAS_Flat) {
      gener = MbPythiaTuneATLAS_Flat();
  } else if (proc == kPhojet) {
      gener = MbPhojet();
  } else if (proc == kpPbPythiaJetJet) {
      gener = HijingpAjet();
  }

  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // Size of the interaction diamond
  // Longitudinal
  Float_t sigmaz  = 6.245; // [cm]
  
  //
  // Transverse
  // Float_t betast  = 10.0;                     // beta* [m]
  // Float_t eps     = 3.75e-6;                  // emittance [m]
  // Float_t gamma   = pBeamEnergy / 0.938272;  // relativistic gamma [1]
  // Float_t sigmaxy = TMath::Sqrt(eps * betast / gamma) / TMath::Sqrt(2.) * 100.;  // [cm]
  Float_t sigmax = 0.0025;
  Float_t sigmay = 0.0029;
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  printf("\n \n Diamond size x-y: %10.3e %10.3e z: %10.3e\n \n", sigmax, sigmay, sigmaz);
   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
  gener->SetOrigin(0., 0., 0.); // Taken from OCDB
  gener->SetSigma(sigmax, sigmay, sigmaz);      // Sigma in (X,Y,Z) (cm) on IP position, sigmaz taken from OCDB
  gener->SetVertexSmear(kPerEvent);
  gener->Init();

  printf("\n \n Comment: %s \n \n", comment.Data());

    //	
   // FIELD
   //


 //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  TGeoGlobalMagField::Instance()->SetField(new AliMagF("Maps","Maps", -1., -1., AliMagF::k5kG,
     	   	AliMagF::kBeamTypepA, 82.*pBeamEnergy/208.));


  rl->CdGAFile();

  Int_t iABSO  = 1;
  Int_t iACORDE= 1;
  Int_t iDIPO  = 1;
  Int_t iEMCAL = 1;
  Int_t iFMD   = 1;
  Int_t iFRAME = 1;
  Int_t iHALL  = 1;
  Int_t iITS   = 1;
  Int_t iMAG   = 1;
  Int_t iMUON  = 1;
  Int_t iPHOS  = 1;
  Int_t iPIPE  = 1;
  Int_t iPMD   = 1;
  Int_t iHMPID = 1;
  Int_t iSHIL  = 1;
  Int_t iT0    = 1;
  Int_t iTOF   = 1;
  Int_t iTPC   = 1;
  Int_t iTRD   = 1;
  Int_t iVZERO = 1;
  Int_t iZDC   = 0;

    //=================== Alice BODY parameters =============================
    AliBODY *BODY = new AliBODY("BODY", "Alice envelop");


    if (iMAG)
    {
        //=================== MAG parameters ============================
        // --- Start with Magnet since detector layouts may be depending ---
        // --- on the selected Magnet dimensions ---
        AliMAG *MAG = new AliMAG("MAG", "Magnet");
    }


    if (iABSO)
    {
        //=================== ABSO parameters ============================
        AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
    }

    if (iDIPO)
    {
        //=================== DIPO parameters ============================

        AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
    }

    if (iHALL)
    {
        //=================== HALL parameters ============================

        AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
    }


    if (iFRAME)
    {
        //=================== FRAME parameters ============================

        AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
	FRAME->SetHoles(1);
    }

    if (iSHIL)
    {
        //=================== SHIL parameters ============================

        AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
    }


    if (iPIPE)
    {
        //=================== PIPE parameters ============================

        AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
    }
 
    if (iITS)
    {
        //=================== ITS parameters ============================

	AliITS *ITS  = new AliITSv11("ITS","ITS v11");
	//	AliITS *ITS  = new AliITSv11Hybrid("ITS","ITS v11Hybrid");
    }

    if (iTPC)
    {
      //============================ TPC parameters =====================

        AliTPC *TPC = new AliTPCv2("TPC", "Default");
    }


    if (iTOF) {
        //=================== TOF parameters ============================

	AliTOF *TOF = new AliTOFv6T0("TOF", "normal TOF");
    }


    if (iHMPID)
    {
        //=================== HMPID parameters ===========================

        AliHMPID *HMPID = new AliHMPIDv3("HMPID", "normal HMPID");

    }


    if (iZDC)
    {
        //=================== ZDC parameters ============================

        AliZDC *ZDC = new AliZDCv4("ZDC", "normal ZDC");
	ZDC->SetLumiLength(0.);
	ZDC->SetpAsystem();
	ZDC->SetBeamEnergy(82.*pBeamEnergy/208.);
    }

    if (iTRD)
    {
        //=================== TRD parameters ============================
 //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        AliTRD *TRD = new AliTRDv1("TRD", "TRD slow simulator");
        AliTRDgeometry *geoTRD = TRD->GetGeometry();
	// Partial geometry: modules at 0,1,7,8,9,16,17 +11,15,16
	// starting at 3h in positive direction
        geoTRD->SetSMstatus(4,0);
        geoTRD->SetSMstatus(5,0);
        geoTRD->SetSMstatus(12,0);
        geoTRD->SetSMstatus(13,0);
        geoTRD->SetSMstatus(14,0); 
    }

    if (iFMD)
    {
        //=================== FMD parameters ============================

	AliFMD *FMD = new AliFMDv1("FMD", "normal FMD");
   }

    if (iMUON)
    {
        //=================== MUON parameters ===========================
        // New MUONv1 version (geometry defined via builders)

        AliMUON *MUON = new AliMUONv1("MUON", "default");
	// activate trigger efficiency by cells
	MUON->SetTriggerEffCells(1); // not needed if raw masks 
	MUON->SetTriggerResponseV1(2);
     }

    if (iPHOS)
    {
        //=================== PHOS parameters ===========================

     AliPHOS *PHOS = new AliPHOSv1("PHOS", "noCPV_Modules123");

    }


    if (iPMD)
    {
        //=================== PMD parameters ============================

        AliPMD *PMD = new AliPMDv1("PMD", "normal PMD");
    }

    if (iT0)
    {
        //=================== T0 parameters ============================
        AliT0 *T0 = new AliT0v1("T0", "T0 Detector");
    }

    if (iEMCAL)
    {
        //=================== EMCAL parameters ============================

        AliEMCAL *EMCAL = new AliEMCALv2("EMCAL", "EMCAL_COMPLETE12SMV1");
    }

     if (iACORDE)
    {
        //=================== ACORDE parameters ============================

        AliACORDE *ACORDE = new AliACORDEv1("ACORDE", "normal ACORDE");
    }

     if (iVZERO)
    {
        //=================== ACORDE parameters ============================

        AliVZERO *VZERO = new AliVZEROv7("VZERO", "normal VZERO");
    }



}
//
//           PYTHIA
//

AliGenerator* MbPythia()
{
      comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);

      return pythia;
}

AliGenerator* MbPythiaTuneD6T()
{
      comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    109     D6T : Rick Field's CDF Tune D6T (NB: needs CTEQ6L pdfs externally)
      pythia->SetTune(109); // F I X
      pythia->SetStrucFunc(kCTEQ6l);
//
      return pythia;
}

AliGenerator* MbPythiaTunePerugia0()
{
      comment = comment.Append(" pp: Pythia low-pt (Perugia0)");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320);
      pythia->UseNewMultipleInteractionsScenario();
//
      return pythia;
}

AliGenerator* MbPythiaTunePerugia0chadr()
{
      comment = comment.Append(" pp: Pythia (Perugia0) chadr (1 ccbar per event, 1 c-hadron in |y|<1.5, chadrons decay to hadrons");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-1.5,1.5);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyCharmppMNRwmi);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320);
      pythia->UseNewMultipleInteractionsScenario();
//
//    decays
      pythia->SetForceDecay(kHadronicDWithout4Bodies);

      return pythia;
}

AliGenerator* MbPythiaTunePerugia0bchadr()
{
      comment = comment.Append(" pp: Pythia (Perugia0) bchadr (1 bbbar per event, 1 c-hadron in |y|<1.5, chadrons decay to hadrons");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-1.5,1.5);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyBeautyppMNRwmi);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320);
      pythia->UseNewMultipleInteractionsScenario();
//
//    decays
      pythia->SetForceDecay(kHadronicDWithout4Bodies);

      return pythia;
}

AliGenerator* MbPythiaTunePerugia0c()
{
      comment = comment.Append(" pp: Pythia (Perugia0) cele (1 ccbar per event, 1 electron in |y|<1.2");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      //pythia->SetYRange(-2.,2.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyCharmppMNRwmi);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(350);
      pythia->UseNewMultipleInteractionsScenario();
//

      return pythia;
}

AliGenerator* MbPythiaTunePerugia0b()
{
      comment = comment.Append(" pp: Pythia (Perugia0) bele (1 bbbar per event, 1 electron in |y|<1.2");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      //pythia->SetYRange(-2.,2.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyBeautyppMNRwmi);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(350);
      pythia->UseNewMultipleInteractionsScenario();
//

      return pythia;
}

AliGenerator* MbPythiaTunePerugia0cele()
{
      comment = comment.Append(" pp: Pythia (Perugia0) cele (1 ccbar per event, 1 electron in |y|<1.2");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      //pythia->SetYRange(-2.,2.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyCharmppMNRwmi);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320);
      pythia->UseNewMultipleInteractionsScenario();
//
//    decays
      pythia->SetCutOnChild(1);
      pythia->SetPdgCodeParticleforAcceptanceCut(11);
      pythia->SetChildYRange(-1.2,1.2);
      pythia->SetChildPtRange(0,10000.);

//    write only HF sub event
      pythia->SetStackFillOpt(AliGenPythia::kHeavyFlavor);
      return pythia;
}

AliGenerator* MbPythiaTunePerugia0bele()
{
      comment = comment.Append(" pp: Pythia (Perugia0) bele (1 bbbar per event, 1 electron in |y|<1.2");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      //pythia->SetYRange(-2.,2.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyBeautyppMNRwmi);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320);
      pythia->UseNewMultipleInteractionsScenario();
//
//    decays
      pythia->SetCutOnChild(1);
      pythia->SetPdgCodeParticleforAcceptanceCut(11);
      pythia->SetChildYRange(-1.2,1.2);
      pythia->SetChildPtRange(0,10000.);
//    write only HF sub event
      pythia->SetStackFillOpt(AliGenPythia::kHeavyFlavor);

      return pythia;
}




AliGenerator* MbPythiaTunePerugia0Jpsi2e()
{
    comment = comment.Append("pp Cocktail: Pythia (Perugia0) + 1 Jpsi forced to dielectrons");
    //Generating a cocktail
    AliGenCocktail *gener = new AliGenCocktail();
    gener->UsePerEventRates();
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    320     Perugia 0
      pythia->SetTune(320);
      pythia->UseNewMultipleInteractionsScenario();

//
//  JPsi is the second ingredient of the cocktail
      //AliGenParam* jpsi = new AliGenParam(1, AliGenMUONlib::kJpsi, "CDF scaled", "Jpsi"); // 14 TeV
      AliGenParam *jpsi=0x0;
      if(JpsiHarderPt) jpsi = new AliGenParam(1, AliGenMUONlib::kJpsi, "CDF pp 8.8", "Jpsi");  // 8.8 TeV
      else jpsi = new AliGenParam(1, AliGenMUONlib::kJpsi, "CDF pp 7", "Jpsi");  // 7 TeV
      jpsi->SetPtRange(0.,999.);
      jpsi->SetYRange(-1.0, 1.0);
      jpsi->SetPhiRange(0.,360.);
      //jpsi->SetChildYRange(-0.9,0.9); // decaying electrons should be in TRD acceptance
      jpsi->SetForceDecay(kDiElectron);
      jpsi->Init();

    gener->AddGenerator(pythia, "Pythia", 1.);  // 1 is weighting factor : Rate per event
    gener->AddGenerator(jpsi,"JPsi",1);
//    gener->Init();

    return gener;
}

void MbPythiaTunePerugia0BtoJpsi2e(AliGenCocktail *gener)
{
  comment = comment.Append(" pp: Pythia (Perugia0) BtoJpsi (1 bbbar per event, 1 b-hadron in |y|<2, 1 J/psi in |y|<2");

  if(!gener) {
    printf("COCKTAIL NOT SET, creating a new one\n");
    gener=new AliGenCocktail();
  }
  //
  //    Pythia
  AliGenPythia* pythia = new AliGenPythia(-1);
  pythia->SetMomentumRange(0, 999999.);
  pythia->SetThetaRange(0., 180.);
  pythia->SetYRange(-2.,2.);
  pythia->SetPtRange(0,1000.);
  pythia->SetProcess(kPyBeautyppMNRwmi);
  pythia->SetEnergyCMS(energy);
  //    Tune
  //    320     Perugia 0
  pythia->SetTune(320);
  pythia->UseNewMultipleInteractionsScenario();
  //
  //    decays
  pythia->SetCutOnChild(1);
  pythia->SetPdgCodeParticleforAcceptanceCut(443);
  pythia->SetChildYRange(-2,2);
  pythia->SetChildPtRange(0,10000.);
  //
  //    decays
  pythia->SetForceDecay(kBJpsiUndecayed);
  
  pythia->SetStackFillOpt(AliGenPythia::kHeavyFlavor);

  gener->UsePerEventRates();
  
  // add evt gen part
  comment.Append(" EvtGen as J/psi decayer;");
  AliGenEvtGen *genEvt = new AliGenEvtGen();
  genEvt->SetForceDecay(kBJpsiDiElectron);
  genEvt->SetParticleSwitchedOff(AliGenEvtGen::kCharmPart);
  
  gener->AddGenerator(pythia,"BtoJpsi2e PYTHIA",1.);
  gener->AddGenerator(genEvt,"EvtGen",1.); 
  
}

AliGenerator* MbPythiaJetJet()
{
      comment = comment.Append(" pp: Pythia (Perugia 2011)");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      
      pythia->SetProcess(kPyJets);
      pythia->SetYRange(-3.,3.);
      pythia->SetJetEtaRange(-1.5,1.5);
      pythia->SetJetPhiRange(0,360);
      pythia->SetJetEtRange(10.,1000.);
      pythia->SetEnergyCMS(energy);
      pythia->SetStrucFunc(kCTEQ5L);
      pythia->UseNewMultipleInteractionsScenario();
//    Tune
//    350     Perugia 2011
      pythia->SetTune(350);
//

      return pythia;
}

AliGenerator* MbPythiaTuneATLAS()
{
      comment = comment.Append(" pp: Pythia low-pt");
//
//    Pythia
      AliGenPythia* pythia = new AliGenPythia(-1);
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
//    Tune
//    C   306 ATLAS-CSC: Arthur Moraes' (new) ATLAS tune (needs CTEQ6L externally)
      pythia->SetTune(306);
      pythia->SetStrucFunc(kCTEQ6l);
//
      return pythia;
}

AliGenerator* MbPythia8(){
  comment = comment.Append(":Pythia8 p-p");
  AliGenPythiaPlus* py8 = new AliGenPythiaPlus(AliPythia8::Instance());
  py8->SetProcess(kPyMbDefault);
  //   Centre of mass energy
  py8->SetEnergyCMS(energy);
  //   Initialize generator
  py8->SetEventListRange(-1, 10); 
  (AliPythia8::Instance())->ReadString("Random:setSeed = on");
  (AliPythia8::Instance())->ReadString(Form("Random:seed = %ld", seed%900000000)); 
  return py8;
}


AliGenerator* Hijing_pA(Bool_t slowN)
{

        AliGenCocktail *gener = 0x0;
        if (slowN) {
	  gener  = new AliGenCocktail();
	  gener->SetProjectile("A", 208, 82);
	  gener->SetTarget    ("P",   1,  1);
	  gener->SetEnergyCMS(energy);
	}

	AliGenHijing   *hijing = new AliGenHijing(-1);
// centre of mass energy 
	hijing->SetEnergyCMS(energy);
// impact parameter range
	hijing->SetImpactParameterRange(0., 100.);
// reference frame
	hijing->SetReferenceFrame("CMS");
	hijing->SetBoostLHC(1);
// projectile
	hijing->SetProjectile("A", 208, 82);
	hijing->SetTarget    ("P", 1, 1);
// tell hijing to keep the full parent child chain
	hijing->KeepFullEvent();
// enable jet quenching
	hijing->SetJetQuenching(0);
// enable shadowing
	hijing->SetShadowing(1);
// kinematic selection
	hijing->SetSelectAll(0);

	if (!slowN) {
	  // DO track spectators
	  hijing->SetSpectators(1);
	  return hijing;
	}
	else {
	  // Cocktail with slow nucleons generator
	  // DO NOT track spectators
	  hijing->SetSpectators(0);

	  AliGenSlowNucleons* gray   = new AliGenSlowNucleons(1);
	  AliCollisionGeometry* coll = hijing->CollisionGeometry();
	  AliSlowNucleonModelExp* model = new AliSlowNucleonModelExp();
	  //  Not yet in the release...
	  //	  model->SetSaturation(kTRUE);
	  gray->SetSlowNucleonModel(model);
	  gray->SetTarget(208,82);
	  gray->SetThetaDist(1);
	  gray->SetProtonDirection(1);
	  //	  gray->SetDebug(1);
	  gray->SetNominalCmsEnergy(2.*pBeamEnergy);
	  gray->NeedsCollisionGeometry();
	  gray->SetCollisionGeometry(coll);

	  gener->AddGenerator(hijing, "Hijing pPb", 1);
	  gener->AddGenerator(gray, "Gray Particles", 1);

	  return gener;
	}
}

AliGenerator* HijingpAPythiaTunePerugia0chadr()
{
 //   Cocktail for HF in pPb
 
  comment = comment.Append(" pPb: cocktail of 1 PYTHIA6 chadr (1 ccbar per event, 1 c-hadron in |y|<1.5, chadrons decay to hadrons) and 1 Hijing pPb event");

  AliGenCocktail *cocktail = new AliGenCocktail();
  cocktail->SetProjectile("p", 1, 1);
  cocktail->SetTarget    ("A", 208, 82);
  cocktail->SetEnergyCMS(energy);
  
  //    Pythia
  AliGenPythia* pythiachadr = MbPythiaTunePerugia0chadr();
  cocktail->AddGenerator(pythiachadr,"chadr PYTHIA",1);
  
  Int_t ncoll=GetNcollpPb();
  comment = comment.Append(Form(" Ncoll=%d\n",ncoll));
  if(ncoll>1){
    AliGenPythiaPlus* hij = Hijing_pA(kFALSE);
    cocktail->AddGenerator(hij,"Hijing UE",1);
  }

  return cocktail;
}

AliGenerator* HijingpAjet()
{
   comment = comment.Append("pPb: jet-jet 4 pt hard bins");

  AliGenCocktail *cocktail = new AliGenCocktail();
  cocktail->SetProjectile("p", 1, 1);
  cocktail->SetTarget    ("A", 208, 82);
  cocktail->SetEnergyCMS(energy);
   //
  Int_t ncoll=GetNcollpPb();
  comment = comment.Append(Form(" Ncoll=%d\n",ncoll));
  if(ncoll>1){
    AliGenPythiaPlus* hij = Hijing_pA(kFALSE);
    cocktail->AddGenerator(hij,"Hijing UE",1);
  }

   // Jets

   Float_t ptHmin[4]  =  {11.,21.,36.,57.};
   Float_t ptHmax[4]  =  {21.,36.,57.,-1};
   
   Float_t ranPTH = gRandom->Rndm();
   Int_t ptHbin =  -1;

     if (ranPTH < 0.1667) {
       ptHbin = 0;
     } 
     if (ranPTH >= 0.1667 && ranPTH < 0.3333) {
       ptHbin = 1;
     } 
     if (ranPTH >= 0.3333 && ranPTH < 0.6666) {
       ptHbin = 2;
     } 
     if (ranPTH >= 0.6666) {
       ptHbin = 3;
     } 


   TString namejet="pythia";     
   AliGenerator* pythiaLF = 0x0;  
   pythiaLF=MbPythiaJetJet();
   namejet=namejet.Append(Form("_jetjet_%1.0f",ptHmin[ptHbin]));
   cocktail->AddGenerator(pythiaLF, namejet.Data(), 1); 
   ((AliGenPythia*)pythiaLF)->SetPtHard(ptHmin[ptHbin],ptHmax[ptHbin]);
    
   return cocktail;
}


AliGenerator* HijingpAPythiaTunePerugia0Jpsi2e(){
//   Cocktail for HF in pPb
  comment = comment.Append(" pPb: cocktail of Ncoll Pythia (Perugia0) events: 1 of them with 1 Jpsi forced to e+e- ");


  AliGenCocktail *cocktail = new AliGenCocktail();
  cocktail->SetProjectile("p", 1, 1);
  cocktail->SetTarget    ("A", 208, 82);
  cocktail->SetEnergyCMS(energy);
  
  // Jpsi
  AliGenParam *jpsi=0x0;
  if(JpsiHarderPt) jpsi = new AliGenParam(1, AliGenMUONlib::kJpsi, "CDF pp 8.8", "Jpsi");  // 8.8 TeV
  else jpsi = new AliGenParam(1, AliGenMUONlib::kJpsi, "pPb 5.03", "Jpsi");  // 7 TeV
  jpsi->SetPtRange(0.,999.);
  jpsi->SetYRange(-1.0, 1.0);
  jpsi->SetPhiRange(0.,360.);
  jpsi->SetForceDecay(kNoDecay);
  jpsi->Init();  

  comment.Append(" EvtGen as J/psi decayer;");
  AliGenEvtGen *gene = new AliGenEvtGen(); 

  // In order to force (undecayed) particles to decay in specific decay channels,
  // you need to set a decay table in which all particles are listed you want to force and the decay
  // channels you want to have (with B.R.) for each one (otherwise the particle is decayed naturally).
  // By the line 'gene->SetForceDecay(kBJpsiDiElectron);'
  // you set the decay table $ALICE_ROOT/TEvtGen/EvtGen/DecayTable/BTOJPSITOELE.DEC
  // in which the decays for both B->J/psi and J/psi->e+e- are set.
  // So this decay table is ok  also if you want to decay just the J/psi.
  gene->SetForceDecay(kBJpsiDiElectron);
  // AliGenEvtGen (which decays particles, but practically works as a generator)
  // decays all particles that are leaved undecayed by the previous generator.
  // The line below is needed to instruct AliGenEvtGen that the particles to be decayed
  // should be searched among charmed (both open and hidden) particles.
  gene->SetParticleSwitchedOff(AliGenEvtGen::kCharmPart);
  
  cocktail->AddGenerator(jpsi,"JPsi",1);
  cocktail->AddGenerator(gene,"EvtGen",1);

  //    Pythia
  Int_t ncoll=GetNcollpPb();
  if(ncoll>1){
    AliGenPythiaPlus* hij = Hijing_pA(kFALSE);
    cocktail->AddGenerator(hij,"Hijing UE",1);
  }

  return cocktail;
 
}

AliGenerator* HijingpAPythiaTunePerugia0BtoJpsi2e(){
//   Cocktail for HF in pPb
   comment = comment.Append(" pPb: cocktail of Ncoll Pythia (Perugia0) events: 1 of them with 1 B->Jpsi->e+e- ");


  AliGenCocktail *cocktail = new AliGenCocktail();
  cocktail->SetProjectile("p", 1, 1);
  cocktail->SetTarget    ("A", 208, 82);
  cocktail->SetEnergyCMS(energy);
  
  MbPythiaTunePerugia0BtoJpsi2e(cocktail);
  
  Int_t ncoll=GetNcollpPb();
  if(ncoll>1){
    AliGenPythiaPlus* hij = Hijing_pA(kFALSE);
    cocktail->AddGenerator(hij,"Hijing UE",1);
  }

  return cocktail;
 
}


AliGenerator* MbPythiaTuneATLAS_Flat()
{
      AliGenPythia* pythia = MbPythiaTuneATLAS();

      comment = comment.Append("; flat multiplicity distribution");

      // set high multiplicity trigger
      // this weight achieves a flat multiplicity distribution
      TH1 *weight = new TH1D("weight","weight",201,-0.5,200.5);
      weight->SetBinContent(1,5.49443);
      weight->SetBinContent(2,8.770816);
      weight->SetBinContent(6,0.4568624);
      weight->SetBinContent(7,0.2919915);
      weight->SetBinContent(8,0.6674189);
      weight->SetBinContent(9,0.364737);
      weight->SetBinContent(10,0.8818444);
      weight->SetBinContent(11,0.531885);
      weight->SetBinContent(12,1.035197);
      weight->SetBinContent(13,0.9394057);
      weight->SetBinContent(14,0.9643193);
      weight->SetBinContent(15,0.94543);
      weight->SetBinContent(16,0.9426507);
      weight->SetBinContent(17,0.9423649);
      weight->SetBinContent(18,0.789456);
      weight->SetBinContent(19,1.149026);
      weight->SetBinContent(20,1.100491);
      weight->SetBinContent(21,0.6350525);
      weight->SetBinContent(22,1.351941);
      weight->SetBinContent(23,0.03233504);
      weight->SetBinContent(24,0.9574557);
      weight->SetBinContent(25,0.868133);
      weight->SetBinContent(26,1.030998);
      weight->SetBinContent(27,1.08897);
      weight->SetBinContent(28,1.251382);
      weight->SetBinContent(29,0.1391099);
      weight->SetBinContent(30,1.192876);
      weight->SetBinContent(31,0.448944);
      weight->SetBinContent(32,1);
      weight->SetBinContent(33,1);
      weight->SetBinContent(34,1);
      weight->SetBinContent(35,1);
      weight->SetBinContent(36,0.9999997);
      weight->SetBinContent(37,0.9999997);
      weight->SetBinContent(38,0.9999996);
      weight->SetBinContent(39,0.9999996);
      weight->SetBinContent(40,0.9999995);
      weight->SetBinContent(41,0.9999993);
      weight->SetBinContent(42,1);
      weight->SetBinContent(43,1);
      weight->SetBinContent(44,1);
      weight->SetBinContent(45,1);
      weight->SetBinContent(46,1);
      weight->SetBinContent(47,0.9999999);
      weight->SetBinContent(48,0.9999998);
      weight->SetBinContent(49,0.9999998);
      weight->SetBinContent(50,0.9999999);
      weight->SetBinContent(51,0.9999999);
      weight->SetBinContent(52,0.9999999);
      weight->SetBinContent(53,0.9999999);
      weight->SetBinContent(54,0.9999998);
      weight->SetBinContent(55,0.9999998);
      weight->SetBinContent(56,0.9999998);
      weight->SetBinContent(57,0.9999997);
      weight->SetBinContent(58,0.9999996);
      weight->SetBinContent(59,0.9999995);
      weight->SetBinContent(60,1);
      weight->SetBinContent(61,1);
      weight->SetBinContent(62,1);
      weight->SetBinContent(63,1);
      weight->SetBinContent(64,1);
      weight->SetBinContent(65,0.9999999);
      weight->SetBinContent(66,0.9999998);
      weight->SetBinContent(67,0.9999998);
      weight->SetBinContent(68,0.9999999);
      weight->SetBinContent(69,1);
      weight->SetBinContent(70,1);
      weight->SetBinContent(71,0.9999997);
      weight->SetBinContent(72,0.9999995);
      weight->SetBinContent(73,0.9999994);
      weight->SetBinContent(74,1);
      weight->SetBinContent(75,1);
      weight->SetBinContent(76,1);
      weight->SetBinContent(77,1);
      weight->SetBinContent(78,0.9999999);
      weight->SetBinContent(79,1);
      weight->SetBinContent(80,1);
      weight->SetEntries(526);

      Int_t limit = weight->GetRandom();
      pythia->SetTriggerChargedMultiplicity(limit, 1.4);

      comment = comment.Append(Form("; multiplicity threshold set to %d in |eta| < 1.4", limit));

      return pythia;
}

Int_t GetNcollpPb(){
  TH1F hncoll("hncoll","NColl pPb",33,-0.5,32.5);
  // hncoll.SetBinContent(1,0);
  // hncoll.SetBinContent(2,150170);
  // hncoll.SetBinContent(3,85468);
  // hncoll.SetBinContent(4,64200);
  // hncoll.SetBinContent(5,54258);
  // hncoll.SetBinContent(6,49386);
  // hncoll.SetBinContent(7,46834);
  // hncoll.SetBinContent(8,45393);
  // hncoll.SetBinContent(9,44949);
  // hncoll.SetBinContent(10,44299);
  // hncoll.SetBinContent(11,42847);
  // hncoll.SetBinContent(12,41139);
  // hncoll.SetBinContent(13,37417);
  // hncoll.SetBinContent(14,33280);
  // hncoll.SetBinContent(15,28078);
  // hncoll.SetBinContent(16,22840);
  // hncoll.SetBinContent(17,17503);
  // hncoll.SetBinContent(18,12724);
  // hncoll.SetBinContent(19,8592);
  // hncoll.SetBinContent(20,5850);
  // hncoll.SetBinContent(21,3689);
  // hncoll.SetBinContent(22,2189);
  // hncoll.SetBinContent(23,1254);
  // hncoll.SetBinContent(24,705);
  // hncoll.SetBinContent(25,360);
  // hncoll.SetBinContent(26,200);
  // hncoll.SetBinContent(27,90);
  // hncoll.SetBinContent(28,27);
  // hncoll.SetBinContent(29,17);
  // hncoll.SetBinContent(30,10);
  // hncoll.SetBinContent(31,1);
  // hncoll.SetBinContent(32,2);
  // hncoll.SetBinContent(33,0);

  hncoll.SetBinContent(1,0);
  hncoll.SetBinContent(2,148650);
  hncoll.SetBinContent(3,83963);
  hncoll.SetBinContent(4,63671);
  hncoll.SetBinContent(5,53898);
  hncoll.SetBinContent(6,49224);
  hncoll.SetBinContent(7,46702);
  hncoll.SetBinContent(8,44839);
  hncoll.SetBinContent(9,45146);
  hncoll.SetBinContent(10,44082);
  hncoll.SetBinContent(11,43401);
  hncoll.SetBinContent(12,41367);
  hncoll.SetBinContent(13,37797);
  hncoll.SetBinContent(14,33258);
  hncoll.SetBinContent(15,28085);
  hncoll.SetBinContent(16,22923);
  hncoll.SetBinContent(17,17406);
  hncoll.SetBinContent(18,12924);
  hncoll.SetBinContent(19,8781);
  hncoll.SetBinContent(20,5855);
  hncoll.SetBinContent(21,3874);
  hncoll.SetBinContent(22,2263);
  hncoll.SetBinContent(23,1295);
  hncoll.SetBinContent(24,714);
  hncoll.SetBinContent(25,348);
  hncoll.SetBinContent(26,182);
  hncoll.SetBinContent(27,82);
  hncoll.SetBinContent(28,38);
  hncoll.SetBinContent(29,17);
  hncoll.SetBinContent(30,5);
  hncoll.SetBinContent(31,4);
  hncoll.SetBinContent(32,2);
  hncoll.SetBinContent(33,1);

  Int_t nc=hncoll.GetRandom();
  return nc;
}

AliGenerator* MbPhojet()
{
      comment = comment.Append(" pp: Pythia low-pt");
//
//    DPMJET
#if defined(__CINT__)
  gSystem->Load("libdpmjet");      // Parton density functions
  gSystem->Load("libTDPMjet");      // Parton density functions
#endif
      AliGenDPMjet* dpmjet = new AliGenDPMjet(-1);
      dpmjet->SetMomentumRange(0, 999999.);
      dpmjet->SetThetaRange(0., 180.);
      dpmjet->SetYRange(-12.,12.);
      dpmjet->SetPtRange(0,1000.);
      dpmjet->SetProcess(kDpmMb);
      dpmjet->SetEnergyCMS(energy);

      return dpmjet;
}

void ProcessEnvironmentVars()
{
    // Run type
    if (gSystem->Getenv("CONFIG_RUN_TYPE")) {
      for (Int_t iRun = 0; iRun < kRunMax; iRun++) {
	if (strcmp(gSystem->Getenv("CONFIG_RUN_TYPE"), pprRunName[iRun])==0) {
	  proc = (PDC06Proc_t)iRun;
	  cout<<"Run type set to "<<pprRunName[iRun]<<endl;
	}
      }
    }

    // Field
    if (gSystem->Getenv("CONFIG_FIELD")) {
      for (Int_t iField = 0; iField < kFieldMax; iField++) {
	if (strcmp(gSystem->Getenv("CONFIG_FIELD"), pprField[iField])==0) {
	  mag = (Mag_t)iField;
	  cout<<"Field set to "<<pprField[iField]<<endl;
	}
      }
    }

    // Energy
    if (gSystem->Getenv("CONFIG_ENERGY")) {
      energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
      cout<<"Energy set to "<<energy<<" GeV"<<endl;
    }

    // Random Number seed
    if (gSystem->Getenv("CONFIG_SEED")) {
      seed = atoi(gSystem->Getenv("CONFIG_SEED"));
    }

    // Run number
    if (gSystem->Getenv("DC_RUN")) {
      runNumber = atoi(gSystem->Getenv("DC_RUN"));
    }
}
