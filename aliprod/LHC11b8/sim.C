void sim(Int_t nev=300) {
  AliSimulation simulator;
  simulator.SetWriteRawData("ALL","raw.root",kTRUE);
  simulator.SetMakeSDigits("TOF PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
  simulator.SetMakeDigits("ITS TPC TOF PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
  simulator.SetMakeDigitsFromHits("ITS TPC");
//
//
// Ideal OCDB
  simulator.SetDefaultStorage("alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
//
// Read GRP Data from RAW
  simulator.SetSpecificStorage("GRP/GRP/Data",                  "alien://Folder=/alice/data/2011/OCDB");
//
//
// Mean vertex from RAW OCDB 
  simulator.SetSpecificStorage("GRP/Calib/MeanVertexSPD",       "alien://folder=/alice/data/2011/OCDB"); 
  simulator.SetSpecificStorage("GRP/Calib/MeanVertex",          "alien://folder=/alice/data/2011/OCDB");
//
// Clock phase from RAW OCDB 
  simulator.SetSpecificStorage("GRP/Calib/LHCClockPhase",       "alien://folder=/alice/data/2011/OCDB");
//
// ITS
//    SDD from RAW OCDB
  simulator.SetSpecificStorage("ITS/Calib/CalibSDD",            "alien://Folder=/alice/data/2011/OCDB");
//    SSD
  simulator.SetSpecificStorage("ITS/Calib/NoiseSSD",            "alien://Folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("ITS/Calib/BadChannelsSSD",      "alien://Folder=/alice/data/2011/OCDB"); 
//
// TOF from RAW OCDB
  simulator.SetSpecificStorage("TOF/Calib/Config",             "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/ConfigNoise",        "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/CTPLatency",         "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/DCSData",            "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/DeltaBCOffset",      "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/HW",                 "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/Noise",              "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/ParOffline",         "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/ParOnline",          "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/ParOnlineDelay",     "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/Problematic",        "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/Pulser",             "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/ReadoutEfficiency",  "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/RecoParam",          "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/RunParams",          "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/SimHisto",           "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/Status",             "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/T0Fill",             "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TOF/Calib/T0FillOnlineCalib",  "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("TRIGGER/TOF/TriggerMask",      "alien://folder=/alice/data/2011/OCDB");
//
// VZERO
  simulator.SetSpecificStorage("VZERO/Calib/Data",              "alien://Folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("VZERO/Trigger/Data",            "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("VZERO/Calib/RecoParam",         "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("VZERO/Calib/TimeSlewing",       "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("VZERO/Calib/TimeDelays",        "alien://folder=/alice/data/2011/OCDB");
//
// FMD from RAW OCDB
  simulator.SetSpecificStorage("FMD/Calib/Pedestal",            "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("FMD/Calib/PulseGain",           "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("FMD/Calib/Dead",                "alien://folder=/alice/data/2011/OCDB");
  simulator.SetSpecificStorage("FMD/Calib/AltroMap",            "alien://folder=/alice/data/2011/OCDB");
//
// EMCAL from RAW OCDB
  simulator.SetSpecificStorage("EMCAL/Calib/Data",             "alien://Folder=/alice/data/2011/OCDB");
//
// MUON 
// Pedestals  
  simulator.SetSpecificStorage("MUON/Calib/Pedestals",          "alien://folder=/alice/data/2011/OCDB");
// MappingData
  simulator.SetSpecificStorage("MUON/Calib/MappingData",        "alien://folder=/alice/data/2011/OCDB");
// Trigger LuT 
  simulator.SetSpecificStorage("MUON/Calib/TriggerLut",         "alien://folder=/alice/data/2011/OCDB");
// Trigger efficiency 
  simulator.SetSpecificStorage("MUON/Calib/TriggerEfficiency",  "alien://folder=/alice/simulation/2008/v4-15-Release/Full");
//
// ZDC
  simulator.SetSpecificStorage("ZDC/Calib/Pedestals",           "alien://folder=/alice/data/2011/OCDB");
//
//
// Vertex and Mag.field from OCDB

  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

//
// The rest
//

  TStopwatch timer;
  timer.Start();
  simulator.Run(nev);
  timer.Stop();
  timer.Print();
}
