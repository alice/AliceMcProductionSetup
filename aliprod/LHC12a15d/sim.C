void sim(Int_t nev=300) {
    AliSimulation simulator;

    simulator.SetMakeSDigits("TOF TRD PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
    simulator.SetMakeDigitsFromHits("ITS TPC");

    //
    //
    // RAW OCDB
    simulator.SetDefaultStorage("alien://Folder=/alice/data/2011/OCDB");
    simulator.SetCDBSnapshotMode("OCDB_MCsim.root");

    // simulate HLT TPC clusters, trigger, and HLT ESD
    simulator.SetRunHLT("chains=TPC-compression,GLOBAL-Trigger,GLOBAL-esd-converter");

    // Specific storages = 3 

    //
    // ITS  1 Total)
    //     Alignment from Ideal OCDB 

    simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

    //
    // MUON (1 Total)
    //      MCH

    simulator.SetSpecificStorage("MUON/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

    //
    // TPC (1 total) 

    simulator.SetSpecificStorage("TPC/Calib/PadGainFactor",   "alien://Folder=/alice/data/2011/OCDB");

    //
    // Vertex and Mag.field from OCDB

    simulator.UseVertexFromCDB();
    simulator.UseMagFieldFromGRP();

    //
    // The rest
    //
    simulator.SetRunQA(":");
    simulator.Run(nev);
    WriteXsection();
}

WriteXsection()
{
 TPythia6 *pythia = TPythia6::Instance();
 pythia->Pystat(1);
 Double_t xsection = pythia->GetPARI(1);
 UInt_t    ntrials  = pythia->GetMSTI(5);

 TFile *file = new TFile("pyxsec.root","recreate");
 TTree   *tree   = new TTree("Xsection","Pythia cross section");
 TBranch *branch = tree->Branch("xsection", &xsection, "X/D");
 TBranch *branch = tree->Branch("ntrials" , &ntrials , "X/i");
 tree->Fill();



 tree->Write();
 file->Close();

 cout << "Pythia cross section: " << xsection
      << ", number of trials: " << ntrials << endl;
}

