void sim(Int_t nev=50)
{

  AliSimulation simulator;

  simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
  simulator.SetMakeDigitsFromHits("ITS TPC");
  
  //
  // Raw OCDB
  simulator.SetDefaultStorage("alien://Folder=/alice/data/2010/OCDB");

  //
  // ITS  (1 Total)
  //     Alignment from Ideal OCDB 
  simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");
  
  //
  // MUON (1 object)
  simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal"); 

  //
  // TPC (7 total) 
  simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  simulator.SetSpecificStorage("TPC/Calib/RecoParam", 	   "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual/");
  
  //
  // Vertex and Mag.field from OCDB
  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

  simulator.SetRunQA(":");

  //
  // The rest
  
  TStopwatch timer;
  timer.Start();
  simulator.Run(nev);
  timer.Stop();
  timer.Print();
}
