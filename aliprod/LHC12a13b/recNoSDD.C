void recNoSDD() {  
  AliReconstruction reco;
// Skip SDD layers
  AliITSRecoParam *itsRecoParam = AliITSRecoParam::GetLowFluxParam();

  // SKIP SDD
  itsRecoParam->SetLayerToSkip(2);
  itsRecoParam->SetLayerToSkip(3);

  // find independently ITS SA tracks
  itsRecoParam->SetSAUseAllClusters();
  itsRecoParam->SetOuterStartLayerSA(2);

  itsRecoParam->SetAllowProlongationWithEmptyRoad(kTRUE);

  // larger seach windows for SA (in case of large misalignments)
  itsRecoParam->SetFactorSAWindowSizes(2);

  // Misalignment syst errors decided at ITS meeting 25.03.2010
  // Errors in Z reduced on 11.10.2010 for SPD and SDD
  // additional error due to misal (B off)
  itsRecoParam->SetClusterMisalErrorY(0.0010,0.0010,0.0300,0.0300,0.0020,0.0020); // [cm]
  itsRecoParam->SetClusterMisalErrorZ(0.0050,0.0050,0.0050,0.0050,0.1000,0.1000); // [cm]
  // additional error due to misal (B on)
  itsRecoParam->SetClusterMisalErrorYBOn(0.0010,0.0030,0.0500,0.0500,0.0020,0.0020); // [cm]
  itsRecoParam->SetClusterMisalErrorZBOn(0.0050,0.0050,0.0050,0.0050,0.1000,0.1000); // [cm]
  //----

  // SDD configuration
  itsRecoParam->SetUseSDDCorrectionMaps(kTRUE); // changed 30.04.2010
  itsRecoParam->SetUseSDDClusterSizeSelection(kTRUE);
  itsRecoParam->SetMinClusterChargeSDD(30.);
  itsRecoParam->SetUseUnfoldingInClusterFinderSDD(kFALSE);

  // SDD configuration
  itsRecoParam->SetUseSDDCorrectionMaps(kTRUE); // changed 30.04.2010
  itsRecoParam->SetUseSDDClusterSizeSelection(kTRUE);
  itsRecoParam->SetMinClusterChargeSDD(30.);
  itsRecoParam->SetUseUnfoldingInClusterFinderSDD(kFALSE);

  // Plane Efficiency evaluation with tracklets Method
  itsRecoParam->SetIPlanePlaneEff(-1);  
  itsRecoParam->SetComputePlaneEff(kTRUE,kFALSE); 
  itsRecoParam->SetUseTrackletsPlaneEff(kTRUE);
  itsRecoParam->SetTrackleterPhiWindowL2(0.07);
  itsRecoParam->SetTrackleterZetaWindowL2(0.4);
  itsRecoParam->SetTrackleterPhiWindowL1(0.10);
  itsRecoParam->SetTrackleterZetaWindowL1(0.6);
  itsRecoParam->SetUpdateOncePerEventPlaneEff(kTRUE);
  itsRecoParam->SetMinContVtxPlaneEff(3);
  //itsRecoParam->SetOptTrackletsPlaneEff(kTRUE); // activate it for MC (very important) !
  // Removal of tracklets reconstructed in the SPD overlaps
  itsRecoParam->SetTrackleterRemoveClustersFromOverlaps(kTRUE);

  // V0 finder (A. Marin)
  itsRecoParam->GetESDV0Params()->SetMaxPidProbPionForb(0.9);

  itsRecoParam->SetEventSpecie(AliRecoParam::kLowMult);
  itsRecoParam->SetTitle("LowMult");

  reco.SetRecoParam("ITS",itsRecoParam);
//============================================

  reco.SetRunReconstruction("ITS TPC TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO");

  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();

  reco.SetDefaultStorage("local://OCDB");

  TStopwatch timer;
  timer.Start();
  reco.Run();
  timer.Stop();
  timer.Print();
}
