// One can use the configuration macro in compiled mode by
// root [0] gSystem->Load("libgeant321");
// root [0] gSystem->SetIncludePath("-I$ROOTSYS/include -I$ALICE_ROOT/include
//                   -I$ALICE_ROOT -I$ALICE/geant3/TGeant3");
// root [0] .x grun.C(1,"Config.C++")

#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TRandom.h>
#include <TDatime.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include <TGeoGlobalMagField.h>
#include "AliRunLoader.h"
#include "AliRun.h"
#include "AliConfig.h"
#include "AliSimulation.h"
#include "AliDecayerPythia.h"
#include "AliGenPythia.h"
#include "AliGenPythiaPlus.h"
#include "AliGenDPMjet.h"
#include "AliGenParam.h"
#include "AliGenMUONlib.h"
#include "AliGenPHOSlib.h"
#include "AliGenSTRANGElib.h"
#include "AliGenCocktail.h"
#include "AliGenDeuteron.h"
#include "AliGenCocktailAfterBurner.h"
#include "AliGenBox.h"
#include "AliGenHijing.h"
#include "AliGenAmpt.h"
#include "AliGenUHKM.h"
#include "AliGenSlowNucleons.h"
#include "AliSlowNucleonModel.h"
#include "AliSlowNucleonModelExp.h"
#include "AliMagF.h"
#include "AliBODY.h"
#include "AliMAG.h"
#include "AliABSOv3.h"
#include "AliDIPOv3.h"
#include "AliHALLv3.h"
#include "AliFRAMEv2.h"
#include "AliSHILv3.h"
#include "AliPIPEv3.h"
#include "AliITSv11.h"
#include "AliTPCv2.h"
#include "AliTOFv6T0.h"
#include "AliHMPIDv3.h"
#include "AliZDCv3.h"
#include "AliTRDv1.h"
#include "AliTRDgeometry.h"
#include "AliFMDv1.h"
#include "AliMUONv1.h"
#include "AliPHOSv1.h"
#include "AliPHOSSimParam.h"
#include "AliPMDv1.h"
#include "AliT0v1.h"
#include "AliEMCALv2.h"
#include "AliACORDEv1.h"
#include "AliVZEROv7.h"
#endif


enum PprRun_t 
{
  kPythia6, kPythiaTune350, kPhojet , kPythiaNucleiFlat, kPythiaDeuteron, kRunMax
};

const char * pprRunName[] = {
  "kPythia6", "kPythiaTune350", "kPhojet" , "kPythiaNucleiFlat", "kPythiaDeuteron"
};

enum Mag_t
{
  kNoField, k5kG, kFieldMax
};

const char * pprField[] = {
  "kNoField", "k5kG"
};

enum PprTrigConf_t
{
    kDefaultPPTrig, kDefaultPbPbTrig, kTrigMax
};

const char * pprTrigConfName[] = {
    "p-p","Pb-Pb"
};

//--- Functions ---
class AliGenPythia;
AliGenPythia* MbPythia();
AliGenPythia* MbPythiaTune350();
AliGenDPMjet* MbPhojet();
AliGenCocktail* MbPythiaNucleiFlat();
AliGenCocktailAfterBurner* MbPythiaDeuteron();

void ProcessEnvironmentVars();

// Geterator, field, beam energy
static PprRun_t      proc     = kPythiaNucleiFlat;
static Mag_t         mag      = k5kG;
static Float_t       energy   = 7000; // energy in CMS
static Int_t         runNumber = 0;
static PprTrigConf_t strig = kDefaultPPTrig; // default pp trigger configuration
//========================//
// Set Random Number seed //
//========================//
TDatime dt;
static UInt_t seed    = dt.Get();

// Comment line
static TString comment;

void Config()
{


  // Get settings from environment variables
  ProcessEnvironmentVars();

  gRandom->SetSeed(seed);
  cerr<<"Seed for random number generation= "<<seed<<endl; 

  // Libraries required by geant321
#if defined(__CINT__)
  gSystem->Load("liblhapdf");      // Parton density functions
  gSystem->Load("libEGPythia6");   // TGenerator interface
  if (proc == kPythia6 || proc == kPhojet) {
    gSystem->Load("libpythia6");        // Pythia 6.2
  } else {
    gSystem->Load("libpythia6.4.25");   // Pythia 6.4
  }
  gSystem->Load("libAliPythia6");  // ALICE specific implementations
  gSystem->Load("libgeant321");
#endif

  new TGeant3TGeo("C++ Interface to Geant3");

  //=======================================================================
  //  Create the output file

   
  AliRunLoader* rl=0x0;

  cout<<"Config.C: Creating Run Loader ..."<<endl;
  rl = AliRunLoader::Open("galice.root",
			  AliConfig::GetDefaultEventFolderName(),
			  "recreate");
  if (rl == 0x0)
    {
      gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
      return;
    }
  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(1000);
  gAlice->SetRunLoader(rl);
  // gAlice->SetGeometryFromFile("geometry.root");
  // gAlice->SetGeometryFromCDB();

    // Set the trigger configuration
    AliSimulation::Instance()->SetTriggerConfig(pprTrigConfName[strig]);
    cout<<"Trigger configuration is set to  "<<pprTrigConfName[strig]<<endl;

  //
  //=======================================================================
  // ************* STEERING parameters FOR ALICE SIMULATION **************
  // --- Specify event type to be tracked through the ALICE setup
  // --- All positions are in cm, angles in degrees, and P and E in GeV


    gMC->SetProcess("DCAY",1);
    gMC->SetProcess("PAIR",1);
    gMC->SetProcess("COMP",1);
    gMC->SetProcess("PHOT",1);
    gMC->SetProcess("PFIS",0);
    gMC->SetProcess("DRAY",0);
    gMC->SetProcess("ANNI",1);
    gMC->SetProcess("BREM",1);
    gMC->SetProcess("MUNU",1);
    gMC->SetProcess("CKOV",1);
    gMC->SetProcess("HADR",1);
    gMC->SetProcess("LOSS",2);
    gMC->SetProcess("MULS",1);
    gMC->SetProcess("RAYL",1);

    Float_t cut = 1.e-3;        // 1MeV cut by default
    Float_t tofmax = 1.e10;

    gMC->SetCut("CUTGAM", cut);
    gMC->SetCut("CUTELE", cut);
    gMC->SetCut("CUTNEU", cut);
    gMC->SetCut("CUTHAD", cut);
    gMC->SetCut("CUTMUO", cut);
    gMC->SetCut("BCUTE",  cut);
    gMC->SetCut("BCUTM",  cut);
    gMC->SetCut("DCUTE",  cut);
    gMC->SetCut("DCUTM",  cut);
    gMC->SetCut("PPCUTM", cut);
    gMC->SetCut("TOFMAX", tofmax);




  //======================//
  // Set External decayer //
  //======================//
  TVirtualMCDecayer* decayer = new AliDecayerPythia();
  decayer->SetForceDecay(kAll);
  decayer->Init();
  gMC->SetExternalDecayer(decayer);

  //=========================//
  // Generator Configuration //
  //=========================//
  AliGenerator* gener = 0;

  if (proc == kPythia6) {
      gener = MbPythia();
  } else if (proc == kPythiaTune350) {
      gener = MbPythiaTune350();
  } else if (proc == kPhojet) {
      gener = MbPhojet();
  } else if (proc == kPythiaNucleiFlat) {
      gener = MbPythiaNucleiFlat();
  } else if (proc == kPythiaDeuteron) {
      gener = MbPythiaDeuteron();
  }
  
  //
  //
  // Size of the interaction diamond
  // Longitudinal
  Float_t sigmaz  = 5.4 / TMath::Sqrt(2.); // [cm]
  if (energy == 900)
  {
    //sigmaz  = 10.5 / TMath::Sqrt(2.); // [cm]
    //sigmaz = 3.7;
  }
  if (energy == 7000)
    sigmaz  = 6.3 / TMath::Sqrt(2.); // [cm]

  //
  // Transverse
  Float_t betast  = 10;                 // beta* [m]
  if (runNumber >= 117048) betast = 2.;
  if (runNumber >  122375) betast = 3.5; // starting with fill 1179

  Float_t eps     = 5.0e-6;            // emittance [m]
  Float_t gamma   = energy / 2.0 / 0.938272;  // relativistic gamma [1]
  Float_t sigmaxy = TMath::Sqrt(eps * betast / gamma) / TMath::Sqrt(2.) * 100.;  // [cm]
  printf("\n \n Diamond size x-y: %10.3e z: %10.3e\n \n", sigmaxy, sigmaz);

  gener->SetSigma(sigmaxy, sigmaxy, sigmaz);      // Sigma in (X,Y,Z) (cm) on IP position
  gener->SetVertexSmear(kPerEvent);
  gener->Init();

 //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  TGeoGlobalMagField::Instance()->SetField(new AliMagF("Maps","Maps", -1., -1., AliMagF::k5kG,
     	   	AliMagF::kBeamTypepp));

    rl->CdGAFile();
//
    Int_t   iABSO   = 1;
    Int_t   iDIPO   = 1;
    Int_t   iFMD    = 1;
    Int_t   iFRAME  = 1;
    Int_t   iHALL   = 1;
    Int_t   iITS    = 1;
    Int_t   iMAG    = 1;
    Int_t   iMUON   = 1;
    Int_t   iPHOS   = 1;
    Int_t   iPIPE   = 1;
    Int_t   iPMD    = 1;
    Int_t   iHMPID  = 1;
    Int_t   iSHIL   = 1;
    Int_t   iT0     = 1;
    Int_t   iTOF    = 1;
    Int_t   iTPC    = 1;
    Int_t   iTRD    = 1;
    Int_t   iZDC    = 1;
    Int_t   iEMCAL  = 1;
    Int_t   iVZERO  = 1;
    Int_t   iACORDE = 1;

    //=================== Alice BODY parameters =============================
    AliBODY *BODY = new AliBODY("BODY", "Alice envelop");


    if (iMAG)
    {
        //=================== MAG parameters ============================
        // --- Start with Magnet since detector layouts may be depending ---
        // --- on the selected Magnet dimensions ---
        AliMAG *MAG = new AliMAG("MAG", "Magnet");
    }


    if (iABSO)
    {
        //=================== ABSO parameters ============================
        AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
    }

    if (iDIPO)
    {
        //=================== DIPO parameters ============================

        AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
    }

    if (iHALL)
    {
        //=================== HALL parameters ============================

        AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
    }


    if (iFRAME)
    {
        //=================== FRAME parameters ============================

        AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
	FRAME->SetHoles(1);
    }

    if (iSHIL)
    {
        //=================== SHIL parameters ============================

        AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
    }


    if (iPIPE)
    {
        //=================== PIPE parameters ============================

        AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
    }
 
    if (iITS)
    {
        //=================== ITS parameters ============================

	AliITS *ITS  = new AliITSv11("ITS","ITS v11");
    }

    if (iTPC)
    {
      //============================ TPC parameters =====================

        AliTPC *TPC = new AliTPCv2("TPC", "Default");
    }


    if (iTOF) {
        //=================== TOF parameters ============================

	AliTOF *TOF = new AliTOFv6T0("TOF", "normal TOF");
    }


    if (iHMPID)
    {
        //=================== HMPID parameters ===========================

        AliHMPID *HMPID = new AliHMPIDv3("HMPID", "normal HMPID");

    }


    if (iZDC)
    {
        //=================== ZDC parameters ============================
	
      	AliZDC *ZDC = new AliZDCv3("ZDC", "normal ZDC");
        //Collimators aperture
        ZDC->SetVCollSideCAperture(0.85);
        ZDC->SetVCollSideCCentre(0.);
        ZDC->SetVCollSideAAperture(0.75);
        ZDC->SetVCollSideACentre(0.);
        //Detector position
        ZDC->SetYZNC(1.6);
        ZDC->SetYZNA(1.6);
        ZDC->SetYZPC(1.6);
        ZDC->SetYZPA(1.6);
    }

    if (iTRD)
    {
        //=================== TRD parameters ============================

        AliTRD *TRD = new AliTRDv1("TRD", "TRD slow simulator");
        AliTRDgeometry *geoTRD = TRD->GetGeometry();
	// Partial geometry: modules at 0,1,7,8,9,16,17
	// starting at 3h in positive direction
	geoTRD->SetSMstatus(2,0);
	geoTRD->SetSMstatus(3,0);
	geoTRD->SetSMstatus(4,0);
        geoTRD->SetSMstatus(5,0);
	geoTRD->SetSMstatus(6,0);
        geoTRD->SetSMstatus(11,0);
        geoTRD->SetSMstatus(12,0);
        geoTRD->SetSMstatus(13,0);
        geoTRD->SetSMstatus(14,0);
        geoTRD->SetSMstatus(15,0);
        geoTRD->SetSMstatus(16,0);
    }

    if (iFMD)
    {
        //=================== FMD parameters ============================

	AliFMD *FMD = new AliFMDv1("FMD", "normal FMD");
   }

    if (iMUON)
    {
        //=================== MUON parameters ===========================
        // New MUONv1 version (geometry defined via builders)
	AliMUON *MUON = new AliMUONv1("MUON", "default");
	// activate trigger efficiency by cells
	MUON->SetTriggerEffCells(1);
    }

    if (iPHOS)
    {
        //=================== PHOS parameters ===========================

     AliPHOS *PHOS = new AliPHOSv1("PHOS", "noCPV_Modules123");

    }


    if (iPMD)
    {
        //=================== PMD parameters ============================

        AliPMD *PMD = new AliPMDv1("PMD", "normal PMD");
    }

    if (iT0)
    {
        //=================== T0 parameters ============================
        AliT0 *T0 = new AliT0v1("T0", "T0 Detector");
    }

    if (iEMCAL)
    {
        //=================== EMCAL parameters ============================

        AliEMCAL *EMCAL = new AliEMCALv2("EMCAL", "EMCAL_FIRSTYEARV1");
    }

     if (iACORDE)
    {
        //=================== ACORDE parameters ============================

        AliACORDE *ACORDE = new AliACORDEv1("ACORDE", "normal ACORDE");
    }

     if (iVZERO)
    {
        //=================== VZERO parameters ============================
        AliVZERO *VZERO = new AliVZEROv7("VZERO", "normal VZERO");
    }
}

AliGenPythia* MbPythia()
{
//
//    Pythia

      comment = comment.Append(" pp: Pythia");

      AliGenPythia* pythia = new AliGenPythia(-1); 
      pythia->SetMomentumRange(0, 999999.);
      pythia->SetThetaRange(0., 180.);
      pythia->SetYRange(-12.,12.);
      pythia->SetPtRange(0,1000.);
      pythia->SetProcess(kPyMb);
      pythia->SetEnergyCMS(energy);
      
      return pythia;
}

AliGenPythia* MbPythiaTune350()
{
// Pythia Perugia 2011 (350)
//
//
      AliGenPythia* pythia = MbPythia();
      
      comment = comment.Append(" tune 350 (Perugia2011)");
      
//    Tune
//    350    Perugia 2011
      pythia->SetTune(350); 
      pythia->UseNewMultipleInteractionsScenario();
      pythia->SetCrossingAngle(0,0.000280);

//
      return pythia;
}

AliGenDPMjet* MbPhojet()
{
//
//    DPMJET
      comment = comment.Append(" pp: PHOJET");

#if defined(__CINT__)
  gSystem->Load("libdpmjet");      // Parton density functions
  gSystem->Load("libTDPMjet");      // Parton density functions
#endif
      AliGenDPMjet* dpmjet = new AliGenDPMjet(-1);
      dpmjet->SetMomentumRange(0, 999999.);
      dpmjet->SetThetaRange(0., 180.);
      dpmjet->SetYRange(-12.,12.);
      dpmjet->SetPtRange(0,1000.);
      dpmjet->SetProcess(kDpmMb);
      dpmjet->SetEnergyCMS(energy);
      dpmjet->SetCrossingAngle(0,0.000280);
      return dpmjet;
}

AliGenCocktail* MbPythiaNucleiFlat()
{
//
// MB + flat pt-y (anti)nuclei
//
	Double_t ptmin = 0.;
	Double_t ptmax = 10.;
	Double_t ymin  = -1.5;
	Double_t ymax  = 1.5;
	Int_t npart    = 20;

	AliGenCocktail* gener = new AliGenCocktail();
	gener->SetProjectile("p", 1, 1);
	gener->SetTarget("p", 1, 1);
	gener->SetEnergyCMS(energy);
	gener->UsePerEventRates();
	gener->SetPtRange(ptmin, ptmax);
	gener->SetYRange(ymin, ymax);

	AliGenerator* pythia = MbPythiaTune350();
	gener->AddGenerator(pythia, "PYTHIA (350)", 1);

	comment = comment.Append("; light nuclei pt-y flat");
/*
	AliGenBox* proton = new AliGenBox(npart);
	proton->SetPart(2212);
	gener->AddGenerator(proton, "proton", 1);
*/
	AliGenBox* deuteron = new AliGenBox(npart);
	deuteron->SetPart(1000010020);
	gener->AddGenerator(deuteron, "deuteron", 1);

	AliGenBox* triton = new AliGenBox(npart);
	triton->SetPart(1000010030);
	gener->AddGenerator(triton, "triton", 1);

	AliGenBox* he3 = new AliGenBox(npart);
	he3->SetPart(1000020030);
	he3->SetPtRange(ptmin, 2.*ptmax);
	gener->AddGenerator(he3, "he3", 1);

	AliGenBox* alpha = new AliGenBox(5);
	alpha->SetPart(1000020040);
	alpha->SetPtRange(ptmin, 2.*ptmax);
	gener->AddGenerator(alpha, "alpha", 1);

	comment = comment.Append("; light antinuclei pt-y flat");
/*
	AliGenBox* antiproton = new AliGenBox(npart);
	antiproton->SetPart(-2212);
	gener->AddGenerator(antiproton, "antiproton", 1);
*/
	AliGenBox* antideuteron = new AliGenBox(npart);
	antideuteron->SetPart(-1000010020);
	gener->AddGenerator(antideuteron, "antideuteron", 1);

	AliGenBox* antitriton = new AliGenBox(npart);
	antitriton->SetPart(-1000010030);
	gener->AddGenerator(antitriton, "antitriton", 1);

	AliGenBox* antihe3 = new AliGenBox(npart);
	antihe3->SetPart(-1000020030);
	antihe3->SetPtRange(ptmin, 2.*ptmax);
	gener->AddGenerator(antihe3, "antihe3", 1);

	AliGenBox* antialpha = new AliGenBox(5);
	antialpha->SetPart(-1000020040);
	antialpha->SetPtRange(ptmin, 2.*ptmax);
	gener->AddGenerator(antialpha, "antialpha", 1);

	return gener;
}

AliGenCocktailAfterBurner* MbPythiaDeuteron()
{
//
// deuterons and antideuterons with Pythia
//
	AliGenCocktailAfterBurner* gener = new AliGenCocktailAfterBurner();
	gener->SetProjectile("p", 1, 1);
	gener->SetTarget("p", 1, 1);
	gener->SetEnergyCMS(energy);
	gener->UsePerEventRates();
	
	AliGenPythia* pythia = MbPythiaTune350();
	gener->AddGenerator(pythia, "PYTHIA (350)", 1);

	comment = comment.Append("; deuteron");
	
	AliGenDeuteron* deuteron = new AliGenDeuteron(1, 0.1);
	gener->AddAfterBurner(deuteron, "deuteron", 1);
	
	comment = comment.Append("; antideuteron");
	
	AliGenDeuteron* antideuteron = new AliGenDeuteron(-1, 0.1);
	gener->AddAfterBurner(antideuteron, "antideuteron", 1);
	
	return gener;
}

void ProcessEnvironmentVars()
{
    // Run type
    if (gSystem->Getenv("CONFIG_RUN_TYPE")) {
      for (Int_t iRun = 0; iRun < kRunMax; iRun++) {
	if (strcmp(gSystem->Getenv("CONFIG_RUN_TYPE"), pprRunName[iRun])==0) {
	  proc = (PprRun_t)iRun;
	  cout<<"Run type set to "<<pprRunName[iRun]<<endl;
	}
      }
    }

    // Field
    if (gSystem->Getenv("CONFIG_FIELD")) {
      for (Int_t iField = 0; iField < kFieldMax; iField++) {
	if (strcmp(gSystem->Getenv("CONFIG_FIELD"), pprField[iField])==0) {
	  mag = (Mag_t)iField;
	  cout<<"Field set to "<<pprField[iField]<<endl;
	}
      }
    }

    // Energy
    if (gSystem->Getenv("CONFIG_ENERGY")) {
      energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
      cout<<"Energy set to "<<energy<<" GeV"<<endl;
    }

    // Random Number seed
    if (gSystem->Getenv("CONFIG_SEED")) {
      seed = atoi(gSystem->Getenv("CONFIG_SEED"));
    }

    // Run number
    if (gSystem->Getenv("DC_RUN")) {
      runNumber = atoi(gSystem->Getenv("DC_RUN"));
    }
}
