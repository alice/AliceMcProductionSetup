#!/bin/bash

##################################################
validateout=`dirname $0`
validatetime=`date`
validated="0"
error=1
if [ -z $validateout ]; then
    validateout="."
fi

cd $validateout
validateworkdir=`pwd`

echo "*******************************************************" >> stdout
echo "* AliRoot Validation Script V1.0                      *" >> stdout
echo "* Time:    $validatetime " >> stdout
echo "* Dir:     $validateout" >> stdout
echo "* Workdir: $validateworkdir" >> stdout
echo "* ----------------------------------------------------*" >> stdout
echo "* Library path: $LD_LIBRARY_PATH" >> stdout
echo "* ----------------------------------------------------*" >> stdout
echo "* Path: $PATH" >> stdout
echo "* ----------------------------------------------------*" >> stdout
ls -la ./ >> stdout
echo "* ----------------------------------------------------*" >> stdout

if [ -f OCDB.generating.job ]; then
    echo "* This was a special OCDB.root job for which I'll skip the rest of the validation"

    cp stdout stdout.ocdb.log
    cp stderr stderr.ocdb.log

    if [ -f OCDB.root ]; then
        echo "* ODCB.root found"
        exit 0
    else
        echo "* OCDB.root NOT found! Failing validation"
        exit 1
    fi
fi

rm aod.log
##################################################
if [ -f rec.log ] && [ -f sim.log ] && [ -f check.log ] && [ -f tag.log ] && [ -f *ESD.tag.root ]; then
sv=`grep -i  "Segmentation violation" *.log`
if [ "$sv" = "" ]; then
    sf=`grep -i  "Segmentation fault" *.log`
    if [ "$sf" = "" ]; then
        be=`grep -i  "Bus error" *.log`
        if [ "$be" = "" ]; then
            ab=`grep -i -w "Abort" *.log`
            if [ "$ab" = "" ]; then
                fp=`grep -i  "Floating point exception" *.log`
                if [ "$fp" = "" ]; then
                    kl=`grep -i  "Killed" *.log`
                    if [ "$kl" = "" ]; then
                        bf=`grep -i "busy flag cleared" *.log`
                        if [ "$bf" = "" ]; then
                           ch=`grep -i "check of ESD was successfull" check.log`
                           if [ "$ch" = "" ]; then
                                echo "* #  The ESD was not successfully checked   *"  >>stdout
                           else
                                echo "* ----------------   Job Validated  ------------------*" >> stdout
                                error="0"
                           fi
                        else
                            echo "* #             Check Macro failed !                  #" >> stdout
                        fi
                    else
                	echo "######## Validation error: $kl" >> stdout
                    fi
                else
            	    echo "######## Validation error: $fp" >> stdout
                fi
            else
        	echo "######## Validation error: $ab" >> stdout
            fi
        else
    	    echo "######## Validation error: $be" >> stdout
        fi
    else
	echo "######## Validation error: $sf" >> stdout
    fi
else
    echo "######## Validation error: $sv" >> stdout
fi
else
    echo "* ########## Job not validated - no rec.log or sim.log or check.log or ESD.tag.root  ###" >> stdout
    echo "* ########## Removing all ROOT files from the local directory, leaving only the logs ###" >> stdout
    rm -rf *.root
fi

if [ -f "qa.log" ]; then
    for fileToCheck in QAresults.root EventStat_temp.root; do
        if [ ! -f "$fileToCheck" ]; then
            echo "* ######### QA was invoked but it didn't produce $fileToCheck" >> stdout
            error=1
    	fi
    done
fi

if [ "$error" = "1" ]; then
    echo "* ################   Job not validated ################" >> stdout
fi

echo "* ----------------------------------------------------*" >> stdout
echo "*******************************************************" >> stdout
cd -
exit $error
