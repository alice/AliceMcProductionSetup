#!/bin/bash

# set job and simulation variables as :
# ./simrun.sh  --run <x> --event <y> --process <kPythia6/kPhojet/kPythia6ATLAS_Flat/kPythia6D6T> --field <kNoField/k5kG> --energy <900/2360/10000>

function runcommand(){
    echo -e "\n"
    echo -e "\n" >&2

    echo "* $1 : $2"
    echo "* $1 : $2" >&2
    
    time aliroot -b -q -x $2 >>$3 2>&1
    exitcode=$?
    
    expectedCode=${5-0}
    
    if [ "$exitcode" -ne "$expectedCode" ]; then
	    echo "*! $2 failed with exitcode $exitcode, expecting $expectedCode"
	    echo "*! $2 failed with exitcode $exitcode, expecting $expectedCode" >&2
        echo "$2 failed with exitcode $exitcode, expecting $expectedCode" > validation_error.message
	    exit ${4-$exitcode}
    else
	    echo "* $2 finished with the expected exit code ($expectedCode), moving on"
        echo "* $2 finished with the expected exit code ($expectedCode), moving on" >&2
    fi
}

CONFIG_SEED=""
CONFIG_RUN_TYPE=""
CONFIG_FIELD=""
CONFIG_ENERGY=""
CONFIG_BMIN=""
CONFIG_BMAX=""
DC_RUN=""
DC_EVENT=""

RUNMODE=""

mkdir input
mv galice.root ./input/galice.root
mv Kinematics.root ./input/Kinematics.root
ls input



while [ ! -z "$1" ]; do
    option="$1"
    shift
    
    if [ "$option" = "--run" ]; then
	    DC_RUN="$1"
    	shift
    elif [ "$option" = "--event" ]; then
	    DC_EVENT="$1"
	    shift
    elif [ "$option" = "--process" ]; then
	    CONFIG_RUN_TYPE="$1"
	    shift
    elif [ "$option" = "--field" ]; then
	    CONFIG_FIELD="$1"
	    shift
    elif [ "$option" = "--energy" ]; then
	    CONFIG_ENERGY="$1"
	    shift
    elif [ "$option" = "--bmin" ]; then
	    CONFIG_BMIN="$1"
	    shift
    elif [ "$option" = "--bmax" ]; then
	    CONFIG_BMAX="$1"
	    shift
    elif [ "$option" = "--sdd" ]; then
	    RUNMODE="SDD"
    fi
done

CONFIG_SEED=$((DC_RUN*1000+DC_EVENT))

if [ "$CONFIG_SEED" -eq 0 ]; then
    echo "*!  WARNING! Seeding variable for MC is 0 !" >&2
else
    echo "* MC Seed is $CONFIG_SEED ($DC_RUN / $DC_EVENT)"
fi

echo "* b min is $CONFIG_BMIN"
echo "* b max is $CONFIG_BMAX"

export CONFIG_SEED CONFIG_RUN_TYPE CONFIG_FIELD CONFIG_ENERGY CONFIG_BMIN CONFIG_BMAX DC_RUN DC_EVENT

export ALIMDC_RAWDB1="./mdc1"
export ALIMDC_RAWDB2="./mdc2"
export ALIMDC_TAGDB="./mdc1/tag"
export ALIMDC_RUNDB="./mdc1/meta"

echo "SIMRUN:: Run $DC_RUN Event $DC_EVENT Generator $CONFIG_RUN_TYPE Field $CONFIG_FIELD Energy $CONFIG_ENERGY"

runcommand "SIMULATION" "sim.C" sim.log 5
mv syswatch.log simwatch.log

runcommand "RECONSTRUCTION" "rec.C" rec.log 10
mv syswatch.log recwatch.log

runcommand "TAG" "tag.C" tag.log 50

runcommand "CHECK ESD" "CheckESD.C" check.log 60 1

rm -f *.RecPoints.root

if [ "$RUNMODE" = "SDD" ]; then
    if [ -f QAtrainsim_SDD.C ]; then
	    runcommand "Running the QA train" "QAtrainsim_SDD.C(\"_wSDD\",$DC_RUN)" qa.log 100
    
	    for file in *.stat; do
	        mv $file ../$file.qa_wSDD
	    done
    fi
    
    mv AliESDs.root AliESDs_wSDD.root
    mv AliESDfriends.root AliESDfriends_wSDD.root
    
    # Without SDD

    for logfile in rec.log qa.log tag.log check.log recwatch.log; do
	    echo -e "\n\n* ------------ Without SDD ------------" >> $logfile
    done

    runcommand "RECONSTRUCTION without SDD" "recNoSDD.C" rec.log 11
    cat syswatch.log >> recwatch.log
    rm syswatch.log

    runcommand "TAG without SDD" "tag.C" tag.log 51

    runcommand "CHECK ESD without SDD" "CheckESD.C" check.log 61

    if [ -f QAtrainsim_SDD.C ]; then
        runcommand "Running the QA train without SDD" "QAtrainsim_SDD.C(\"\",$DC_RUN)" qa.log 101
    
	    for file in *.stat; do
	        mv $file ../$file.qa
	    done
    fi
fi

rm -f *.RecPoints.root *.Hits.root *.Digits.root *.SDigits.root

exit 0
