
#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TPDGCode.h>
#include <TRandom.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include "STEER/AliRunLoader.h"
#include "STEER/AliRun.h"
#include "STEER/AliConfig.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "EVGEN/AliGenCocktail.h"
#include "EVGEN/AliGenHIJINGpara.h"
#include "STEER/AliMagF.h"
#include "STEER/AliMagFCheb.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "ITS/AliITSv11Hybrid.h"
#include "TPC/AliTPCv2.h"
#include "TOF/AliTOFv6T0.h"
#include "HMPID/AliHMPIDv3.h"
#include "ZDC/AliZDCv3.h"
#include "TRD/AliTRDv1.h"
#include "FMD/AliFMDv1.h"
#include "MUON/AliMUONv1.h"
#include "PHOS/AliPHOSv1.h"
#include "PMD/AliPMDv1.h"
#include "T0/AliT0v1.h"
#include "EMCAL/AliEMCALv2.h"
#include "ACORDE/AliACORDEv1.h"
#include "VZERO/AliVZEROv7.h"
#endif

static Int_t runNumber = 0;
static Int_t alcount = 0;
static UInt_t    seed     = 1;
static Float_t   energy   = 7000.0; // energy in CMS
static Int_t nruns = 161;
static Int_t runlist[161];

void Config(){
  if (gSystem->Getenv("CONFIG_SEED")) seed      = atoi(gSystem->Getenv("CONFIG_SEED"));
  if (gSystem->Getenv("DC_RUN"))      runNumber = atoi(gSystem->Getenv("DC_RUN"));
  if (gSystem->Getenv("DC_EVENT"))    alcount   = atoi(gSystem->Getenv("DC_EVENT"));

  gRandom->SetSeed( seed );
  cout<<"Seed for random number generation= "<<seed<<endl;//cerr
  if (gSystem->Getenv("CONFIG_ENERGY")) energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
  cout<<"Energy set to "<<energy<<" GeV"<<endl;

  Int_t nseg = 23;      //number of segments
  Int_t nevt = 10000;   //events in run
  Int_t njob = 1000;    //events in alien job

  // Libraries required by geant321
#if defined(__CINT__)
  gSystem->Load("liblhapdf");      // Parton density functions
  gSystem->Load("libEGPythia6");   // TGenerator interface
  gSystem->Load("libpythia6");     // Pythia 6.2
  gSystem->Load("libAliPythia6");  // ALICE specific implementations
  gSystem->Load("libgeant321");
#endif

  new TGeant3TGeo("C++ Interface to Geant3");
  cout<<"Config.C: Creating Run Loader ..."<<endl;
  AliRunLoader* rl=AliRunLoader::Open("galice.root",AliConfig::GetDefaultEventFolderName(),"recreate");
  if (rl == 0x0) {
    gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
    return;
  }

  rl->LoadKinematics("RECREATE");
  rl->MakeTree("E");

  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(nevt);
  gAlice->SetRunLoader(rl);

  AliGenReaderTreeK *reader = new AliGenReaderTreeK();
  reader->SetFileName("galice.root");
  reader->AddDir("./input");

  AliGenExtFile *gener = new AliGenExtFile(-1);
  gener->SetVertexSmear(kPerEvent);
  gener->SetEnergyCMS(energy); 
  gener->SetProjectile("p", 1, 1); 
  gener->SetTarget    ("p", 1, 1);
  gener->SetReader(reader);
  gener->Init();

  Int_t ofs, seg, ofs_fin;
  InitRunList();
  for(Int_t irun=0; irun<nruns; irun++) {
    seg = irun/nseg;
    ofs = irun -seg*nseg;
    if(runNumber == runlist[irun]) break;
  }
  ofs_fin = ofs*nevt + (alcount-1)*njob;  // ofs = offset for the given run , nevt = number of events in run
                                      // alcount = #alien_counter# , njob = number of events in alien job
                                      // supposes split 1-10 in JDL and number of events in sim.C set to njob
  cout<<" In Config.C, runNumber: "<<runNumber<<" , offset: "<<ofs_fin<<endl;
  for(Int_t i=0; i<ofs_fin; i++) reader->NextEvent();

  gMC->SetProcess("DCAY",1);
  gMC->SetProcess("PAIR",1);
  gMC->SetProcess("COMP",1);
  gMC->SetProcess("PHOT",1);
  gMC->SetProcess("PFIS",0);
  gMC->SetProcess("DRAY",0);
  gMC->SetProcess("ANNI",1);
  gMC->SetProcess("BREM",1);
  gMC->SetProcess("MUNU",1);
  gMC->SetProcess("CKOV",1);
  gMC->SetProcess("HADR",1);
  gMC->SetProcess("LOSS",2);
  gMC->SetProcess("MULS",1);
  gMC->SetProcess("RAYL",1);

  Float_t cut = 1.e-3;
  Float_t tofmax = 1.e10;

  gMC->SetCut("CUTGAM", cut);
  gMC->SetCut("CUTELE", cut);
  gMC->SetCut("CUTNEU", cut);
  gMC->SetCut("CUTHAD", cut);
  gMC->SetCut("CUTMUO", cut);
  gMC->SetCut("BCUTE",  cut); 
  gMC->SetCut("BCUTM",  cut); 
  gMC->SetCut("DCUTE",  cut); 
  gMC->SetCut("DCUTM",  cut); 
  gMC->SetCut("PPCUTM", cut);
  gMC->SetCut("TOFMAX", tofmax); 
  
  rl->CdGAFile();

    Int_t   iABSO   = 1;//.
    Int_t   iDIPO   = 1;//.
    Int_t   iFMD    = 1;
    Int_t   iFRAME  = 1;//.
    Int_t   iHALL   = 1;//.
    Int_t   iITS    = 1;
    Int_t   iMAG    = 1;//.
    Int_t   iMUON   = 1;//.
    Int_t   iPHOS   = 1;
    Int_t   iPIPE   = 1;//.
    Int_t   iPMD    = 1;
    Int_t   iHMPID  = 1;
    Int_t   iSHIL   = 1;//.
    Int_t   iT0     = 1;
    Int_t   iTOF    = 1;
    Int_t   iTPC    = 1;
    Int_t   iTRD    = 1;
    Int_t   iZDC    = 1;
    Int_t   iEMCAL  = 1;
    Int_t   iACORDE = 1;
    Int_t   iVZERO  = 1;

    //=================== Alice BODY parameters =============================
    AliBODY *BODY = new AliBODY("BODY", "Alice envelop");

    if (iMAG)
    {
        //=================== MAG parameters ============================
        // --- Start with Magnet since detector layouts may be depending ---
        // --- on the selected Magnet dimensions ---
        AliMAG *MAG = new AliMAG("MAG", "Magnet");
    }


    if (iABSO)
    {
        //=================== ABSO parameters ============================
        AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
    }

    if (iDIPO)
    {
        //=================== DIPO parameters ============================

        AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
    }

    if (iHALL)
    {
        //=================== HALL parameters ============================

        AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
    }


    if (iFRAME)
    {
        //=================== FRAME parameters ============================

        AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
	FRAME->SetHoles(1);
    }

    if (iSHIL)
    {
        //=================== SHIL parameters ============================

        AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
    }


    if (iPIPE)
    {
        //=================== PIPE parameters ============================

        AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
    }
 
    if (iITS)
    {
        //=================== ITS parameters ============================

	AliITS *ITS  = new AliITSv11("ITS","ITS v11");
    }

    if (iTPC)
    {
        //============================ TPC parameters ===================
        AliTPC *TPC = new AliTPCv2("TPC", "Default");
    }


    if (iTOF) {
        //=================== TOF parameters ============================
	AliTOF *TOF = new AliTOFv6T0("TOF", "normal TOF");
    }


    if (iHMPID)
    {
        //=================== HMPID parameters ===========================
        AliHMPID *HMPID = new AliHMPIDv3("HMPID", "normal HMPID");

    }


    if (iZDC)
    {
        //=================== ZDC parameters ============================

        AliZDC *ZDC = new AliZDCv3("ZDC", "normal ZDC");
    }

    if (iTRD)
    {
        //=================== TRD parameters ============================

        AliTRD *TRD = new AliTRDv1("TRD", "TRD slow simulator");
    }

    if (iFMD)
    {
        //=================== FMD parameters ============================
	AliFMD *FMD = new AliFMDv1("FMD", "normal FMD");
   }

    if (iMUON)
    {
        //=================== MUON parameters ===========================
        // New MUONv1 version (geometry defined via builders)
        AliMUON *MUON = new AliMUONv1("MUON", "default");
        MUON->SetTriggerEffCells(1);
    }
    //=================== PHOS parameters ===========================

    if (iPHOS)
    {
        AliPHOS *PHOS = new AliPHOSv1("PHOS", "IHEP");
    }


    if (iPMD)
    {
        //=================== PMD parameters ============================
        AliPMD *PMD = new AliPMDv1("PMD", "normal PMD");
    }

    if (iT0)
    {
        //=================== T0 parameters ============================
        AliT0 *T0 = new AliT0v1("T0", "T0 Detector");
    }

    if (iEMCAL)
    {
        //=================== EMCAL parameters ============================
        AliEMCAL *EMCAL = new AliEMCALv2("EMCAL", "EMCAL_COMPLETEV1");//EMCAL_FIRSTYEARV1 EMCAL_COMPLETEV1
    }

     if (iACORDE)
    {
        //=================== ACORDE parameters ============================
        AliACORDE *ACORDE = new AliACORDEv1("ACORDE", "normal ACORDE");
    }

     if (iVZERO)
    {
        //=================== VZERO parameters ============================
        AliVZERO *VZERO = new AliVZEROv7("VZERO", "normal VZERO");
    }

     AliLog::Message(AliLog::kInfo, "End of Config", "Config.C", "Config.C", "Config()"," Config.C", __LINE__);











}//Config

void InitRunList() {

  runlist[0] = 154726;
  runlist[1] = 154732;
  runlist[2] = 154733;
  runlist[3] = 154742;
  runlist[4] = 154745;
  runlist[5] = 154750;
  runlist[6] = 154753;
  runlist[7] = 154789;
  runlist[8] = 154793;
  runlist[9] = 154808;
  runlist[10] = 155135;
  runlist[11] = 155162;
  runlist[12] = 155163;
  runlist[13] = 155164;
  runlist[14] = 155165;
  runlist[15] = 155166;
  runlist[16] = 155167;
  runlist[17] = 155174;
  runlist[18] = 155235;
  runlist[19] = 155237;
  runlist[20] = 155239;
  runlist[21] = 155251;
  runlist[22] = 155277;
  runlist[23] = 155278;
  runlist[24] = 155300;
  runlist[25] = 155302;
  runlist[26] = 155305;
  runlist[27] = 155308;
  runlist[28] = 155314;
  runlist[29] = 155325;
  runlist[30] = 155331;
  runlist[31] = 155333;
  runlist[32] = 155345;
  runlist[33] = 155346;
  runlist[34] = 155347;
  runlist[35] = 155368;
  runlist[36] = 155370;
  runlist[37] = 155371;
  runlist[38] = 155375;
  runlist[39] = 155376;
  runlist[40] = 155382;
  runlist[41] = 156889;
  runlist[42] = 156891;
  runlist[43] = 156896;
  runlist[44] = 157025;
  runlist[45] = 157028;
  runlist[46] = 157079;
  runlist[47] = 157087;
  runlist[48] = 157091;
  runlist[49] = 157092;
  runlist[50] = 157094;
  runlist[51] = 157096;
  runlist[52] = 157098;
  runlist[53] = 157100;
  runlist[54] = 157209;
  runlist[55] = 157210;
  runlist[56] = 157211;
  runlist[57] = 157227;
  runlist[58] = 157257;
  runlist[59] = 157261;
  runlist[60] = 157262;
  runlist[61] = 157275;
  runlist[62] = 157277;
  runlist[63] = 157475;
  runlist[64] = 157560;
  runlist[65] = 157562;
  runlist[66] = 157564;
  runlist[67] = 157569;
  runlist[68] = 157770;
  runlist[69] = 157819;
  runlist[70] = 157848;
  runlist[71] = 157975;
  runlist[72] = 158086;
  runlist[73] = 158111;
  runlist[74] = 158112;
  runlist[75] = 158115;
  runlist[76] = 158118;
  runlist[77] = 158124;
  runlist[78] = 158136;
  runlist[79] = 158137;
  runlist[80] = 158171;
  runlist[81] = 158173;
  runlist[82] = 158175;
  runlist[83] = 158176;
  runlist[84] = 158177;
  runlist[85] = 158179;
  runlist[86] = 158189;
  runlist[87] = 158192;
  runlist[88] = 158194;
  runlist[89] = 158196;
  runlist[90] = 158200;
  runlist[91] = 158201;
  runlist[92] = 158285;
  runlist[93] = 158287;
  runlist[94] = 158288;
  runlist[95] = 158299;
  runlist[96] = 158303;
  runlist[97] = 158304;
  runlist[98] = 158467;
  runlist[99] = 158468;
  runlist[100] = 158471;
  runlist[101] = 158492;
  runlist[102] = 158495;
  runlist[103] = 158496;
  runlist[104] = 158516;
  runlist[105] = 158518;
  runlist[106] = 158520;
  runlist[107] = 158526;
  runlist[108] = 158528;
  runlist[109] = 158533;
  runlist[110] = 158602;
  runlist[111] = 158604;
  runlist[112] = 158611;
  runlist[113] = 158613;
  runlist[114] = 158615;
  runlist[115] = 158617;
  runlist[116] = 158626;
  runlist[117] = 158784;
  runlist[118] = 158790;
  runlist[119] = 158791;
  runlist[120] = 158793;
  runlist[121] = 159254;
  runlist[122] = 159259;
  runlist[123] = 159283;
  runlist[124] = 159285;
  runlist[125] = 159286;
  runlist[126] = 159318;
  runlist[127] = 159532;
  runlist[128] = 159535;
  runlist[129] = 159536;
  runlist[130] = 159538;
  runlist[131] = 159539;
  runlist[132] = 159577;
  runlist[133] = 159580;
  runlist[134] = 159581;
  runlist[135] = 159582;
  runlist[136] = 159593;
  runlist[137] = 159595;
  runlist[138] = 159599;
  runlist[139] = 159602;
  runlist[140] = 159606;
  runlist[141] = 161665;
  runlist[142] = 161688;
  runlist[143] = 161721;
  runlist[144] = 161722;
  runlist[145] = 161723;
  runlist[146] = 161724;
  runlist[147] = 161756;
  runlist[148] = 161822;
  runlist[149] = 161825;
  runlist[150] = 161827;
  runlist[151] = 161937;
  runlist[152] = 162626;
  runlist[153] = 162627;
  runlist[154] = 162628;
  runlist[155] = 162629;
  runlist[156] = 162630;
  runlist[157] = 162633;
  runlist[158] = 162635;
  runlist[159] = 162636;
  runlist[160] = 162642;

}//InitRunList

