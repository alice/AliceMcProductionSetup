void sim(Int_t nev=400) {
  AliSimulation simulator;
  simulator.SetMakeSDigits("TOF TRD PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
  simulator.SetMakeDigitsFromHits("ITS TPC");
  //  simulator.SetRunNumber(117222);
  //simulator.SetSeed(seed);
 //

  // RAW OCDB
  simulator.SetDefaultStorage("alien://Folder=/alice/data/2010/OCDB");
  
// Specific storages = 23 

//
// ITS  (1 Total)
//     Alignment from Ideal OCDB 

  simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

//
// MUON (1 object)

  simulator.SetSpecificStorage("MUON/Align/Data","alien://folder=/alice/simulation/2008/v4-15-Release/Ideal"); 

//
// TPC (6 total) 

simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
simulator.SetSpecificStorage("TPC/Align/Data",           "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");



// TRD: the following entry to cure the following error (appearing with run 117222):
//
// #3  0x000000011181b81a in AliLog::Message (level=<value temporarily unavailable, due to optimizations>, message=0x11151ad75 "Lock is ON: cannot use different run number than the internal one!", module=<value temporarily unavailable, due to optimizations>, className=<value temporarily unavailable, due to optimizations>, function=0x11151ad49 "Get", file=0x11151a2cc "/Users/administrator/soft/alisoft/aliroot/v5-03-Rev-19/src/STEER/CDB/AliCDBManager.cxx", line=<value temporarily unavailable, due to optimizations>) at /Users/administrator/soft/alisoft/aliroot/v5-03-Rev-19/src/STEER/STEERBase/AliLog.cxx:921
// #4  0x000000011145c612 in AliCDBManager::Get (this=0x7f9e4a8aae10, query=
// 0x7fff59cc0570, forceCaching=false) at /Users/administrator/soft/alisoft/aliroot/v5-03-Rev-19/src/STEER/CDB/AliCDBManager.cxx:835
// #5  0x000000011145c434 in AliCDBManager::Get (this=0x7f9e4a8aae10, path=<value temporarily unavailable, due to optimizations>, runNumber=<value temporarily unavailable, due to optimizations>, version=<value temporarily unavailable, due to optimizations>, subVersion=290562764) at /Users/administrator/soft/alisoft/aliroot/v5-03-Rev-19/src/STEER/CDB/AliCDBManager.cxx:804
// #6  0x000000010ec6c706 in AliTRDcalibDB::GetCDBEntry (this=0x7f9e3ec25870, cdbPath=0x10ee56dfa "TRD/Calib/DCS") at /Users/administrator/soft/alisoft/aliroot/v5-03-Rev-19/src/TRD/AliTRDcalibDB.cxx:357
// #7  0x000000010ec6c066 in AliTRDcalibDB::CacheCDBEntry () at /Users/administrator/soft/AliSoft/aliroot/v5-03-Rev-19/src/TRD/AliTRDcalibDB.h:375
// #8  0x000000010ec6c066 in AliTRDcalibDB::GetCachedCDBObject (this=0x7f9e3ec25870, id=<value temporarily unavailable, due to optimizations>) at /Users/administrator/soft/alisoft/aliroot/v5-03-Rev-19/src/TRD/AliTRDcalibDB.cxx:324
// #9  0x000000010ec6d6f8 in AliTRDcalibDB::GetGlobalConfiguration (this=0x7f9e3ec25870, config=
// 0x7fff59cc0830) at /Users/administrator/soft/alisoft/aliroot/v5-03-Rev-19/src/TRD/AliTRDcalibDB.cxx:1065

simulator.SetSpecificStorage("TRD/Calib/DCS","alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

//
// Vertex and magfield


  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();
  // Switch off QA
  simulator.SetRunQA("");

//
// The rest

  TStopwatch timer;
  timer.Start();
  simulator.Run(nev);
  timer.Stop();
  timer.Print();
}
