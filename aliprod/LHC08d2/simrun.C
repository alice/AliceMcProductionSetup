// #define VERBOSEARGS
// simrun.C
{
// set job and simulation variables as :
// root.exe -b -q simrun.C  --run <x> --event <y> --process <kPythia6/kPhojet> --field <kNoField/k5kG> --energy <900/10000>

  int nrun = 0;
  int nevent = 0;
  int seed = 0;

  char sseed[1024];
  char srun[1024];
  char sevent[1024];
  char sprocess[1024];
  char sfield[1024];
  char senergy[1024];

  sprintf(srun,"");
  sprintf(sevent,"");
  sprintf(sprocess,"");
  sprintf(sfield,"");
  sprintf(senergy,"");

  for (int i=0; i< gApplication->Argc();i++){
#ifdef VERBOSEARGS
    printf("Arg  %d:  %s\n",i,gApplication->Argv(i));
#endif
    if (!(strcmp(gApplication->Argv(i),"--run")))
      nrun = atoi(gApplication->Argv(i+1));
    sprintf(srun,"%d",nrun);

    if (!(strcmp(gApplication->Argv(i),"--event")))
      nevent = atoi(gApplication->Argv(i+1));
    sprintf(sevent,"%d",nevent);

    if (!(strcmp(gApplication->Argv(i),"--process")))
      sprintf(sprocess, gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--field")))
      sprintf(sfield,gApplication->Argv(i+1));

    if (!(strcmp(gApplication->Argv(i),"--energy")))
      sprintf(senergy,gApplication->Argv(i+1));

  }

  seed = nrun * 100000 + nevent;
  sprintf(sseed,"%d",seed);

  if (seed==0) {
    fprintf(stderr,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    fprintf(stderr,"!!!!  WARNING! Seeding variable for MC is 0          !!!!\n");
    fprintf(stderr,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  } else {
    fprintf(stdout,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    fprintf(stdout,"!!!  MC Seed is %d \n",seed);
    fprintf(stdout,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  }
  
// set the seed environment variable
  gSystem->Setenv("CONFIG_SEED",sseed);
  gSystem->Setenv("CONFIG_RUN_TYPE",sprocess); // kPythia6 or kPhojet
  gSystem->Setenv("CONFIG_FIELD",sfield);      // kNoField or k5kG
  gSystem->Setenv("CONFIG_ENERGY",senergy);    // 900 or 10000 (GeV)
  gSystem->Setenv("DC_RUN",srun); // Not used in Config.C
  gSystem->Setenv("DC_EVENT",sevent); // Not used in Config.C
  
// Needed to produce simulated RAW data
  gSystem->Setenv("ALIMDC_RAWDB1","./mdc1");
  gSystem->Setenv("ALIMDC_RAWDB2","./mdc2");
  gSystem->Setenv("ALIMDC_TAGDB","./mdc1/tag");
  gSystem->Setenv("ALIMDC_RUNDB","./mdc1/meta");
  cout<< "SIMRUN:: Run " << gSystem->Getenv("DC_RUN") << " Event " << gSystem->Getenv("DC_EVENT")
	  << " Process "    << gSystem->Getenv("DC_RUN_TYPE")
	  << " minpthard " << gSystem->Getenv("PTHARDMIN")
	  << " maxpthard " << gSystem->Getenv("PTHARDMAX")
	  << " minpt "     << gSystem->Getenv("PTGAMMAPI0MIN")
	  << endl;

  cout<<">>>>> SIMULATION <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q sim.C > sim.log 2>&1");
  cout<<">>>>> PREAPARTION FOR the TRD RECO <<<<<"<<endl;
  gSystem->Exec("rm -rf trdrec; mkdir trdrec");  
  gSystem->Exec("cd trdrec; for a in `ls ../*.root`; do ln -s $a .; done");  
  cout<<">>>>> RECONSTRUCTION <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q rec.C > rec.log 2>&1");
  cout<<">>>>> TAG <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q tag.C > tag.log 2>&1");
  cout<<">>>>> CHECK ESD <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q CheckESD.C > check.log 2>&1");
  cout<<">>>>> AOD <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q CreateAODfromESD.C > aod.log 2>&1");
  cout<<">>>>> TRD RECONSTRUCTION <<<<<"<<endl;
  gSystem->Exec("cd trdrec; aliroot -b -q ../trdrec.C > trdrec.log 2>&1");
  gSystem->Exec("mv trdrec/AliESDs.root AliESDsTRD.root");
  gSystem->Exec("mv trdrec/trdrec.log .");
  //      PLANE EFFICIENCY EVALUATION
  gSystem->mkdir("PlaneEff");
  gSystem->cd("PlaneEff");
  cout<<">>>>> SPD EFFICIENCY WITH TRACKLETS <<<<<"<<endl;
  gSystem->Exec("aliroot -b -q ../EvaluateSPDEffWithTracklets.C\\\(\\\"..\\\",0,1,\\\"alien://Folder=/alice/simulation/2008/v4-14-Release/Full\\\"\\\) > Tracklets_bkg.log 2>&1");
  gSystem->Exec("mv AliITSPlaneEffSPDtracklet.root AliITSPlaneEffSPDtracklet_bkg.root");
  gSystem->Exec("aliroot -b -q ../EvaluateSPDEffWithTracklets.C\\\(\\\"..\\\",1,0,\\\"alien://Folder=/alice/simulation/2008/v4-14-Release/Ideal\\\"\\\) > Tracklets_ideal.log 2>&1");
  gSystem->Exec("mv AliITSPlaneEffSPDtracklet.root AliITSPlaneEffSPDtracklet_ideal.root");
  gSystem->Exec("mv TrackletsMCpred.root TrackletsMCpred_ideal.root");
  gSystem->Exec("mv TrackleterSPDHistos.root TrackleterSPDHistos_ideal.root");
  gSystem->Exec("aliroot -b -q ../EvaluateSPDEffWithTracklets.C\\\(\\\"..\\\",1,0,\\\"alien://Folder=/alice/simulation/2008/v4-14-Release/Full\\\"\\\) > Tracklets.log 2>&1");
  gSystem->Exec("mv Tracklet* ..");
  gSystem->Exec("mv AliITSPlaneEff*root ..");
  gSystem->cd("..");
}}
