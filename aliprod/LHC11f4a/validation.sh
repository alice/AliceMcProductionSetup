#!/bin/sh
##################################################
validateout=`dirname $0`
validatetime=`date`
validated="0";
error=1

if [ -z $validateout ]
then
    validateout="."
fi
cd $validateout;
validateworkdir=`pwd`;

echo "*******************************************************" >> stdout;
echo "* AliRoot Validation Script V1.0                      *" >> stdout;
echo "* Time:    $validatetime " >> stdout;
echo "* Dir:     $validateout" >> stdout;
echo "* Workdir: $validateworkdir" >> stdout;
echo "* ----------------------------------------------------*" >> stdout;
ls -la ./ >> stdout;
ls -la Background/ >> stdout;
ls -la Signal/ >> stdout;
echo "* ----------------------------------------------------*" >> stdout;

##################################################
if [ -f sim.log ] 
then 
sv=`grep -i  "Segmentation violation" *.log`
if [ "$sv" = "" ]
    then
    sf=`grep -i  "Segmentation fault" *.log`
    if [ "$sf" = "" ]
        then
        be=`grep -i  "Bus error" *.log`
        if [ "$be" = "" ]
					then
					br=`grep -i "Break" *.log`
					if [ "$br" = "" ]
						then
						fp=`grep -i  "Floating point exception" *.log`
						if [ "$fp" = "" ]
							then
							kl=`grep -i  "Killed" *.log`
							if [ "$kl" = "" ]
								then
								bf=`grep -i "busy flag cleared" *.log`
								if [ "$bf" = "" ]
									then
									ab=`grep -i "Aborted" stderr`
									if [ "$ab" = "" ]
										then
										fd=`grep -i "No such file or directory" stderr`
										if [ "$fd" = "" ]
											then	
											echo "* ----------------   Job Validated  ------------------*" >> stdout;
											error="0";
										else
											echo "* #             Check Macro failed !                  #" >> stdout;
										fi
									fi
								fi
							fi
						fi
					fi
        fi
    fi
fi
else
    echo "* ########## Job not validated - no sim.log ###" >> stdout;
    echo "* ########## Removing all ROOT files from the local directory, leaving only the logs ###" >> stdout;
    rm -rf *.root
fi
if [ "$error" = "1" ] 
    then
    echo "* ################   Job not validated ################" >> stdout;
fi
echo "* ----------------------------------------------------*" >> stdout;
echo "*******************************************************" >> stdout;

sleep 15;
cd -
exit $error

