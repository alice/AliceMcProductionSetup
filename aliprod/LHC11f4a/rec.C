void rec(Bool_t useHLT=kFALSE) {
  AliReconstruction reco;

  reco.SetFractionFriends(1);
  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();

  // Ideal OCDB
  reco.SetDefaultStorage("alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");
  reco.SetSpecificStorage("GRP/GRP/Data", Form("local://%s",gSystem->pwd()));

  // HLT settings
  if (useHLT) {
    reco.SetSpecificStorage("TPC/Calib/RecoParam","alien://folder=/alice/cern.ch/user/t/tkollegg/HLT_verification/sim.pp_jets/OCDB");
    reco.SetSpecificStorage("HLT/ConfigTPC/TPCHWClusterFinder", "alien://folder=/alice/cern.ch/user/t/tkollegg/HLT_verification/sim.pp_jets/OCDB");
    reco.SetSpecificStorage("HLT/ConfigTPC/TPCHWClusterTransform", "alien://folder=/alice/cern.ch/user/t/tkollegg/HLT_verification/sim.pp_jets/OCDB");
    reco.SetSpecificStorage("HLT/ConfigTPC/TPCDataCompressor", "alien://folder=/alice/cern.ch/user/t/tkollegg/HLT_verification/sim.pp_jets/OCDB");
    reco.SetSpecificStorage("HLT/ConfigTPC/TPCDataCompressorHuffmanTables", "alien://folder=/alice/cern.ch/user/t/tkollegg/HLT_verification/sim.pp_jets/OCDB");
  }

  reco.SetRunReconstruction("ITS TPC HLT");
  reco.SetFillESD("ITS TPC");
  reco.SetRunPlaneEff(kTRUE);

  reco.SetRunQA(":") ;
  reco.Run();
}
