
// Configuration for the first physics production 2008
//

// One can use the configuration macro in compiled mode by
// root [0] gSystem->Load("libgeant321");
// root [0] gSystem->SetIncludePath("-I$ROOTSYS/include -I$ALICE_ROOT/include\
//                   -I$ALICE_ROOT -I$ALICE/geant3/TGeant3");
// root [0] .x grun.C(1,"Config.C++")

#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TRandom.h>
#include <TDatime.h>
#include <TSystem.h>
#include <TVirtualMC.h>
#include <TGeant3TGeo.h>
#include "STEER/AliRunLoader.h"
#include "STEER/AliRun.h"
#include "STEER/AliConfig.h"
#include "PYTHIA6/AliDecayerPythia.h"
#include "PYTHIA6/AliGenPythia.h"
#include "TDPMjet/AliGenDPMjet.h"
#include "STEER/AliMagFCheb.h"
#include "STRUCT/AliBODY.h"
#include "STRUCT/AliMAG.h"
#include "STRUCT/AliABSOv3.h"
#include "STRUCT/AliDIPOv3.h"
#include "STRUCT/AliHALLv3.h"
#include "STRUCT/AliFRAMEv2.h"
#include "STRUCT/AliSHILv3.h"
#include "STRUCT/AliPIPEv3.h"
#include "ITS/AliITSv11.h"
#include "TPC/AliTPCv2.h"
#include "TOF/AliTOFv6T0.h"
#include "HMPID/AliHMPIDv3.h"
#include "ZDC/AliZDCv3.h"
#include "TRD/AliTRDv1.h"
#include "TRD/AliTRDgeometry.h"
#include "FMD/AliFMDv1.h"
#include "MUON/AliMUONv1.h"
#include "PHOS/AliPHOSv1.h"
#include "PHOS/AliPHOSSimParam.h"
#include "PMD/AliPMDv1.h"
#include "T0/AliT0v1.h"
#include "EMCAL/AliEMCALv2.h"
#include "ACORDE/AliACORDEv1.h"
#include "VZERO/AliVZEROv7.h"
#endif


enum PDC06Proc_t  // renaming?
{
  kPyJetJet,        kPyJetJetProQ20,  kPyJetJetPerugia0,  kPyJetJetPerugia2010, kPyJetJetPerugia2011, 
  kPyJetJetParton,  kPyGammaJetEMCAL, kPyGammaJetpPb,     kPyJetJetEMCAL,       kPyJetJetGammaDecayEMCAL, 
  kPyGammaJetPHOS,  kPyJetJetPHOS,    kPyJetJetpPb,       kRunMax
};

const char * pprRunName[] = {
  "kPyJetJet",       "kPyJetJetProQ20",  "kPyJetJetPerugia0", "kPyJetJetPerugia2010",  "kPyJetJetPerugia2011", 
  "kPyJetJetParton", "kPyGammaJetEMCAL", "kPyGammaJetpPb",    "kPyJetJetEMCAL",        "kPyJetJetGammaDecayEMCAL", 
  "kPyGammaJetPHOS", "kPyJetJetPHOS",    "kPyJetJetpPb"
};

enum Mag_t
{
  kNoField, k5kG, kFieldMax
};

const char * pprField[] = {
  "kNoField", "k5kG"
};

// Geterator, field, beam energy
static PDC06Proc_t   proc      = kPyJetJetGammaDecayEMCAL;
static Mag_t         mag       = k5kG; // not used in this production, taken from CDB (see sim.C)
static Float_t       energy    = 2760; // energy in CMS
static Int_t         runNumber = 197669;

static Float_t       ptHardMin  = 10.;
static Float_t	     ptHardMax  = 20.;
static Int_t         iquenching = 0  ;
static Float_t       qhat       = 0;

// Calorimeter "trigger"
static Bool_t        pi0FragPhotonInDet = kFALSE ;
static Float_t       ptGammaPi0Min      = 3.5;

//========================//
// Set Random Number seed //
//========================//
TDatime dt;
static UInt_t seed    = dt.Get();

// Comment line
static TString comment;

//--- Functions ---
class AliGenPythia;
/*
AliGenerator *MbPythia();
AliGenerator *MbPythiaTuneD6T();
AliGenerator *MbPhojet();
*/
void ProcessEnvironmentVars();

AliGenPythia *Blubb(Int_t proc){
  //*******************************************************************//
  // Configuration file for hard QCD processes generation with PYTHIA  //
  //                                                                   //
  //*******************************************************************//
  AliGenPythia * gener = 0x0;

  switch(proc) {
      
  case kPyJetJet:
    comment = comment.Append(Form("pp->jet + jet at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
    gener = new AliGenPythia(-1);
    gener->SetEnergyCMS(energy);//        Centre of mass energy
    gener->SetProcess(kPyJets);//        Process type
    gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ5L);
    break;
  case kPyJetJetpPb:
    comment = comment.Append(Form("pPb->jet + jet at %3.0f GeV, pt hard %3.0f - %3.0f Perugia 2011 no nPDFs",energy,ptHardMin,ptHardMax));
    gener = new AliGenPythia(1);

    gener->SetVertexSmear(kPerEvent);
    gener->SetEnergyCMS(energy);
    gener->SetProcess(kPyJets);
    gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
    gener->SetJetPhiRange(0., 360.);
    gener->SetJetEtRange(10., 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ5L);
    gener->UseNewMultipleInteractionsScenario(); // for all Pythia versions >= 6.3
    
    //    Tune
    //    350     Perugia 2011
    gener->SetTune(350);
    gener->SetTarget("A", 208, 82);
    gener->SetProjectile("P", 1, 1);
    
    break;
  case kPyJetJetProQ20:
      comment = comment.Append(Form("pp->jet + jet at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      AliGenPythia * gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);//        Centre of mass energy
      gener->SetProcess(kPyJets);//        Process type
      gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
      gener->SetJetPhiRange(0., 360.);
      gener->SetJetEtRange(10., 1000.);
      gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
      gener->SetStrucFunc(kCTEQ5L);
      //    Tune
      //    129     Pro-Q20
      gener->SetTune(129); 
      break;
    case kPyJetJetPerugia0:
      comment = comment.Append(Form("pp->jet + jet at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      AliGenPythia * gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);//        Centre of mass energy
      gener->SetProcess(kPyJets);//        Process type
      gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
      gener->SetJetPhiRange(0., 360.);
      gener->SetJetEtRange(10., 1000.);
      gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
      gener->SetStrucFunc(kCTEQ5L);
      //    Tune
      //    320     Perugia-0
      gener->SetTune(320);
      break;


    case kPyJetJetPerugia2010:
      comment = comment.Append(Form("pp->jet + jet at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      AliGenPythia * gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);//        Centre of mass energy
      gener->SetProcess(kPyJets);//        Process type
      gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
      gener->SetJetPhiRange(0., 360.);
      gener->SetJetEtRange(10., 1000.);
      gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
      gener->SetStrucFunc(kCTEQ5L);
      //    Tune
      //    327     Perugia 2010
      gener->SetTune(327); 
      break;

    case kPyJetJetPerugia2011:
      comment = comment.Append(Form("pp->jet + jet at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      AliGenPythia * gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);//        Centre of mass energy
      gener->SetProcess(kPyJets);//        Process type
      gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
      gener->SetJetPhiRange(0., 360.);
      gener->SetJetEtRange(10., 1000.);
      gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
      gener->SetStrucFunc(kCTEQ5L);
      //    Tune
      //    350     Perugia 2011
      gener->SetTune(350);
      break;


    case kPyJetJetParton:
      comment = comment.Append(Form("pp->jet + jet at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      AliGenPythia * gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);//        Centre of mass energy
      gener->SetProcess(kPyJets);//        Process type
      gener->SetJetEtaRange(-1.5, 1.5);//  Final state kinematic cuts
      gener->SetJetPhiRange(0., 360.);
      gener->SetJetEtRange(10., 1000.);
      gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
      gener->SetStrucFunc(kCTEQ5L);
      gener->SetMIS(0);
      break;

    case kPyGammaJetEMCAL:
      comment = comment.Append(Form("pp->jet + gamma EMCAL at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);
      gener->SetProcess(kPyDirectGamma);
      //gener->SetStrucFunc(kCTEQ6l);
      gener->SetStrucFunc(kCTEQ5L);
      gener->SetPtHard(ptHardMin,ptHardMax);
      //gener->SetYHard(-1.0,1.0);
      gener->SetGammaEtaRange(-0.701,0.701);
      gener->SetGammaPhiRange(79.,181.); // Over 5 supermodules +-2 deg
      break;

  case kPyGammaJetpPb:
    comment = comment.Append(Form("pPb->gamma + jet at %3.0f GeV, pt hard %3.0f - %3.0f Perugia 2011 no nPDFs",energy,ptHardMin,ptHardMax));
    gener = new AliGenPythia(1);

    gener->SetVertexSmear(kPerEvent);
    gener->SetEnergyCMS(energy);
    gener->SetProcess(kPyDirectGamma);
    gener->SetGammaEtaRange(-0.701, 0.701);//  Final state kinematic cuts
    gener->SetGammaPhiRange(79., 181.);
    gener->SetJetEtRange(0.1, 1000.);
    gener->SetPtHard(ptHardMin, ptHardMax);// Pt transfer of the hard scattering
    gener->SetStrucFunc(kCTEQ5L);
    gener->UseNewMultipleInteractionsScenario(); // for all Pythia versions >= 6.3
    
    //    Tune
    //    350     Perugia 2011
    gener->SetTune(350);
    gener->SetTarget("A", 208, 82);
    gener->SetProjectile("P", 1, 1);
    
    break;

    case kPyJetJetEMCAL:
      comment = comment.Append(Form("pp->jet + jet over EMCAL at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);
      gener->SetProcess(kPyJets);
      //gener->SetStrucFunc(kCTEQ6L);
      gener->SetStrucFunc(kCTEQ5L);
      gener->SetPtHard(ptHardMin,ptHardMax);
      gener->SetJetEtaRange(-1,1);     // Larger than EMCAL to not bias too much
      gener->SetJetPhiRange(60.,210.); // Larger than EMCAL to not bias too much
      gener->SetJetEtRange(10., 1000.);
      gener->SetPi0InEMCAL(pi0FragPhotonInDet);
      gener->SetFragPhotonOrPi0MinPt(ptGammaPi0Min);  // Careful with pT hard limits if pi0FragPhotonInDet option is on
      break;
      
    case kPyJetJetGammaDecayEMCAL:
      comment = comment.Append(Form("pp->jet + jet over EMCAL at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      AliGenPythia * gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);
      gener->SetProcess(kPyJets);
      //gener->SetStrucFunc(kCTEQ6L);
      gener->SetStrucFunc(kCTEQ5L);
      gener->SetPtHard(ptHardMin,ptHardMax);
      gener->SetJetEtaRange(-2,2);     // Larger than EMCAL to not bias too much
      gener->SetJetPhiRange(0.,360.); // Larger than EMCAL to not bias too much
      gener->SetJetEtRange(5., 1000.);
      gener->SetTriggerParticleMinPt(ptGammaPi0Min);  // Careful with pT hard limits if triggerParticleInCalo option is on
      gener->SetEMCALAcceptance(80,180,0.7);
      gener->SetDecayPhotonInEMCAL(kTRUE);
      comment.Append(Form("Trigger on decay photons with pT > %2.2f",ptGammaPi0Min));
      break;
      
    case kPyGammaJetPHOS:
      comment = comment.Append(Form("pp->jet + gamma PHOS at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);
      gener->SetProcess(kPyDirectGamma);
      //gener->SetStrucFunc(kCTEQ6l);
      gener->SetStrucFunc(kCTEQ5L);
      gener->SetPtHard(ptHardMin,ptHardMax);
      //gener->SetYHard(-1.0,1.0);
      gener->SetGammaEtaRange(-0.13,0.13);
      gener->SetGammaPhiRange(259.,321.); // Over 3 modules +-2 deg
      break;      

    case kPyJetJetPHOS:
      comment = comment.Append(Form("pp->jet + jet over PHOS at %3.0f GeV, pt hard %3.0f - %3.0f",energy,ptHardMin,ptHardMax));
      gener = new AliGenPythia(-1);
      gener->SetEnergyCMS(energy);
      gener->SetProcess(kPyJets);
      //gener->SetStrucFunc(kCTEQ6L);
      gener->SetStrucFunc(kCTEQ5L);
      gener->SetPtHard(ptHardMin,ptHardMax);
      gener->SetJetEtaRange(-0.5,0.5);     // Larger than PHOS to not bias too much
      gener->SetJetPhiRange(220.,340.);    // Larger than PHOS to not bias too much
      gener->SetJetEtRange(10., 1000.);
      gener->SetPi0InEMCAL(pi0FragPhotonInDet);
      gener->SetFragPhotonOrPi0MinPt(ptGammaPi0Min);   // Careful with pT hard limits if pi0FragPhotonInDet option is on
      break;

  }

  return gener;

}


void Config()
{


   // Get settings from environment variables
   ProcessEnvironmentVars();

   gRandom->SetSeed(seed);
   cerr<<"Seed for random number generation= "<<seed<<endl; 

   // Libraries required by geant321
#if defined(__CINT__)
   gSystem->Load("liblhapdf");      // Parton density functions
   gSystem->Load("libEGPythia6");   // TGenerator interface
   if (proc == kPyJetJetPerugia2011 || proc == kPyJetJetPerugia0 || proc == kPyJetJetPerugia2010 || proc == kPyJetJetpPb || proc == kPyGammaJetpPb) {
     gSystem->Load("libpythia6.4.25");        // Pythia 6.4.24
   } else {
     gSystem->Load("libpythia6.4.21");   // Pythia 6.4.21
   }
   gSystem->Load("libAliPythia6");  // ALICE specific implementations
   gSystem->Load("libgeant321");
#endif

   new TGeant3TGeo("C++ Interface to Geant3");

   //=======================================================================
   //  Create the output file


   AliRunLoader* rl=0x0;

   cout<<"Config.C: Creating Run Loader ..."<<endl;
   rl = AliRunLoader::Open("galice.root",
         AliConfig::GetDefaultEventFolderName(),
         "recreate");
   if (rl == 0x0)
   {
      gAlice->Fatal("Config.C","Can not instatiate the Run Loader");
      return;
   }
   rl->SetCompressionLevel(2);
   rl->SetNumberOfEventsPerFile(1000);
   gAlice->SetRunLoader(rl);
   // gAlice->SetGeometryFromFile("geometry.root");
   // gAlice->SetGeometryFromCDB();

   // Set the trigger configuration: proton-proton
   //gAlice->SetTriggerDescriptor("p-p");
   AliSimulation::Instance()->SetTriggerConfig("p-p");

   //
   //=======================================================================
   // ************* STEERING parameters FOR ALICE SIMULATION **************
   // --- Specify event type to be tracked through the ALICE setup
   // --- All positions are in cm, angles in degrees, and P and E in GeV


   gMC->SetProcess("DCAY",1);
   gMC->SetProcess("PAIR",1);
   gMC->SetProcess("COMP",1);
   gMC->SetProcess("PHOT",1);
   gMC->SetProcess("PFIS",0);
   gMC->SetProcess("DRAY",0);
   gMC->SetProcess("ANNI",1);
   gMC->SetProcess("BREM",1);
   gMC->SetProcess("MUNU",1);
   gMC->SetProcess("CKOV",1);
   gMC->SetProcess("HADR",1);
   gMC->SetProcess("LOSS",2);
   gMC->SetProcess("MULS",1);
   gMC->SetProcess("RAYL",1);

   Float_t cut = 1.e-3;        // 1MeV cut by default
   Float_t tofmax = 1.e10;

   gMC->SetCut("CUTGAM", cut);
   gMC->SetCut("CUTELE", cut);
   gMC->SetCut("CUTNEU", cut);
   gMC->SetCut("CUTHAD", cut);
   gMC->SetCut("CUTMUO", cut);
   gMC->SetCut("BCUTE",  cut); 
   gMC->SetCut("BCUTM",  cut); 
   gMC->SetCut("DCUTE",  cut); 
   gMC->SetCut("DCUTM",  cut); 
   gMC->SetCut("PPCUTM", cut);
   gMC->SetCut("TOFMAX", tofmax); 




   //======================//
   // Set External decayer //
   //======================//
   TVirtualMCDecayer* decayer = new AliDecayerPythia();
   decayer->SetForceDecay(kAll);
   decayer->Init();
   gMC->SetExternalDecayer(decayer);

   //=========================//
   // Generator Configuration //
   //=========================//
   AliGenPythia *gener = Blubb((Int_t) proc );

   //gener->SetPycellParameters(2., 274, 432, 0., 4., 5., 1.0);
   // expand the pycell grid to eta max 2.5 since jet eta max is 1.5
   //gener->SetPycellParameters(2.5, 343, 432, 0., 4., 5., 1.0);
   // reduce Et seed, in order to minimize bias at track spectrum
   gener->SetPycellParameters(2.5, 343, 432, 0., 0.15, 5., 1.0);
   gener->SetPtKick(5.); // set the intrinsic kt to 5 GeV/c
   gener->SetGluonRadiation(1,1);

   //
   //
   // Size of the interaction diamond
   // Longitudinal
   Float_t sigmaz  = 5.4 / TMath::Sqrt(2.); // [cm]
   if (energy == 900)
      //sigmaz  = 10.5 / TMath::Sqrt(2.); // [cm]
      //sigmaz = 3.7;
      if (energy == 7000 || energy == 2760)
         sigmaz  = 6.3 / TMath::Sqrt(2.); // [cm]

   //
   // Transverse
   // beta*
   Float_t betast  = 2.;                 // beta* [m]
   printf("beta* for run# %8d is %13.3f", runNumber, betast);
   //
   Float_t eps     = 3.75e-6;            // emittance [m]
   Float_t gamma   = energy / 2.0 / 0.938272;  // relativistic gamma [1]
   Float_t sigmaxy = TMath::Sqrt(eps * betast / gamma) / TMath::Sqrt(2.) * 100.;  // [cm]
   printf("\n \n Diamond size x-y: %10.3e z: %10.3e\n \n", sigmaxy, sigmaz);

   gener->SetSigma(sigmaxy, sigmaxy, sigmaz);      // Sigma in (X,Y,Z) (cm) on IP position
   gener->SetVertexSmear(kPerEvent);

   //Quenching
   gener->SetQuench(iquenching);
   if(iquenching == 1){
      Float_t k = 6e5*(qhat/1.7);  //qhat=1.7, k=6e5, default value
      AliPythia::Instance()->InitQuenching(0.,0.1,k,0,0.95,6);
   }
   if(iquenching == 2){
      //(T0,tau0,Nf,Eloss,angledist)
      //Eloss=0=>coll+rad 
      //angledist=1=>wide, dN^g/dtheta~1/theta 
      gener->SetPyquenPar(1.,0.1,0,0,1);
   }

   gener->Init();

   printf("\n \n Comment: %s \n \n", comment.Data());

   rl->CdGAFile();

   Int_t iABSO  = 1; //1
   Int_t iACORDE= 0;
   Int_t iDIPO  = 1;//1
   Int_t iEMCAL = 1;
   Int_t iFMD   = 1;//1
   Int_t iFRAME = 1;//1
   Int_t iHALL  = 1;//1
   Int_t iITS   = 1;//1
   Int_t iMAG   = 1;//1
   Int_t iMUON  = 1;//1
   Int_t iPHOS  = 1;//1
   Int_t iPIPE  = 1;//1
   Int_t iPMD   = 1;//1
   Int_t iHMPID = 1;//1
   Int_t iSHIL  = 1;//1
   Int_t iT0    = 1;//1
   Int_t iTOF   = 1;//1
   Int_t iTPC   = 1;//1
   Int_t iTRD   = 1;//1
   Int_t iVZERO = 1;//1
   Int_t iZDC   = 1;//1


   //=================== Alice BODY parameters =============================
   AliBODY *BODY = new AliBODY("BODY", "Alice envelop");


   if (iMAG)
   {
      //=================== MAG parameters ============================
      // --- Start with Magnet since detector layouts may be depending ---
      // --- on the selected Magnet dimensions ---
      AliMAG *MAG = new AliMAG("MAG", "Magnet");
   }


   if (iABSO)
   {
      //=================== ABSO parameters ============================
      AliABSO *ABSO = new AliABSOv3("ABSO", "Muon Absorber");
   }

   if (iDIPO)
   {
      //=================== DIPO parameters ============================

      AliDIPO *DIPO = new AliDIPOv3("DIPO", "Dipole version 3");
   }

   if (iHALL)
   {
      //=================== HALL parameters ============================

      AliHALL *HALL = new AliHALLv3("HALL", "Alice Hall");
   }


   if (iFRAME)
   {
      //=================== FRAME parameters ============================

      AliFRAMEv2 *FRAME = new AliFRAMEv2("FRAME", "Space Frame");
      FRAME->SetHoles(1);
   }

   if (iSHIL)
   {
      //=================== SHIL parameters ============================

      AliSHIL *SHIL = new AliSHILv3("SHIL", "Shielding Version 3");
   }


   if (iPIPE)
   {
      //=================== PIPE parameters ============================

      AliPIPE *PIPE = new AliPIPEv3("PIPE", "Beam Pipe");
   }

   if (iITS)
   {
      //=================== ITS parameters ============================

      AliITS *ITS  = new AliITSv11("ITS","ITS v11");
   }

   if (iTPC)
   {
      //============================ TPC parameters =====================

      AliTPC *TPC = new AliTPCv2("TPC", "Default");
   }


   if (iTOF) {
      //=================== TOF parameters ============================

      AliTOF *TOF = new AliTOFv6T0("TOF", "normal TOF");
   }


   if (iHMPID)
   {
      //=================== HMPID parameters ===========================

      AliHMPID *HMPID = new AliHMPIDv3("HMPID", "normal HMPID");

   }


   if (iZDC)
   {
      //=================== ZDC parameters ============================

      AliZDC *ZDC = new AliZDCv3("ZDC", "normal ZDC");
   }

   if (iTRD)
   {
      //=================== TRD parameters ============================

      AliTRD *TRD = new AliTRDv1("TRD", "TRD slow simulator");
      AliTRDgeometry *geoTRD = TRD->GetGeometry();
      // Partial geometry: modules at 0,1,7,8,9,10,11,15,16,17
      // starting at 3h in positive direction
      geoTRD->SetSMstatus(2,0);
      geoTRD->SetSMstatus(3,0);
      geoTRD->SetSMstatus(4,0);
      geoTRD->SetSMstatus(5,0);
      geoTRD->SetSMstatus(6,0);
      geoTRD->SetSMstatus(12,0);
      geoTRD->SetSMstatus(13,0);
      geoTRD->SetSMstatus(14,0);
   }

   if (iFMD)
   {
      //=================== FMD parameters ============================

      AliFMD *FMD = new AliFMDv1("FMD", "normal FMD");
   }

   if (iMUON)
   {
      //=================== MUON parameters ===========================
      // New MUONv1 version (geometry defined via builders)

      AliMUON *MUON = new AliMUONv1("MUON", "default");
   }

   if (iPHOS)
   {
      //=================== PHOS parameters ===========================

      AliPHOS *PHOS = new AliPHOSv1("PHOS", "noCPV_Modules123");

   }

   if (iPMD)
   {
      //=================== PMD parameters ============================

      AliPMD *PMD = new AliPMDv1("PMD", "normal PMD");
   }

   if (iT0)
   {
      //=================== T0 parameters ============================
      AliT0 *T0 = new AliT0v1("T0", "T0 Detector");
   }

   if (iEMCAL)
   {
      //=================== EMCAL parameters ============================

      AliEMCAL *EMCAL = new AliEMCALv2("EMCAL", "EMCAL_COMPLETE12SMV1");
   }

   if (iACORDE)
   {
      //=================== ACORDE parameters ============================

      AliACORDE *ACORDE = new AliACORDEv1("ACORDE", "normal ACORDE");
   }

   if (iVZERO)
   {
      //=================== ACORDE parameters ============================

      AliVZERO *VZERO = new AliVZEROv7("VZERO", "normal VZERO");
   }
}


void ProcessEnvironmentVars()
{
   // Run type
   if (gSystem->Getenv("CONFIG_RUN_TYPE")) {
      for (Int_t iRun = 0; iRun < kRunMax; iRun++) {
         if (strcmp(gSystem->Getenv("CONFIG_RUN_TYPE"), pprRunName[iRun])==0) {
            proc = (PDC06Proc_t)iRun;
            cout<<"Run type set to "<<pprRunName[iRun]<<endl;
         }
      }
   }

   // Field
   if (gSystem->Getenv("CONFIG_FIELD")) {
      for (Int_t iField = 0; iField < kFieldMax; iField++) {
         if (strcmp(gSystem->Getenv("CONFIG_FIELD"), pprField[iField])==0) {
            mag = (Mag_t)iField;
            cout<<"Field set to "<<pprField[iField]<<endl;
         }
      }
   }

   // Energy
   if (gSystem->Getenv("CONFIG_ENERGY")) {
      energy = atoi(gSystem->Getenv("CONFIG_ENERGY"));
      cout<<"Energy set to "<<energy<<" GeV"<<endl;
   }

   // Random Number seed
   if (gSystem->Getenv("CONFIG_SEED")) {
      seed = atoi(gSystem->Getenv("CONFIG_SEED"));
   }

   // Run number
   if (gSystem->Getenv("DC_RUN")) {
      runNumber = atoi(gSystem->Getenv("DC_RUN"));
   }

   // hard probes
   if (gSystem->Getenv("CONFIG_PTHARDMIN"))
      ptHardMin = atof(gSystem->Getenv("CONFIG_PTHARDMIN"));
   if (gSystem->Getenv("CONFIG_PTHARDMAX"))
      ptHardMax = atof(gSystem->Getenv("CONFIG_PTHARDMAX"));
   if (gSystem->Getenv("CONFIG_QUENCHING"))
      iquenching = atof(gSystem->Getenv("CONFIG_QUENCHING"));
   if (gSystem->Getenv("QHAT"))
      qhat = atof(gSystem->Getenv("QHAT"));

   // Calorimeter "trigger"
   if ( gSystem->Getenv("PI0GAMMAINDET") )
      pi0FragPhotonInDet	= atoi(gSystem->Getenv("PI0GAMMAINDET"));
   if (gSystem->Getenv("PTGAMMAPI0MIN"))
      ptGammaPi0Min       = atof(gSystem->Getenv("PTGAMMAPI0MIN"));

   cout<<">> Run type set to "<<pprRunName[proc]<<endl;
   cout<<">> Collision energy set to "<<energy <<endl;
   cout<<">> ptHard limits: "<<ptHardMin<<" to " <<ptHardMax<<" GeV"<<endl;
   cout<<">> quenching on? "<< iquenching<<" qhat "<<qhat<<endl;
   if(pi0FragPhotonInDet) cout<< "Force photon or gamma in calorimeter with  pT > "<< ptGammaPi0Min << endl;

   cout << "######################################" << endl;
   cout << "######################################" << endl;

}

