#if !defined(__CINT__) || defined(__MAKECINT__)

#include <Riostream.h>
#include "TFile.h"
#include "TTree.h"

#include "AliAODEvent.h"
#include "AliAODHeader.h"
#include "AliAODVertex.h"
#include "AliAODTrack.h"
#include "AliAODCluster.h"

#endif

void ReadAOD(const char *fileName = "AliAOD.root") {

  gROOT->LoadMacro("$ALICE_PHYSICS/PWGHF/vertexingHF/macros/LoadLibraries.C");
  LoadLibraries();

  // open input file and get the TTree
  TFile inFile(fileName, "READ");
  if (!inFile.IsOpen()) return;

  TTree *aodTree = (TTree*)inFile.Get("aodTree");
  aodTree->AddFriend("aodTree", "./AliAOD.VertexingHF.root");
  AliAODEvent *ev = new AliAODEvent();
  ev->ReadFromTree(aodTree);

  Int_t pdgCand = 4122;
  Int_t pdgDgLctoV0bachelor[2] = {2212, 310};
  Int_t pdgDgV0toDaughters[2] = {211, 211};

  TH1F* hout = new TH1F("AODLcCandidates", "Lc in AOD Resonant flag", 5, -0.5, 4.5);
	TH1F *hpart = new TH1F("hpart","pdg track", 6001,-1.5,5999.5);
	TH1F *hmother = new TH1F("hmother","pdg mother particles", 6001,-1.5,5999.5);
	TH1F *hgmother = new TH1F("hgmother","pdg gmother particles", 6001,-1.5,5999.5);

  // loop over events
  Int_t nEvents = aodTree->GetEntries();
	for (Int_t nEv = 0; nEv < nEvents; nEv++) {
		Printf("\n");
		cout << "Event: " << nEv+1 << "/" << nEvents << endl;

		// read events
		aodTree->GetEvent(nEv);

		//print event info
		ev->GetHeader()->Print();

		// loop over tracks
		Int_t nTracks = ev->GetNumberOfTracks();

		Printf("Number of AOD tracks = %d", nTracks);

		TClonesArray* clArr = (TClonesArray*)ev->GetList()->FindObject("Charm3Prong");
		clArr->Print();
		Printf("clArr = %p, with %d entries", clArr, clArr->GetEntries());

		TClonesArray* clArrD0 = (TClonesArray*)ev->GetList()->FindObject("D0toKpi");
		Printf("clArrD0 = %p, with %d entries", clArrD0, clArrD0->GetEntries());

		TClonesArray *mcArray = dynamic_cast<TClonesArray*>(ev->FindListObject(AliAODMCParticle::StdBranchName()));

		for (Int_t iarray = 0; iarray < clArr->GetEntries(); iarray++){
			Printf("--- candidate %i of %i ---",iarray,clArr->GetEntries());
			AliAODRecoDecayHF3Prong *reco = dynamic_cast<AliAODRecoDecayHF3Prong*>(clArr->At(iarray));
			//    AliAODRecoCascadeHF* cascade = dynamic_cast<AliAODRecoCascadeHF*>(clArr->At(iarray));
			if (!reco) {
				Printf("reco %d not accessible, continuing to the next", iarray);
				continue;
			}
			//Get MC particle
			Int_t isKstar = 0;
			Int_t isDelta = 0;
			Int_t isNonRes = 0;
			Int_t isLambda = 0;
			Int_t isMotherLc = 0;

			for(Int_t i=0;i<3;i++){
				Int_t motherPdg = 0;
				AliAODTrack *daugh=(AliAODTrack*)reco->GetDaughter(i);
				Int_t lab=daugh->GetLabel();
				if(lab<0) {Printf("lab<0");
					break;
				}
				AliAODMCParticle *part= (AliAODMCParticle*)mcArray->At(lab);
				if(!part){
				 Printf("No part at lab");
			 	 continue;
				}
				Int_t partPdgcode = TMath::Abs(part->GetPdgCode());
				hpart->Fill(Double_t(partPdgcode));
				if(partPdgcode==211 || partPdgcode==321 || partPdgcode==2212){
					Printf("proton, kaon or pion - check mother");
					Int_t motherLabel=part->GetMother();
					if(motherLabel<0) {
						Printf("motherLab<0");
						break;
					}
					AliAODMCParticle *motherPart = (AliAODMCParticle*)mcArray->At(motherLabel);
					if(!motherPart) {
						Printf("No mother part at motherLabel = %i",motherLabel);
						continue;
					}
					motherPdg = TMath::Abs(motherPart->GetPdgCode());
					hmother->Fill(Double_t(motherPdg));
					if(motherPdg==4122) {
						Printf("---- Mother is Lc ----");
						isMotherLc++;
						continue;
					}
					else if(motherPdg==313 || motherPdg==2224 || motherPdg==3124){
						Printf("mother is resonant - Check grandmother");
						Int_t granMotherLabel=motherPart->GetMother();
						if(granMotherLabel<0){
							Printf("gmotherLab<0");
							break;
						}
						AliAODMCParticle *granMotherPart = (AliAODMCParticle*)mcArray->At(granMotherLabel);
						if(!granMotherPart) continue;
						Int_t granMotherPdg  = TMath::Abs(granMotherPart->GetPdgCode());
						hgmother->Fill(Double_t(granMotherPdg));
						if(motherPdg==313) isKstar++;
						if(motherPdg==2224) isDelta++;
						if(motherPdg==3123) isLambda++;
						if(granMotherPdg ==4122) {
							Printf("---- Grandmother is Lc ----");
							isMotherLc++;
							continue;
						}
					}
					else {
						Printf("Not pKpi from Lc");
						break;
					}
				}
				else {
					Printf("Not pKpi");
					break;
				}
				Printf("pdg1 %i pdg2 %i pdg3 %i ",partPdgcode,motherPdg,granMotherPdg);
			}
			if(isMotherLc==3) Printf("3 prong IS Lc!");
			else if(isMotherLc==2) Printf("Not Lc, though 2 prongs from Lc");
			else if(isMotherLc==1) Printf("Not Lc, though 1 prong from Lc");
			else if (isMotherLc==0) Printf("Not Lc");
			hout->Fill(isMotherLc);
																	
			Printf("check Daughters");

/*			Int_t isLcResonant = LambdacDaugh(reco,mcArray);

			if (isLcResonant > 0){
				Printf("reconstructed Lc - isLcResonant for reco %d = %d", iarray, isLcResonant);
				hout->Fill(isLcResonant);
			}
			else {
				hout->Fill(0);
			}
			*/
		}
		/*
		for (Int_t nTr = 0; nTr < nTracks; nTr++) {

			AliAODTrack *tr = ev->GetTrack(nTr);

			// print track info
			//cout << nTr+1 << "/" << nTracks << ": track pt: " << tr->Pt();
			if (tr->GetProdVertex()) {
				//cout << ", vertex z of this track: " << tr->GetProdVertex()->GetZ();
			}
			//cout << endl;
		}
		*/

		// loop over vertices
		Int_t nVtxs = ev->GetNumberOfVertices();
		for (Int_t nVtx = 0; nVtx < nVtxs; nVtx++) {

			// print track info
			//cout << nVtx+1 << "/" << nVtxs << ": vertex z position: " << ev->GetVertex(nVtx)->GetZ() << endl;
		}
	}

	TFile* fout = new TFile("aodStat.root", "RECREATE");
	hout->Write();
	hpart->Write();
	hmother->Write();
	hgmother->Write();
	fout->Close();
	return;
}

Int_t LambdacDaugh(AliAODMCParticle *part, TClonesArray *arrayMC) {
	
	// 
	// return value of Lc resonant channel, from AOD MC particle
	// 0=not Lc, 1=non resonant Lc, 2=via L(1520) + pi, 3=via K* + p, 4=via Delta++ + K  
	//

	Int_t numberOfLambdac=0;
	if(TMath::Abs(part->GetPdgCode())!=4122) return 0;
	// Int_t daughTmp[2];
	// daughTmp[0]=part->GetDaughter(0);
	// daughTmp[1]=part->GetDaughter(1);
	Int_t nDaugh = (Int_t)part->GetNDaughters();
	if(nDaugh<2) return 0;
	if(nDaugh>3) return 0;
	AliAODMCParticle* pdaugh1 = (AliAODMCParticle*)arrayMC->At(part->GetDaughter(0));
	if(!pdaugh1) {return 0;}
	Int_t number1 = TMath::Abs(pdaugh1->GetPdgCode());
	AliAODMCParticle* pdaugh2 = (AliAODMCParticle*)arrayMC->At(part->GetDaughter(1));
	if(!pdaugh2) {return 0;}
	Int_t number2 = TMath::Abs(pdaugh2->GetPdgCode());

	Printf("Is non resonant?");
	if(nDaugh==3){
		Int_t thirdDaugh=part->GetDaughter(1)-1;
		AliAODMCParticle* pdaugh3 = (AliAODMCParticle*)arrayMC->At(thirdDaugh);
		Int_t number3 = TMath::Abs(pdaugh3->GetPdgCode());
		if((number1==321 && number2==211 && number3==2212) ||
				(number1==211 && number2==321 && number3==2212) ||
				(number1==211 && number2==2212 && number3==321) ||
				(number1==321 && number2==2212 && number3==211) ||
				(number1==2212 && number2==321 && number3==211) ||
				(number1==2212 && number2==211 && number3==321)) {
			numberOfLambdac++;
			Printf("Lc decays non-resonantly");
			return 1;
		} 
	}

	if(nDaugh==2){

		//Lambda resonant

		//Lambda -> p K*0
		//
		Int_t nfiglieK=0;

		if((number1==2212 && number2==313)){
			nfiglieK=pdaugh2->GetNDaughters();
			if(nfiglieK!=2) return 0;
			AliAODMCParticle* pdaughK1 = (AliAODMCParticle*)arrayMC->At(pdaugh2->GetDaughter(0));
			AliAODMCParticle* pdaughK2 = (AliAODMCParticle*)arrayMC->At(pdaugh2->GetDaughter(1));
			if(!pdaughK1) return 0;
			if(!pdaughK2) return 0;
			if((TMath::Abs(pdaughK1->GetPdgCode())==211 && TMath::Abs(pdaughK2->GetPdgCode())==321) || (TMath::Abs(pdaughK1->GetPdgCode())==321 && TMath::Abs(pdaughK2->GetPdgCode())==211)) {
				numberOfLambdac++;
				Printf("Lc decays via K* p");
				return 3;
			}
		}

		if((number1==313 && number2==2212)){
			nfiglieK=pdaugh1->GetNDaughters();
			if(nfiglieK!=2) return 0;
			AliAODMCParticle* pdaughK1 = (AliAODMCParticle*)arrayMC->At(pdaugh1->GetDaughter(0));
			AliAODMCParticle* pdaughK2 = (AliAODMCParticle*)arrayMC->At(pdaugh1->GetDaughter(1));
			if(!pdaughK1) return 0;
			if(!pdaughK2) return 0;
			if((TMath::Abs(pdaughK1->GetPdgCode())==211 && TMath::Abs(pdaughK2->GetPdgCode())==321) || (TMath::Abs(pdaughK1->GetPdgCode())==321 && TMath::Abs(pdaughK2->GetPdgCode())==211)) {
				numberOfLambdac++;
				Printf("Lc decays via K* p");
				return 3;
			}
		}

		//Lambda -> Delta++ k
		Int_t nfiglieDelta=0;
		if(number1==321 && number2==2224){
			nfiglieDelta=pdaugh2->GetNDaughters();
			if(nfiglieDelta!=2) return 0;
			AliAODMCParticle *pdaughD1=(AliAODMCParticle*)arrayMC->At(pdaugh2->GetDaughter(0));
			AliAODMCParticle *pdaughD2=(AliAODMCParticle*)arrayMC->At(pdaugh2->GetDaughter(1));
			if(!pdaughD1) return 0;
			if(!pdaughD2) return 0;
			if((TMath::Abs(pdaughD1->GetPdgCode())==211 && TMath::Abs(pdaughD2->GetPdgCode())==2212) || (TMath::Abs(pdaughD1->GetPdgCode())==2212 && TMath::Abs(pdaughD2->GetPdgCode())==211)) {
				numberOfLambdac++;
				Printf("Lc decays via Delta++ k");
				return 4;
			}
		}
		if(number1==2224 && number2==321){
			nfiglieDelta=pdaugh1->GetNDaughters();
			if(nfiglieDelta!=2) return 0;
			AliAODMCParticle* pdaughD1 = (AliAODMCParticle*)arrayMC->At(pdaugh1->GetDaughter(0));
			AliAODMCParticle* pdaughD2 = (AliAODMCParticle*)arrayMC->At(pdaugh1->GetDaughter(1));
			if(!pdaughD1) return 0;
			if(!pdaughD2) return 0;
			if((TMath::Abs(pdaughD1->GetPdgCode())==211 && TMath::Abs(pdaughD2->GetPdgCode())==2212) || (TMath::Abs(pdaughD1->GetPdgCode())==2212 && TMath::Abs(pdaughD2->GetPdgCode())==211)) {
				numberOfLambdac++;
				Printf("Lc decays via Delta++ k");
				return 4;
			}
		}


		//Lambdac -> Lambda(1520) pi
		Int_t nfiglieLa=0;
		if(number1==3124 && number2==211){
			nfiglieLa=pdaugh1->GetNDaughters();
			if(nfiglieLa!=2) return 0;
			AliAODMCParticle *pdaughL1=(AliAODMCParticle*)arrayMC->At(pdaugh1->GetDaughter(0));
			AliAODMCParticle *pdaughL2=(AliAODMCParticle*)arrayMC->At(pdaugh1->GetDaughter(1));
			if(!pdaughL1) return 0;
			if(!pdaughL2) return 0;
			if((TMath::Abs(pdaughL1->GetPdgCode())==321 && TMath::Abs(pdaughL2->GetPdgCode())==2212) || (TMath::Abs(pdaughL1->GetPdgCode())==2212 && TMath::Abs(pdaughL2->GetPdgCode())==321)) {
				numberOfLambdac++;
				Printf("Lc decays via Lambda(1520) pi");
				return 2;
			}
		}
		if(number1==211 && number2==3124){
			nfiglieLa=pdaugh2->GetNDaughters();
			if(nfiglieLa!=2) return 0;
			AliAODMCParticle *pdaughL1=(AliAODMCParticle*)arrayMC->At(pdaugh2->GetDaughter(0));
			AliAODMCParticle *pdaughL2=(AliAODMCParticle*)arrayMC->At(pdaugh2->GetDaughter(1));
			if(!pdaughL1) return 0;
			if(!pdaughL2) return 0;
			if((TMath::Abs(pdaughL1->GetPdgCode())==321 && TMath::Abs(pdaughL2->GetPdgCode())==2212) || (TMath::Abs(pdaughL1->GetPdgCode())==2212 && TMath::Abs(pdaughL2->GetPdgCode())==321)) {
				numberOfLambdac++;
				Printf("Lc decays via Lambda(1520) pi");
				return 2;
			}
		}
	}
	return -100;
	Printf("Lc but not one of resonances - should not happen");
}



