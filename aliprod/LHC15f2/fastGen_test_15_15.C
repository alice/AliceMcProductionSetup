#if !defined(__CINT__) || defined(__MAKECINT__)
#include <Riostream.h>
#include <TH1F.h>
#include <TStopwatch.h>
#include <TDatime.h>
#include <TRandom.h>
#include <TDatabasePDG.h>
#include <TParticle.h>
#include <TArrayI.h>
#include <TTree.h>
#include <TFile.h>
#include <fstream>

#include "AliGenerator.h"
#include "AliPDG.h"
#include "AliRunLoader.h"
#include "AliRun.h"
#include "AliStack.h"
#include "AliHeader.h"
#include "AliGenHIJINGpara.h"
#include "AliGenHijing.h"
#include "THijing.h"
#include <TVirtualMC.h>


#endif

void GetFinalDecayProducts(Int_t ind, AliStack & stack , TArrayI & ar);


//========================//
// Set Random Number seed //
//========================//
TDatime dt;
static UInt_t seed    = dt.Get();

void fastGen(Int_t nev = 1, char* filename = "galice.root")
{
  gRandom->SetSeed(seed);
  cout<<"Seed for random number generation= "<<seed<<endl;

  AliPDG::AddParticlesToPdgDataBase();
  TDatabasePDG::Instance();
  
  TH1F* htrial = new TH1F("htrial", "trial", 1000, 0.5, 1000.5);
  TH1F* htrial2Bin = new TH1F("htrial2Bin", "trial2Bin", 2, -0.5, 1.5);

  // Run loader
  AliRunLoader* rl = AliRunLoader::Open("galice.root","FASTRUN","recreate");
  
  rl->SetCompressionLevel(2);
  rl->SetNumberOfEventsPerFile(nev);
  rl->LoadKinematics("RECREATE");
  rl->MakeTree("E");
  gAlice->SetRunLoader(rl);
  
  //  Create stack
  rl->MakeStack();
  AliStack* stack = rl->Stack();
  
  //  Header
  AliHeader* header = rl->GetHeader();
  
  //  Create and Initialize Generator
 
  Float_t bMin = 0;
  Float_t bMax = 100.;

  AliGenHijing   *gener = new AliGenHijing(-1);
  // centre of mass energy 
  gener->SetEnergyCMS(TMath::Sqrt(82./208.) * 8000.);
  // impact parameter range
  gener->SetImpactParameterRange(bMin, bMax);	
  // reference frame
  gener->SetReferenceFrame("CMS");
  gener->SetBoostLHC(1);
  // projectile
  gener->SetProjectile("P", 1, 1);
  gener->SetTarget    ("A", 208, 82);
  // tell hijing to keep the full parent child chain
  gener->KeepFullEvent();
  // enable jet quenching
  gener->SetJetQuenching(0);
  // enable shadowing
  gener->SetShadowing(1);
  // Don't track spectators
  gener->SetSpectators(0);
  // kinematic selection
  gener->SetSelectAll(1);
  //gener->SetPtHardMin(2.3);
  gener->SetStack(stack);
  gener->Init();
  cout<<"Initialized AliGenHijing generator"<<endl;
  //

  // Go to galice.root
  rl->CdGAFile();

  THijing * th = gener->GetTHijing();
  
  
  for(Int_t d=927; d<=940; d++){ 
    th->SetMDME(d,1,0);
  }
  //decay 941 - Delta++ + K-
  for(Int_t d=942; d<=943; d++){ 
    th->SetMDME(d,1,0);
  }
  //decay 944 - K* + p 
	//decay 945 - p + K + pi
  for(Int_t d=946; d<=949; d++){ 
    th->SetMDME(d,1,0);
  }
  //decay 950 - Lambda1520 + pi+
  for(Int_t d=951; d<=1004; d++){ 
    th->SetMDME(d,1,0);
  }


//other decays
//
//Kstar - decay 629 - K+ + pi-
//  th->SetMDME(630,1,0);
//  th->SetMDME(631,1,0);

//Lambda1520 1071-1078
//	for(Int_t d=1071; d<=1078; d++){
//		th->SetMDME(d,1,0);
//	}	

  
  TStopwatch timer;
  timer.Start();
  for (Int_t iev = 0; iev < nev; iev++) {

	  //cout <<"Event number "<< iev << endl;

	  //  Initialize event
	  header->Reset(0,iev);
	  rl->SetEventNumber(iev);
	  stack->Reset();
	  rl->MakeTree("K");

	  //  Generate event
	  Int_t nprim = 0;
	  Int_t ntrial = 0;
	  Int_t ndstar = 0;  

	  //-------------------------------------------------------------------------------------

	  Double_t yCut = 1.5;
	  Double_t yCutDaugh = 1.5;
	  while(!ndstar) {
		  // Selection of events with D*
		  stack->Reset();
		  stack->ConnectTree(rl->TreeK());
		  THijing * thTMP = gener->GetTHijing();
		  Printf(">>>>>> IMPACT PARAMTER FOR CURRENT EVENT: bMin = %f, bMax = %f", thTMP->GetBMIN(), thTMP->GetBMAX());
		  gener->Generate();
		  htrial2Bin->Fill(0);
		  ntrial++;
		  nprim = stack->GetNprimary();

		  for(Int_t ipart =0; ipart < nprim; ipart++){
			  TParticle * part = stack->Particle(ipart);
			  Double_t yLc = part->Y();
			  if(part)    {
			//	if(TMath::Abs(part->GetPdgCode())== 3124) {
			//		cout <<"found L1520"<<endl;
			//		stack->DumpPStack();
			//		Int_t firstDaughter = part->GetFirstDaughter();
			//		Int_t lastDaughter = part->GetLastDaughter();
				//	Int_t nDaughter = lastDaughter - firstDaughter;
			//		cout<<"L1520 n daughters = "<<nDaughter <<endl;
			//		continue;
				  if (TMath::Abs(part->GetPdgCode())== 4122 && TMath::Abs(yLc) < yCut && part->IsPrimary()) {
					  Printf("Found Lc with label %d", ipart);
					  TArrayI daughtersId;
					  TArrayI firstDaughtersId;

				//		stack->DumpPStack();


					  Bool_t kineOK = kTRUE;

					  GetFinalDecayProducts(ipart,*stack,daughtersId);

					  for (Int_t id=1; id<=daughtersId[0]; id++) {
						  TParticle * daughter = stack->Particle(daughtersId[id]);
						  if (!daughter) {
							  kineOK = kFALSE;
							  break;
						  }
						  Double_t yDaugh = daughter->Y();
						  if (TMath::Abs(yDaugh) > yCutDaugh){
							  kineOK = kFALSE;
							  Printf(">>>> Daughter out of y range, y = %f", yDaugh);
							  break;
						  }
						  daughter->Print();
						  Printf(">>>> daughter %d has y = %f", id, yDaugh);
					  }
					  if (!kineOK) continue;
					  Printf("The mother was:");
					  part->Print();
					  Printf(">>>> Lc has y = %f", yLc);
					  htrial2Bin->Fill(1);	   
					  ndstar++;     
				  }
				  else if (TMath::Abs(part->GetPdgCode())== 4122){
					  Printf(">>>> Lc out of y range, y = %f", yLc);
				  }
			  }
		  }   
	  }   

	  cout << "Number of particles " << nprim << endl;
	  cout << "Number of trials " << ntrial << endl;
	  htrial->Fill(ntrial);

	  //  Finish event
	  header->SetNprimary(stack->GetNprimary());
	  header->SetNtrack(stack->GetNtrack());  

	  //      I/O
	  stack->FinishEvent();
	  header->SetStack(stack);
	  rl->TreeE()->Fill();
	  rl->WriteKinematics("OVERWRITE");

  } // event loop
  timer.Stop();
  timer.Print();

  //                         Termination
  //  Generator
  gener->FinishRun();
  //  Write file
  rl->WriteHeader("OVERWRITE");
  gener->Write();
  rl->Write();

  TFile* fout = new TFile("statistics.root", "RECREATE");
  htrial->Write();
  htrial2Bin->Write();
  fout->Close();


}

void GetFinalDecayProducts(Int_t ind, AliStack & stack , TArrayI & ar){

	// Recursive algorithm to get the final decay products of a particle
	//
	// ind is the index of the particle in the AliStack
	// stack is the particle stack from the generator
	// ar contains the indexes of the final decay products
	// ar[0] is the number of final decay products

	if (ind<0 || ind>stack.GetNtrack()) {
		cerr << "Invalid index of the particle " << ind << endl;
		return;
	} 
	if (ar.GetSize()==0) {
		ar.Set(10);
		ar[0] = 0;
	}

	TParticle * part = stack.Particle(ind);

	Int_t iFirstDaughter = part->GetFirstDaughter();
	
	if(iFirstDaughter>=0) cout <<"not final decay product: "<<endl;
	TParticle * daughter = stack.Particle(ind);
	daughter->Print();

	if( iFirstDaughter<0) {
		// This particle is a final decay product, add its index to the array
		ar[0]++;
		if (ar.GetSize() <= ar[0]) ar.Set(ar.GetSize()+10); // resize if needed
		ar[ar[0]] = ind;
		return;
	} 

	Int_t iLastDaughter = part->GetLastDaughter();

	for (Int_t id=iFirstDaughter; id<=iLastDaughter;id++) {
		// Now search for final decay products of the daughters
		GetFinalDecayProducts(id,stack,ar);
	}
}
