void sim(Int_t nev=400) {
// void sim(Int_t nev=10) {
  AliSimulation simulator;

  simulator.SetWriteRawData("ALL","raw.root",kTRUE); 
  simulator.SetMakeSDigits("TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO");
  simulator.SetMakeDigits("ITS TPC TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO");
  simulator.SetMakeDigitsFromHits("ITS TPC");
//
//
// RAW OCDB snapshot, LHC12a, Pass2
  simulator.SetDefaultStorage("local://OCDB");

//
// Vertex and Mag.field from OCDB

  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

//
// The rest
//
  simulator.Run(nev);
}

