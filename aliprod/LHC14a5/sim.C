void sim(Int_t nev=100) {
  AliSimulation simulator;
  //simulator.SetWriteRawData("ALL","raw.root",kFALSE);
  simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD");
  simulator.SetMakeDigitsFromHits("ITS TPC");
//
//
// RAW OCDB
  simulator.SetDefaultStorage("alien://Folder=/alice/data/2013/OCDB");

// Specific storages = 8 

//
// ITS  (1 Total)
//     Alignment from Ideal OCDB 

  simulator.SetSpecificStorage("ITS/Align/Data",  "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");

//
//
  // MUON  (1 Total)                                                                                                                    
  simulator.SetSpecificStorage("MUON/Align/Data",          "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");

  //                                                                                                                                    
  // TPC (6 total)                                                                                                                      
  simulator.SetSpecificStorage("TPC/Calib/TimeGain",       "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/Correction",     "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Align/Data",           "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");
  simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      "alien://folder=/alice/simulation/2008/v4-15-Release/Ideal");


//
// Vertex and magfield


  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

  // PHOS simulation settings
  AliPHOSSimParam *simParam = AliPHOSSimParam::GetInstance();
  simParam->SetCellNonLineairyA(0.001);
  simParam->SetCellNonLineairyB(0.2);
  simParam->SetCellNonLineairyC(1.02);


  //simulator.SetRunHLT("");
  //simulator.SetRunQA(":") ;


//
// The rest
//
  printf("Before simulator.Run(nev);\n");
  simulator.Run(nev);
  printf("After simulator.Run(nev);\n");
}
