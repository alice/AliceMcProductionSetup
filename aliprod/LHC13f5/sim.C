void sim(Int_t nev=10) {
  AliSimulation simulator;

  simulator.SetMakeSDigits("TOF TRD PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");
  simulator.SetMakeDigitsFromHits("ITS TPC");

//
//
// RAW OCDB
  simulator.SetDefaultStorage("alien://Folder=/alice/data/2011/OCDB");
  simulator.SetCDBSnapshotMode("OCDB_MCsim.root");

// simulate HLT TPC clusters, trigger, and HLT ESD
  simulator.SetRunHLT("chains=TPC-compression,GLOBAL-Trigger,GLOBAL-esd-converter");

// Specific storages = 3 

//
// ITS  1 Total)
//     Alignment from Ideal OCDB 

  simulator.SetSpecificStorage("ITS/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal");

//
// MUON (1 Total)
//      MCH

  simulator.SetSpecificStorage("MUON/Align/Data",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Ideal/");

//
// TPC (1 total) 

  simulator.SetSpecificStorage("TPC/Calib/PadGainFactor",   "alien://Folder=/alice/data/2011/OCDB");

//
// Vertex and Mag.field from OCDB

  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

//
// The rest
//
  simulator.SetRunQA(":");
  simulator.Run(nev);
}
