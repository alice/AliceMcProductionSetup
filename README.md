ALICE Monte Carlo Production setup
==================================

This is the repository for the ALICE Monte Carlo Production setup. The main
clone of this repository is hosted at CERN GitLab.

If you clone it using the given HTTPS URL you will authenticate using your CERN
username and your CERN password.


Configure your repository
-------------------------

Once you have cloned, you need to set up your repository like the following:

```bash
git config user.name 'Firstname Lastname'   # *NOT* your CERN username
git config user.email 'user.email@cern.ch'
```

**Note:** set your full name (first and last name) and respect capitalization:
**do not use your CERN username here!**


Permissions
-----------

 * All users in the **alice-off** egroup can **push** and **create new
   branches**.
 * Force-pushes, which may accidentally destroy commits, are banned for the
   **master** branch.

For more information, [this is the full list of
permissions](https://gitlab.cern.ch/help/permissions/permissions.md): users in
**alice-off** have **Developer** permissions from the table.


### First login

You must login at least once on the GitLab web interface before being able to
push from the command line. In case push fails with an "authentication
problem", it means you need to go once again on GitLab web and login from there.

This is because egroups are synchronized with GitLab groups only when logging in
from the web interface.


GitLab at CERN
--------------

Information on the GitLab service at CERN [can be found
here](https://cern.service-now.com/service-portal/topic.do?topic=Gitlab).


Issues
------

In case of a problem with the service (cannot connect, webpage down, etc.) you
should open a report using [CERN Service
Now](https://cern.service-now.com/service-portal/) as the Git service is managed
by the CERN IT.

Have a look at the [Service Status
Dashboard](https://cern.service-now.com/service-portal/sls.do) to see if the Git
service is reported as "green" before opening a ticket.

For problems related to the Git administration of the project, open a ticket on
the [ALICE JIRA](https://alice.its.cern.ch/), on the Git Administration (GA)
project.
