# Macro to fill first estmate of the MC production tables based on the past production
#
# Parsing per production and per RUN JDL, sim.C, rec.C
# 
#    1.)  Cache basic production information - cacheProductionSetupInormation()
#    2.)  Cache per run production information
#

# To run the code some parameters should be adjusted
#
workSpace=$NOTES/JIRA/PWGPP-123/data/productionCache
#

cacheProductionSetupInormation(){ 
    #
    # cache per period production information
    #
    mkdir -p  $workSpace/lists
    cd $workSpace/lists
    #
    alien_find /alice/cern.ch/user/a/aliprod/ JDL  > production.list
    alien_find /alice/cern.ch/user/a/aliprod/ *.C  >> production.list
    alien_find /alice/cern.ch/user/a/aliprod/ *.sh >> production.list
    cd $workSpace/
    $ALICE_PHYSICS/../src/PWGPP/QA/scripts/alienSync.sh  alienFindCommand="cat lists/production.list" localPathPrefix=$workSpace
}


cachePerRunProductionInfo(){
    #
    curl --cert $HOME/.globus/usercert.pem --key $HOME/.globus/userkey.pem  --capath $HOME/alien/globus/share/certificates   "https://alimonitor.cern.ch/job_details.jsp?$&res_path=csv" -o  production.csv
}
